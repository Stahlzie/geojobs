//
//  MyJobListingsView.swift
//  GeoJobs
//
//  Created by Kory Kappel on 3/26/15.
//
//

import UIKit

class MyJobListingsView: UIViewController {
    
    var child : MyJobListingsTable!
    var parentView : HomeView!
    
    @IBOutlet weak var segmentedControl : UISegmentedControl!

    @IBAction func IndexChanged(sender: UISegmentedControl) {
        
        switch segmentedControl.selectedSegmentIndex {
        case 0:
            child.segmentedState = "New"
            child.tableView.reloadData()
        case 1:
            child.segmentedState = "Old"
            child.tableView.reloadData()
        default:
            break
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        child = self.childViewControllers[0] as! MyJobListingsTable
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //back button pressed
    override func viewWillDisappear(animated: Bool) {
        parentView.tableView.reloadData()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
