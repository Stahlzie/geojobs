//
//  ApplicationViewTestCase.swift
//  GeoJobs
//
//  Created by Steven Patterson on 3/18/15.
//
//

import UIKit
import XCTest
import GeoJobs

class ApplicationViewTestCase: XCTestCase {
    
    var appView : ApplicationView!

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        //var storyboard = UIStoryboard(name: "Main", bundle: nil)
        //appView = storyboard.instantiateViewControllerWithIdentifier("Application") as ApplicationView
        //appView = appViewTemp as ApplicationView
        //appView = ApplicationView()
        //appView.viewDidLoad()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    /*
    
    func testApplicationTableExists() {
        var bool : Bool = false
        bool = (appView.child != nil)
        XCTAssert(bool, "Pass")
    }
    
    func testUserNameExists() {
        var bool : Bool = false
        bool = (appView.child.username != nil)
        XCTAssert(bool, "Pass")
    }
    
    func testUserNameIsSet() {
        var bool : Bool = false
        bool = (appView.child.username.text == "UserName")
        XCTAssert(bool, "Pass")
    }
    
    func testPriceExists() {
        var bool : Bool = false
        bool = (appView.child.price != nil)
        XCTAssert(bool, "Pass")
    }
    
    func testPriceIsSet() {
        var bool : Bool = false
        bool = (appView.child.price.text == "$40")
        XCTAssert(bool, "Pass")
    }
    
    func testProfilePicExists() {
        var bool : Bool = false
        bool = (appView.child.profilePic != nil)
        XCTAssert(bool, "Pass")
    }
    
    func testProfilePicIsSet() {
        var bool : Bool = false
        bool = (appView.child.profilePic.image == "DosEquis.png")
        XCTAssert(bool, "Pass")
    }
    
    func testPitchIsSet() {
        var bool : Bool = false
        bool = (appView.child.pitch.text == "This is dummy data; will be able to easily assign the right data once My Job Applications page is done")
        XCTAssert(bool, "Pass")
    }
    
//    func testFloatRatingExists() {
//        var bool : Bool = false
//        bool = (appView.child.floatRatingView as FloatRatingView! != nil)
//        XCTAssert(bool, "Pass")
//    }
    
    func testNumReviewsExists() {
        var bool : Bool = false
        bool = (appView.child.numReviews != nil)
        XCTAssert(bool, "Pass")
    }
    
    func testNumReviewsIsSet() {
        var bool : Bool = false
        bool = (appView.child.numReviews.text == "(24)")
    }

    func testExample() {
        // This is an example of a functional test case.
        XCTAssert(true, "Pass")
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock() {
            // Put the code you want to measure the time of here.
        }
    }
*/

}
