//
//  ListingClassTests.swift
//  GeoJobs
//
//  Created by Nathan Snyder on 2015-03-22.
//
//

import UIKit
import XCTest
import GeoJobs

class ListingsClassTests: XCTestCase {
    var objectsToDelete : [PFObject] = []
    
    var appView : ApplicationView!
    //var testApp : GeoJobs.PSListing!
    
    let existingUserID = "CoolDude"
    let existingUserPW = "cool"
    let existingAppID = "ZAjE6Oa1oX"
    let existingAppTitle = "hi"
    let numberOfNewContracts = 1
    let numberOfContracts = 2
    
    override func setUp() {
        super.setUp()
        //PSListing.load()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        //var storyboard = UIStoryboard(name: "Main", bundle: nil)
        //appView = storyboard.instantiateViewControllerWithIdentifier("Application") as ApplicationView
        //appView = appViewTemp as ApplicationView
        //appView = ApplicationView()
        //appView.viewDidLoad()
        PSUser.logInWithUsername(existingUserID, password: existingUserPW)
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        PSUser.logOut()
        PFObject.deleteAll(objectsToDelete)
        super.tearDown()
    }
    
    func testQueryListingById() {
        var testListing = GeoJobs.PSListing.queryListingById(existingAppID) //as! GeoJobs.PSListing!
        NSLog(testListing.title)
        XCTAssertTrue(testListing.title == self.existingAppTitle, "Test with known listing")
    }
    
    func testQueryListingByIdInBackground() {
        let readyExpectation = expectationWithDescription("Ready")
        
        GeoJobs.PSListing.queryListingByIdInBackground(existingAppID, completion: { (listing) -> Void in
            XCTAssertTrue(listing.title == self.existingAppTitle, "Test in background with known listing")
            
            readyExpectation.fulfill()
        })
        
        waitForExpectationsWithTimeout(5, handler: { error in
            XCTAssertNil(error, "Error")
        })
        
    }
    
    func testListingInitializationAndSaving() {
        let listing = GeoJobs.PSListing(title: "test listing", description: "test description", location: PFGeoPoint(latitude: 80, longitude: 80), price: 10.0, lister: GeoJobs.PSUser.currentUser())
        
        let success = listing.save()
        XCTAssertTrue(success, "Listing not saved")
        
        objectsToDelete.append(listing)
    }
    
    func testListingInitializationAndSavingInBackground() {
        let listing = GeoJobs.PSListing(title: "test listing", description: "test description", location: PFGeoPoint(latitude: 80, longitude: 80), price: 10.0, lister: GeoJobs.PSUser.currentUser())
        
        let readyExpectation = expectationWithDescription("Ready")
        objectsToDelete.append(listing)
        
        listing.saveInBackgroundWithBlock { (success, error) -> Void in
            XCTAssertTrue(success, "Test with known listing")
            XCTAssertNil(error, "Error")
            
            readyExpectation.fulfill()
        }
        
        waitForExpectationsWithTimeout(5, handler: { error in
            XCTAssertNil(error, "Error")
        })
    }
    
    func testQueryNewContractCount() {
        let count = GeoJobs.PSListing.queryNewContractCount()
        XCTAssertTrue(Int(count) == self.numberOfNewContracts, "Count is wrong")
    }
    
    func testQueryNewContractCountInBackground() {
        let readyExpectation = expectationWithDescription("Ready")
        
        GeoJobs.PSListing.queryNewContractCountInBackground { (count) -> Void in
            XCTAssertTrue(Int(count) == self.numberOfNewContracts, "Count is wrong")
            
            readyExpectation.fulfill()
        }
        
        waitForExpectationsWithTimeout(5, handler: { error in
            XCTAssertNil(error, "Error")
        })
    }
    
    func testQueryMyListingsAndQueryMyListingsCount() {
        let count = GeoJobs.PSListing.queryMyListingsCount()
        let objects = GeoJobs.PSListing.queryMyListings()
        XCTAssertTrue(objects.count == count, "There are no objects")
    }
    
    func testQueryMyListingsInBackground() {
        let readyExpectation = expectationWithDescription("Ready")
        
        GeoJobs.PSListing.queryMyListingsInBackground(false) { (listings) -> Void in
            XCTAssertTrue(listings.count > 0, "No listings found")
            
            readyExpectation.fulfill()
        }
        
        waitForExpectationsWithTimeout(5, handler: { error in
            XCTAssertNil(error, "Error")
        })
    }
    
    func testMyContracts() {
        let contracts = GeoJobs.PSListing.queryMyContracts()
        
        XCTAssertTrue(contracts.count == self.numberOfContracts, "Not the right number of contracts")
    }
    
    func testMyContractsInBackground() {
        let readyExpectation = expectationWithDescription("Ready")
        
        GeoJobs.PSListing.queryMyContractsInBackground() { (contracts) -> Void in
            XCTAssertTrue(contracts.count == self.numberOfContracts, "Not the right number of contracts")
            
            readyExpectation.fulfill()
        }
        
        waitForExpectationsWithTimeout(5, handler: { error in
            XCTAssertNil(error, "Error")
        })
    }
    
    func testQueryApplicationAndQueryApplicationCount() {
        var testListing = GeoJobs.PSListing.queryListingById(existingAppID) //as! GeoJobs.PSListing!
        var testApp = testListing.queryApplications()
        var testAppCount = testListing.queryApplicationCount()
        
        XCTAssertTrue(testApp.count == testAppCount, "Test Listing's Applications != count of those Applications")
    }
    
//    func testQueryApplicationInBackground() {
//        let readyExpectation = expectationWithDescription("Ready")
//        var testListing = GeoJobs.PSListing.queryListingById(existingAppID) //as! GeoJobs.PSListing!
//        var testAppCount = testListing.queryApplicationCount()
//        
//        testListing.queryApplicationsInBackground { (applications) -> Void in
//            XCTAssertTrue(listings.count > 0, "No listings found")
//            
//            readyExpectation.fulfill()
//        }
//        
//        waitForExpectationsWithTimeout(5, handler: { error in
//            XCTAssertNil(error, "Error")
//        })
//    }
    
    func testQueryApplicationCountInBackground() {
        let readyExpectation = expectationWithDescription("Ready")
        var testListing = GeoJobs.PSListing.queryListingById(existingAppID) //as! GeoJobs.PSListing!
        var testApps = testListing.queryApplications()
        
        testListing.queryApplicationCountInBackground { (count) -> Void in
            XCTAssertTrue(testApps.count == count, "Query application count in background is false")
            
            readyExpectation.fulfill()
        }
        
        waitForExpectationsWithTimeout(5, handler: { error in
            XCTAssertNil(error, "Error")
        })
    }
    
    func testQueryAcceptedApplication() {
        var testListing = GeoJobs.PSListing.queryListingById(existingAppID) //as! GeoJobs.PSListing!
        var testAcceptedApp = testListing.queryAcceptedApplication()
        XCTAssertNotNil(testAcceptedApp, "Listing did not return accepted application")
    }
    
    func testQueryAcceptedApplicationInBackground() {
        let readyExpectation = expectationWithDescription("Ready")
        var testListing = GeoJobs.PSListing.queryListingById(existingAppID) //as! GeoJobs.PSListing!
        
        testListing.queryAcceptedApplicationInBackground { (testAcceptedApp) -> Void in
            XCTAssertNotNil(testAcceptedApp, "Listing did not return accepted application")
            readyExpectation.fulfill()
        }
        
        waitForExpectationsWithTimeout(5, handler: { error in
            XCTAssertNil(error, "Error")
        })
    }
    
    /*
    
    func testApplicationTableExists() {
    var bool : Bool = false
    bool = (appView.child != nil)
    XCTAssert(bool, "Pass")
    }
    
    func testUserNameExists() {
    var bool : Bool = false
    bool = (appView.child.username != nil)
    XCTAssert(bool, "Pass")
    }
    
    func testUserNameIsSet() {
    var bool : Bool = false
    bool = (appView.child.username.text == "UserName")
    XCTAssert(bool, "Pass")
    }
    
    func testPriceExists() {
    var bool : Bool = false
    bool = (appView.child.price != nil)
    XCTAssert(bool, "Pass")
    }
    
    func testPriceIsSet() {
    var bool : Bool = false
    bool = (appView.child.price.text == "$40")
    XCTAssert(bool, "Pass")
    }
    
    func testProfilePicExists() {
    var bool : Bool = false
    bool = (appView.child.profilePic != nil)
    XCTAssert(bool, "Pass")
    }
    
    func testProfilePicIsSet() {
    var bool : Bool = false
    bool = (appView.child.profilePic.image == "DosEquis.png")
    XCTAssert(bool, "Pass")
    }
    
    func testPitchIsSet() {
    var bool : Bool = false
    bool = (appView.child.pitch.text == "This is dummy data; will be able to easily assign the right data once My Job Applications page is done")
    XCTAssert(bool, "Pass")
    }
    
    //    func testFloatRatingExists() {
    //        var bool : Bool = false
    //        bool = (appView.child.floatRatingView as FloatRatingView! != nil)
    //        XCTAssert(bool, "Pass")
    //    }
    
    func testNumReviewsExists() {
    var bool : Bool = false
    bool = (appView.child.numReviews != nil)
    XCTAssert(bool, "Pass")
    }
    
    func testNumReviewsIsSet() {
    var bool : Bool = false
    bool = (appView.child.numReviews.text == "(24)")
    }
    
    func testExample() {
    // This is an example of a functional test case.
    XCTAssert(true, "Pass")
    }
    
    func testPerformanceExample() {
    // This is an example of a performance test case.
    self.measureBlock() {
    // Put the code you want to measure the time of here.
    }
    }
    */
    
}
