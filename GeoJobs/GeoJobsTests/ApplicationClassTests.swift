//
//  ApplicationClassTests.swift
//  GeoJobs
//
//  Created by Zachary D. Stahl on 3/17/15.
//
//

import UIKit
import XCTest
import GeoJobs

class ApplicationClassTests: XCTestCase {
    var objectsToDelete : [PFObject] = []
    
    let existingUsername = "CoolDude"
    let existingPW = "cool"
    let existingListing = "Bs2S7FCp7n"
    
    override func setUp() {
        super.setUp()
        PSUser.logInWithUsername(existingUsername, password: existingPW)
        var objectsToDelete : [PFObject] = []
    }
    
    override func tearDown() {
        PSUser.logOut()
        PFObject.deleteAll(objectsToDelete)
        super.tearDown()
    }
    
    func testApplicationInitialization() {
        
        let listing = PSListing.query().getObjectWithId(existingListing) as! GeoJobs.PSListing
        let testApp = GeoJobs.PSApplication(pitch: "test pitch", offer: 10, listing: listing)
        
        testApp.status = "testing"
        
        objectsToDelete.append(testApp)
        
        XCTAssert(testApp.save(), "Could not save application to Parse")
    }
    
    func testAcceptAndTestApplicationCount() {
        let listing = GeoJobs.PSListing.query().getObjectWithId(existingListing) as! GeoJobs.PSListing
        let testApp1 = GeoJobs.PSApplication(pitch: "test pitch", offer: 10, listing: listing)
        testApp1.save()
        objectsToDelete.append(testApp1)
        let testApp2 = GeoJobs.PSApplication(pitch: "test pitch", offer: 10, listing: listing)
        testApp2.save()
        objectsToDelete.append(testApp2)
        
        testApp1.accept()
        
        XCTAssertTrue(testApp1.status == "accepted", "TestApp1 status not changed")
        XCTAssertTrue(testApp2.status == "rejected", "TestApp2 status not changed")
        XCTAssertTrue(testApp1.listing.status == "new contract", "TestApp1's Listing's status not changed")
    
        let appCount = GeoJobs.PSApplication.queryApplicationCount()
        XCTAssertTrue(appCount > 0, "No apps rejected")
    }
    
    func testSetApplicationsSeen() {
        PSApplication.setApplicationsSeen()
        let appCount = PSApplication.queryContractCount()
        
        XCTAssertTrue(appCount == 0, "Some apps not set rejected seen")
    }
    
    func testSetContractsSeen() {
        GeoJobs.PSApplication.setContractsSeen()
        let appCount = GeoJobs.PSApplication.queryContractCount()
        XCTAssertTrue(appCount == 0, "Some contracts not set accepted seen")
    }
    
    
    func testExample() {
        XCTAssertTrue(true, "always passes")
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock() {
            // Put the code you want to measure the time of here.
        }
    }


}
