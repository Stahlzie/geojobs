//
//  UserClassTests.swift
//  GeoJobs
//
//  Created by Zachary D. Stahl on 3/17/15.
//
//

import UIKit
import XCTest
import GeoJobs

class UserClassTests: XCTestCase {
    
    let testUsername = "asdflkd"
    let testEmail = "asdlkd@mal.com"
    let testPassword = "password"
    let testRealName = "John Doe"
    let testPhoneNumber = "1231231234"
    
    let dbUsername = "CoolDude"
    let dbPassword = "cool"
    
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        GeoJobs.PSUser.logOut()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        GeoJobs.PSUser.logOut()
        
        super.tearDown()
    }
    
    
    //Note: You cannot run this test multiple times in a row due to a caching issue which throws an error when reusing a PFObject
    func testSignUp() {
        var tempUser = GeoJobs.PSUser(username: testUsername, email: testEmail, password: testPassword, realName: testRealName, phoneNumber: testPhoneNumber)
        XCTAssertTrue(tempUser.signUp(), "Signup Test")
        XCTAssertTrue(GeoJobs.PFUser.currentUser().username == testUsername, "Signed Up User is now Current User Test")
        XCTAssertTrue(tempUser.delete(), "Temporary Test User Deletion")
    }
    
    func testLogin() {
        var tempUser = GeoJobs.PSUser.logInWithUsername(dbUsername, password: dbPassword)
        XCTAssertTrue(tempUser.objectId == GeoJobs.PFUser.currentUser().objectId, "Login Test")
    }
    
    func testQueryUser() {
        let queriedUser = GeoJobs.PSUser.queryUser(dbUsername)
        XCTAssertTrue(queriedUser.username == dbUsername, "Query DB For User")
    }
    
    func testQueryUserAsync() {
        let readyExpectation = expectationWithDescription("Ready")
        
        GeoJobs.PSUser.queryUserInBackground(dbUsername) { (queriedUser) -> Void in
            XCTAssertTrue(queriedUser.username == self.dbUsername, "Async Query DB For User")
            
            readyExpectation.fulfill()
        }
        
        waitForExpectationsWithTimeout(5, handler: { error in
            XCTAssertNil(error, "Error")
        })
    }
    
    func testProfilePhoto() {
        var tempUser = GeoJobs.PSUser.logInWithUsername(dbUsername, password: dbPassword)
        //tempUser.fetchIfNeeded()
        //tempUser.profileImage
        let readyExpectation = expectationWithDescription("Ready")
        tempUser.getProfileImageInBackground { (image) -> Void in
            XCTAssertNotNil(image, "Profile image returned nil")
            readyExpectation.fulfill()
        }
        
        waitForExpectationsWithTimeout(5, handler: { error in
            XCTAssertNil(error, "Error")
        })
    }
}
