//
//  ReviewClassTests.swift
//  GeoJobs
//
//  Created by Zachary D. Stahl on 3/17/15.
//
//

import UIKit
import XCTest
import GeoJobs

class ReviewClassTests: XCTestCase {

    var objectsToDelete : [PFObject] = []
    
    let existingUsername = "CoolDude"
    let existingPW = "cool"
    let existingListing = "AFzy4Cq53y"
    let existingOtherUsername = "StevePatterson"
    var existingOtherUser : GeoJobs.PSUser? = nil
    
    var existingContractWithOneReview = "tFSgm0h1gr"
    var existingContractReviewedUsername = "KoryKappel"
    var existingContractReviewedPW = "kory"
    var existingContractReviewingUsername = "NateSnyder"
    var existingContractReviewingPW = "nathan"
    
    override func setUp() {
        super.setUp()
        GeoJobs.PSUser.logInWithUsername(existingUsername, password: existingPW)
        var objectsToDelete : [PFObject] = []
        
        self.existingOtherUser = GeoJobs.PSUser.queryUser(existingOtherUsername)
    }
    
    override func tearDown() {
        GeoJobs.PSUser.logOut()
        PFObject.deleteAll(objectsToDelete)
        super.tearDown()
    }
    
    func testReviewInitialization() {
        
        let listing = PSListing.query().getObjectWithId(existingListing) as! GeoJobs.PSListing
        let testReview1 = GeoJobs.PSReview(reviewText: "testReview 1-2", listing: listing, rating: NSNumber(double: 1.0), reviewedUser: existingOtherUser!, reviewingUser: GeoJobs.PSUser.currentUser()!)
        objectsToDelete.append(testReview1)
        let testReview2 = GeoJobs.PSReview(reviewText: "testReview 2-1", listing: listing, rating: 5.0 as NSNumber, reviewedUser: GeoJobs.PSUser.currentUser(), reviewingUser: existingOtherUser!)
        objectsToDelete.append(testReview2)
        
        XCTAssertNotNil(testReview1.save(), "Could not save review1 to Parse")
        XCTAssertNotNil(testReview2.save(), "Could not save review2 to Parse")
    }
    
    func testQueryReviewOfUserAndQueryReviewCount() {
        let readyExpectation = expectationWithDescription("Ready")
        let count = GeoJobs.PSReview.queryReviewCount(GeoJobs.PSUser.currentUser())
        GeoJobs.PSReview.queryReviewsOfUser(GeoJobs.PSUser.currentUser()) { (reviews) -> Void in
            XCTAssertNotNil(reviews, "Listing did not return accepted application")
            XCTAssertEqual(reviews.count, count, "Unequal number of reviews returned")
            readyExpectation.fulfill()
        }
        
        waitForExpectationsWithTimeout(5, handler: { error in
            XCTAssertNil(error, "Error")
        })
    }
    
    func testQueryById() {
        let readyExpectation = expectationWithDescription("Ready")
        var revs : [GeoJobs.PSReview] = []
        GeoJobs.PSReview.queryReviewsOfUser(GeoJobs.PSUser.currentUser()) { (reviews) -> Void in
            XCTAssertNotNil(reviews, "Listing did not return accepted application")
            readyExpectation.fulfill()
            revs = reviews
        }
        
        waitForExpectationsWithTimeout(5, handler: { error in
            XCTAssertNil(error, "Error")
        })
        
        for rev in revs {
            let r = GeoJobs.PSReview.queryReviewById(rev.objectId)
            XCTAssertEqual(r, rev, "reviews not equal")
        }
    }
    
    func testQueryReviewLeftForContract() {
        let contract = GeoJobs.PSListing.queryListingById(existingContractWithOneReview)
        
        // 1
        let readyExpectation = expectationWithDescription("Ready")
        
        GeoJobs.PSUser.logOut()
        GeoJobs.PSUser.logInWithUsername(existingContractReviewedUsername, password: existingContractReviewedPW)
        GeoJobs.PSReview.queryReviewLeftForContractInBackground(contract) { (reviewed) -> Void in
            XCTAssertTrue(reviewed, "User has not left review")
            readyExpectation.fulfill()
        }
        
        waitForExpectationsWithTimeout(5, handler: { error in
            XCTAssertNil(error, "Error")
        })
        
        
        GeoJobs.PSUser.logOut()
        GeoJobs.PSUser.logInWithUsername(existingContractReviewingUsername, password: existingContractReviewingPW)
        
        //2
//let readyExpectation2 = expectationWithDescription("Ready")
//        
//        GeoJobs.PSReview.queryReviewLeftForContractInBackground(contract) { (reviewed) -> Void in
//            XCTAssertFalse(reviewed, "User has left review")
//            readyExpectation2.fulfill()
//        }
//        
//        waitForExpectationsWithTimeout(5, handler: { error in
//            XCTAssertNil(error, "Error")
//        })
        
        //3
        let readyExpectation3 = expectationWithDescription("Ready")
        
        GeoJobs.PSReview.queryILeftReviewForContractInBackground(contract) { (reviewed) -> Void in
            XCTAssertTrue(reviewed, "User has left review")
            readyExpectation3.fulfill()
        }
        
        waitForExpectationsWithTimeout(5, handler: { error in
            XCTAssertNil(error, "Error")
        })
    }

}
