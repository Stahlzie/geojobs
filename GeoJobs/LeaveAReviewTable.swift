//
//  LeaveAReviewTable.swift
//  GeoJobs
//
//  Created by Kory Kappel on 3/13/15.
//
//

import UIKit

class LeaveAReviewTable: UITableViewController, FloatRatingViewDelegate {

    @IBOutlet weak var reviewText : UITextView!
    @IBOutlet var floatRatingView: FloatRatingView!
    
    var rating : Float!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        reviewText.becomeFirstResponder()
        
        // Required float rating view params
        self.floatRatingView.emptyImage = UIImage(named: "StarEmpty")
        self.floatRatingView.fullImage = UIImage(named: "StarFull")
        // Optional params
        self.floatRatingView.delegate = self
        self.floatRatingView.contentMode = UIViewContentMode.ScaleAspectFit
        self.floatRatingView.maxRating = 5
        self.floatRatingView.minRating = 1
        self.floatRatingView.rating = 5.0
        rating = self.floatRatingView.rating
        self.floatRatingView.editable = true
        self.floatRatingView.halfRatings = true
        self.floatRatingView.floatRatings = false

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func floatRatingView(ratingView: FloatRatingView, didUpdate rating: Float) {
       // self.updatedLabel.text = NSString(format: "%.2f", self.floatRatingView.rating)
        //NSLog(NSString(format: "%.2f", self.floatRatingView.rating))
        self.rating = self.floatRatingView.rating
    }
    
}
