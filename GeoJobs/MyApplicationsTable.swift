//
//  MyApplicationsTable.swift
//  GeoJobs
//
//  Created by Kory Kappel on 3/27/15.
//
//

import UIKit

class MyApplicationsTable: UITableViewController, UISearchBarDelegate {
    
    var myApplications : [PSApplication] = []
    var applicants = Array<Array<PSApplication>>() //pass this onto child
    var path = NSIndexPath(forRow: 0, inSection: 0)
    var parentView : HomeView!
    var data : [CellData] = []
    
    //for searching
    var filteredData : [CellData] = []
    var filteredApps : [PSApplication] = []
    var filteredApplicants = Array<Array<PSApplication>>()
    var searchActive : Bool = false
    var searchBar : UISearchBar!
    
    struct CellData {
        var reviewCount : Int
        var rating : Float
        var listing : PSListing!
        var lister : PSUser!
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "My Applications"

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        
        //creates a search bar and sets it to be hidden until user scrolls up
        searchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: self.tableView.frame.width, height: 44))
        self.tableView.tableHeaderView = searchBar
        self.tableView.setContentOffset(CGPointMake(0.0, 44), animated: true)
        searchBar.delegate = self
        searchBar.showsCancelButton = true
        
        
        self.view.userInteractionEnabled = false
        PSApplication.queryMyApplicationsInBackground { (applications) -> Void in
            dispatch_async(dispatch_get_global_queue(Int(QOS_CLASS_USER_INITIATED.value), 0)) {
                self.myApplications = applications
                for application in applications {
                    self.applicants.append(application.listing.queryApplications())
                }
                self.fetchOnce()
                self.view.userInteractionEnabled = true
                dispatch_async(dispatch_get_main_queue()) {
                    self.tableView.reloadData()
                }
            }
            
            
            //if there are any unseen rejected apps, it sets them as seen when this view loads
            //so that the notifications go away
            //JUST DOING THIS ONE BY ONE IN CELLFORROWATINDEXPATH NOW
            //PSApplication.setApplicationsSeenInBackground()
            
            //myApplications = PSApplication.queryMyApplications()
        }
    }
    
    func fetchOnce() {
        for application in self.myApplications {
            var cd = CellData(reviewCount: 0, rating: 0.0, listing: nil, lister: nil)
            let listing = application.listing.fetchIfNeeded() as! PSListing
            let lister = listing.lister.fetchIfNeeded() as! PSUser!
            cd.reviewCount = PSReview.queryReviewCount(lister)
            cd.rating = Float(PSReview.queryAverageRatingOfUser(lister))
            cd.listing = listing
            cd.lister = lister
            data.append(cd)
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        if searchActive {
            return filteredData.count
        } else {
            return myApplications.count
        }
    }

    func calculateTimeSinceJobPosting(jobDate: NSDate) -> String {
        let currentDate = NSDate()
        var calendar : NSCalendar = NSCalendar.currentCalendar()
        let components = calendar.components(NSCalendarUnit.CalendarUnitYear | NSCalendarUnit.CalendarUnitMonth | NSCalendarUnit.CalendarUnitWeekOfYear | NSCalendarUnit.CalendarUnitDay | NSCalendarUnit.CalendarUnitHour | NSCalendarUnit.CalendarUnitMinute, fromDate: jobDate, toDate: currentDate, options: nil)
        
        if components.year >= 1 {
            if components.year < 2 {
                return "1 year ago"
            } else {
                return "\(components.year) years ago"
            }
        } else if components.month >= 1 {
            if components.month < 2 {
                return "1 month ago"
            } else {
                return "\(components.month) months ago"
            }
        } else if components.weekOfYear >= 1 {
            if components.weekOfYear < 2 {
                return "1 week ago"
            } else {
                return "\(components.weekOfYear) weeks ago"
            }
        } else if components.day >= 1 {
            if components.day < 2 {
                return "1 day ago"
            } else {
                return "\(components.day) days ago"
            }
        } else if components.hour >= 1 {
            if components.hour < 2 {
                return "1 hour ago"
            } else {
                return "\(components.hour) hours ago"
            }
        } else {
            if components.minute >= 2 {
                return "\(components.minute) minutes ago"
            } else if components.minute < 2 && components.minute >= 1 {
                return "1 minute ago"
            } else {
                return "a moment ago"
            }
        }
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as! MyApplicationsCell

        // Set up the cell using stuff from myApplications[indexPath.row]
        
        cell.jobTitleAndPrice.alpha = 0
        cell.jobOwner.alpha = 0
        cell.rating.alpha = 0
        cell.category.alpha = 0
        cell.numReviews.alpha = 0
        cell.numReplies.alpha = 0
        cell.dateApplied.alpha = 0
        
        var formatter = NSNumberFormatter()
        formatter.maximumFractionDigits = 2
        formatter.minimumFractionDigits = 2
        if !searchActive {
            var str = formatter.stringFromNumber(myApplications[indexPath.row].listing.price)
            cell.jobTitleAndPrice.text = "\(data[indexPath.row].listing.title) - $\(str!)"
            cell.jobOwner.text = data[indexPath.row].lister.username
            cell.category.text = data[indexPath.row].listing.category
            cell.numReviews.text = "(\(data[indexPath.row].reviewCount))"
            cell.rating.rating = data[indexPath.row].rating
        } else {
            var str = formatter.stringFromNumber(filteredApps[indexPath.row].listing.price)
            cell.jobTitleAndPrice.text = "\(filteredData[indexPath.row].listing.title) - $\(str!)"
            cell.jobOwner.text = filteredData[indexPath.row].lister.username
            cell.category.text = filteredData[indexPath.row].listing.category
            cell.numReviews.text = "(\(filteredData[indexPath.row].reviewCount))"
            cell.rating.rating = filteredData[indexPath.row].rating
        }
        
        if applicants[indexPath.row].count == 1 {
            cell.numReplies.text = (String)(applicants[indexPath.row].count) + " reply"
        } else {
            cell.numReplies.text = (String)(applicants[indexPath.row].count) + " replies"
        }
        
        
        //change color of job title and price and date to red (and change date text to "You were not selected"
        if myApplications[indexPath.row].rejected {
            cell.jobTitleAndPrice.textColor = UIColor.redColor()
            cell.dateApplied.text = "You were not selected"
            cell.dateApplied.textColor = UIColor.redColor()
        }
        else {
//            let formatter = NSDateFormatter()
//            formatter.dateStyle = NSDateFormatterStyle.ShortStyle
//            let dateString = formatter.stringFromDate(myApplications[indexPath.row].createdAt)
//            cell.dateApplied.text = dateString
            cell.dateApplied.text = calculateTimeSinceJobPosting(myApplications[indexPath.row].createdAt)
        }
        
        UIView.animateWithDuration(0.6, animations: { () -> Void in
            cell.jobTitleAndPrice.alpha = 1
            cell.jobOwner.alpha = 1
            cell.rating.alpha = 1
            cell.category.alpha = 1
            cell.numReviews.alpha = 1
            cell.numReplies.alpha = 1
            cell.dateApplied.alpha = 1
        })

        return cell
    }
    
    override func tableView(tableView: UITableView, willSelectRowAtIndexPath indexPath: NSIndexPath) -> NSIndexPath? {
        path = indexPath
        return indexPath
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let child = segue.destinationViewController as! JobView
        if searchActive {
            child.job = filteredApps[path.row].listing
            child.applicants = filteredApplicants[path.row]
        } else {
            child.job = myApplications[path.row].listing
            child.applicants = applicants[path.row]
        }
        child.cameFromMyApplications = true
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
    
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {

        if myApplications[indexPath.row].rejected {
            return true
        } else {
            return false
        }
    }
    
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        
        if editingStyle == UITableViewCellEditingStyle.Delete {
            myApplications[indexPath.row].delete()
            myApplications.removeAtIndex(indexPath.row)
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Fade)
        }
    }
    
    //back button pressed
    override func viewWillDisappear(animated: Bool) {
        parentView.tableView.reloadData()
        
        //self.tableView.reloadData()
    }
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        
        if count(searchText) == 0 {
            searchActive = false;
            self.tableView.reloadData()
            return
        } else {
            searchActive = true;
        }
        
        var index : Int = -1
        self.filteredApps = []
        self.filteredApplicants = []
        
        filteredData = data.filter {
            index++
            
            if ($0.listing.title as NSString).rangeOfString(searchText, options: .CaseInsensitiveSearch).location != NSNotFound {
                self.filteredApps.append(self.myApplications[index])
                self.filteredApplicants.append(self.applicants[index])
                return true
            }
            if ($0.listing.lister.username as NSString).rangeOfString(searchText, options: .CaseInsensitiveSearch).location != NSNotFound {
                self.filteredApps.append(self.myApplications[index])
                self.filteredApplicants.append(self.applicants[index])
                return true
            }
            if ($0.listing.category as NSString).rangeOfString(searchText, options: .CaseInsensitiveSearch).location != NSNotFound {
                self.filteredApps.append(self.myApplications[index])
                self.filteredApplicants.append(self.applicants[index])
                return true
            }
            //searching by description is causing problems for some reason
            if ($0.listing.jobDescription as NSString).rangeOfString(searchText, options: .CaseInsensitiveSearch).location != NSNotFound {
                self.filteredApps.append(self.myApplications[index])
                self.filteredApplicants.append(self.applicants[index])
                return true
            }
            return false
            
        }
        self.tableView.reloadData()
    }

    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
}
