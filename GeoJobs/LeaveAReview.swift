//
//  LeaveAReview.swift
//  GeoJobs
//
//  Created by Kory Kappel on 3/13/15.
//
//

import UIKit

class LeaveAReview: UIViewController {
    
    var child : LeaveAReviewTable!
    
    var listing : PSListing!
    var reviewedUser : PSUser!
    
    @IBAction func sendReview() {
        //require text
        self.child.reviewText.resignFirstResponder()
        let reviewText = self.child.reviewText.text
        
        if reviewText == "" {
            self.child.reviewText.becomeFirstResponder()
            return
        }
        
        PSReview(reviewText: self.child.reviewText.text, listing: listing, rating: self.child.floatRatingView.rating, reviewedUser: reviewedUser, reviewingUser: PSUser.currentUser()).saveInBackground()
        
        PSReview.queryReviewLeftForContractInBackground(listing, completion: { (reviewLeft) -> Void in
            if reviewLeft == true {
                //segue back to my job listings IF LISTER
                //else just segue to home
                if self.reviewedUser == self.listing.lister { //i'm the applicant, go Home
                    NSNotificationCenter.defaultCenter().postNotificationName("secondReviewLeftHOME", object: self)
                } else { //i'm the lister, go to My Job Listings
                    NSNotificationCenter.defaultCenter().postNotificationName("secondReviewLeft", object: self)
                }
                
                self.listing.status = "complete"
                self.listing.save()
            } else {
                //pop to my contracts, which is two screens back
                NSNotificationCenter.defaultCenter().postNotificationName("firstReviewLeft", object: self)
            }
        })
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        child = self.childViewControllers[0] as! LeaveAReviewTable
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
