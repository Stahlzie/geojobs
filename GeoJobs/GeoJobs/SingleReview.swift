//
//  SingleReview.swift
//  GeoJobs
//
//  Created by Kory Kappel on 3/10/15.
//
//

import UIKit

class SingleReview: UIViewController {

    var child : SingleReviewTable!
    
    var review : PSReview!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        child = self.childViewControllers[0] as! SingleReviewTable
        
        var actInd : UIActivityIndicatorView = UIActivityIndicatorView(frame: CGRectMake(0,0, 50, 50)) as UIActivityIndicatorView
        actInd.center = self.view.center
        actInd.hidesWhenStopped = true
        actInd.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.Gray
        view.addSubview(actInd)
        
        
        //START SPINNER:
        actInd.startAnimating()
        UIApplication.sharedApplication().beginIgnoringInteractionEvents()
        
        review.fetchIfNeededInBackgroundWithBlock { (review, error) -> Void in
            self.review = review as! PSReview
            actInd.stopAnimating()
            actInd.removeFromSuperview()
            UIApplication.sharedApplication().endIgnoringInteractionEvents()
        }
        
        view.addSubview(actInd)
        
        
        //START SPINNER:
        actInd.startAnimating()
        UIApplication.sharedApplication().beginIgnoringInteractionEvents()
        review.reviewingUser.fetchIfNeededInBackgroundWithBlock { (user, error) -> Void in
            if let u = self.review {
                if let v = u.reviewingUser as PSUser! {
                    self.review.reviewingUser = user as! PSUser!
                }
            }
            actInd.stopAnimating()
            actInd.removeFromSuperview()
            UIApplication.sharedApplication().endIgnoringInteractionEvents()
        }
        
        child.reviewDescription.text = review.reviewText
        child.reviewRating.rating = Float(review.rating)
        var rating : Float = 0.0
        child.userRating.rating = Float(rating)
        dispatch_async(dispatch_get_global_queue(Int(QOS_CLASS_USER_INITIATED.value), 0)) {
            rating = (Float)(PSReview.queryAverageRatingOfUser(self.review.reviewingUser))  // Should this be in background?
            dispatch_async(dispatch_get_main_queue()) {
                self.child.userRating.rating = Float(rating)
            }
        }
        
        view.addSubview(actInd)
        
        
        //START SPINNER:
        actInd.startAnimating()
        UIApplication.sharedApplication().beginIgnoringInteractionEvents()
        PSReview.queryReviewCountInBackground(review.reviewingUser, completion: { (count, error) -> Void in
            let temp : Int = Int(count)
            self.child.numReviews.text = "(\(temp))"
            
            actInd.removeFromSuperview()
            UIApplication.sharedApplication().endIgnoringInteractionEvents()
        })
        dispatch_async(dispatch_get_global_queue(Int(QOS_CLASS_USER_INITIATED.value), 0)) {
            self.review.reviewingUser.fetchIfNeeded()
            dispatch_async(dispatch_get_main_queue()) {
                
            }
            
            self.child.username.text = "from \(self.review.reviewingUser.username)"
            self.review.reviewingUser.getProfileImageInBackground { (image) -> Void in
                self.child.profilePic.image = image
            }
            self.child.profilePic.layer.masksToBounds = false
            self.child.profilePic.layer.borderColor = UIColor.whiteColor().CGColor
            self.child.profilePic.layer.cornerRadius = 40
            self.child.profilePic.clipsToBounds = true
            
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
