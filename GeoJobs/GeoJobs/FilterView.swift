//
//  FilterView.swift
//  GeoJobs
//
//  Created by Kory Kappel on 4/13/15.
//
//

import UIKit

class FilterView: UITableViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    var sortingOptions = ["category", "distance", "price (low to high)", "price (high to low)"]
    
    var child : FilterSubTable!
    var parentView : BrowseView!

    @IBOutlet weak var zipCodeField: UITextField!
    @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var milesLabel: UILabel!
    @IBOutlet weak var sortingPicker: UIPickerView!
    
    @IBAction func sliderValueChanged(sender: UISlider) {
        var formatter = NSNumberFormatter()
        formatter.maximumFractionDigits = 1
        formatter.minimumFractionDigits = 1
        var str = formatter.stringFromNumber(sender.value)
        milesLabel.text = str! + " miles"
    }
    
    @IBAction func change(sender: AnyObject) {
        let newZip = zipCodeField.text
        if count(newZip) != 5 {
            self.navigationItem.setHidesBackButton(true, animated: false)
        } else {
            self.navigationItem.setHidesBackButton(false, animated: false)
        }
    }
    
    @IBAction func resign() {
        child.active.resignFirstResponder()
    }
    
    //the following needs to be sent back to browse page upon dismissal
    var selectedCategories : [String]!
    //var radiusOfSearch : Float!
    //var sortByCategory : String!
    //var zipCodeForSearch : String!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        child = self.childViewControllers[0] as! FilterSubTable
        
        sortingPicker.delegate = self
        sortingPicker.dataSource = self
        
        tableView.backgroundColor = UIColor.groupTableViewBackgroundColor()
        
        //use passed-in persistent data to set initial view of filter page
        slider.value = parentView.radiusOfSearch
        var formatter = NSNumberFormatter()
        formatter.maximumFractionDigits = 1
        formatter.minimumFractionDigits = 1
        var str = formatter.stringFromNumber(parentView.radiusOfSearch)
        milesLabel.text = str! + " miles"
        switch parentView.sortByCategory {
            case "category":
                sortingPicker.selectRow(0, inComponent: 0, animated: true)
            case "distance":
                sortingPicker.selectRow(1, inComponent: 0, animated: true)
            case "price (low to high)":
                sortingPicker.selectRow(2, inComponent: 0, animated: true)
            case "price (high to low)":
                sortingPicker.selectRow(3, inComponent: 0, animated: true)
            default:
                sortingPicker.selectRow(0, inComponent: 0, animated: true)
        }
        let preferences = NSUserDefaults.standardUserDefaults()
        if preferences.boolForKey("useLocationServices") {
            zipCodeField.text = "Location services are on."
            zipCodeField.enabled = false
        } else {
            let zip = preferences.valueForKey("zipCode") as! String
            zipCodeField.text = zip
            zipCodeField.enabled = true
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return sortingOptions.count
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String! {
        return sortingOptions[row]
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let childView = segue.destinationViewController as! FilterSubTable
        childView.parentView = self
    }
    
    //will this only run when back button was pressed
    override func viewWillDisappear(animated: Bool) {
        
        //PASS BACK UP PERSISTENT DATA USING UPDATED STUFF IN THIS VIEW
        parentView.selectedCategories = selectedCategories
        parentView.radiusOfSearch = slider.value
        parentView.sortByCategory = sortingOptions[sortingPicker.selectedRowInComponent(0)]
        //if location services isn't being used, send back zip code:
        let preferences = NSUserDefaults.standardUserDefaults()
        if !preferences.boolForKey("useLocationServices") {
            parentView.zipCodeForSearch = zipCodeField.text
        }
        
        //SAVE ALL THESE TO PERSIST
        preferences.setValue(selectedCategories, forKey: "selectedSearchCategories")
        preferences.setValue(slider.value, forKey: "searchRadius")
        preferences.setValue(sortingOptions[sortingPicker.selectedRowInComponent(0)], forKey: "sortByCategory")
        if count(zipCodeField.text) == 5 {
            preferences.setValue(zipCodeField.text, forKey: "zipCode")
        }
        preferences.synchronize()
        
        
        parentView.reloadListingsArray()
    }
    
    override func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let label = UILabel()
        label.frame = CGRectMake(10, 7.5, tableView.frame.size.width, 15)
        label.font = UIFont.systemFontOfSize(12)
        //label.textAlignment = NSTextAlignment.Center
        label.textColor = UIColor.darkGrayColor()
        
        switch section {
        case 0:
            let preferences = NSUserDefaults.standardUserDefaults()
            if preferences.boolForKey("useLocationServices") {
                label.text = "You can change to use zip code in Settings"
            } else {
                label.text = "You can change to use location services in Settings"
            }
        case 1:
            label.text = "Change radius of search"
        case 2:
            label.text = "Change sorting options"
        case 3:
            label.text = "Select category for search"
        default:
            label.text = ""
        }
        
        
        
        let view = UIView()
        view.frame = CGRectMake(0, 0, tableView.frame.size.width, 40)
        view.backgroundColor = UIColor.groupTableViewBackgroundColor()
        view.addSubview(label)
        return view
    }
    
    override func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 40.0
    }
    
}
