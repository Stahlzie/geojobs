//
//  MyReviewsCell.swift
//  GeoJobs
//
//  Created by Kory Kappel on 3/25/15.
//
//

import UIKit

class MyReviewsCell: UITableViewCell {
    
    @IBOutlet weak var jobTitleandPrice : UILabel!
    @IBOutlet weak var reviewDescription : UITextView!
    @IBOutlet weak var starRating: FloatRatingView!
    @IBOutlet weak var category : UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
