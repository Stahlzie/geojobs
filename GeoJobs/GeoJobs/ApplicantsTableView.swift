//
//  ApplicantsTableView.swift
//  GeoJobs
//
//  Created by Kory Kappel on 3/26/15.
//
//

import UIKit

class ApplicantsTableView: UITableViewController {
    
    var applications : [PSApplication] = []
    var path = NSIndexPath(forRow: 0, inSection: 0)
    var data : [CellData]! = []
    
    struct CellData {
        var application : PSApplication!
        var user : PSUser!
        var reviewCount : Int
        var rating : Double
        var offer : Double
        var pitch : String
        var createdAt : NSDate
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        navigationItem.title = "Applicants"
        dispatch_async(dispatch_get_global_queue(Int(QOS_CLASS_USER_INITIATED.value), 0)) {
            self.loadData()
            dispatch_async(dispatch_get_main_queue()) {
                self.tableView.reloadData()
            }
        }
    }
    
    func loadData() {
        for app in applications {
            var cd : CellData = CellData(application: nil, user: nil, reviewCount: 0, rating: 0, offer: 0.0, pitch: "", createdAt: NSDate())
            app.fetchIfNeeded()
            cd.createdAt = app.createdAt
            cd.application = app
            cd.user = app.applicant.fetchIfNeeded() as! PSUser!
            cd.reviewCount = PSReview.queryReviewCount(cd.user)
            cd.rating = PSReview.queryAverageRatingOfUser(cd.user)
            cd.pitch = app.pitch
            cd.offer = app.offer.doubleValue
            data.append(cd)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return min(applications.count,data.count)
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as! ApplicantsCell
        // WARN
        // Configure the cell...
        let cellData = data[indexPath.row]
        // WARN
        cell.username.text = cellData.user.username
        cell.numReviews.text = "(\(cellData.reviewCount))"
        cell.starRating.rating = Float(cellData.rating)
        var formatter = NSNumberFormatter()
        formatter.maximumFractionDigits = 2
        formatter.minimumFractionDigits = 2
        let str = formatter.stringFromNumber(cellData.offer)
        cell.price.text = "$\(str!)"
        cell.pitch.text = cellData.pitch
        cell.timePosted.text = calculateTimeSinceJobPosting(cellData.createdAt)
        
        
        return cell
    }
    
    func calculateTimeSinceJobPosting(jobDate: NSDate) -> String {
        let currentDate = NSDate()
        var calendar : NSCalendar = NSCalendar.currentCalendar()
        let components = calendar.components(NSCalendarUnit.CalendarUnitYear | NSCalendarUnit.CalendarUnitMonth | NSCalendarUnit.CalendarUnitWeekOfYear | NSCalendarUnit.CalendarUnitDay | NSCalendarUnit.CalendarUnitHour | NSCalendarUnit.CalendarUnitMinute, fromDate: jobDate, toDate: currentDate, options: nil)
        
        if components.year >= 1 {
            if components.year < 2 {
                return "1 year ago"
            } else {
                return "\(components.year) years ago"
            }
        } else if components.month >= 1 {
            if components.month < 2 {
                return "1 month ago"
            } else {
                return "\(components.month) months ago"
            }
        } else if components.weekOfYear >= 1 {
            if components.weekOfYear < 2 {
                return "1 week ago"
            } else {
                return "\(components.weekOfYear) weeks ago"
            }
        } else if components.day >= 1 {
            if components.day < 2 {
                return "1 day ago"
            } else {
                return "\(components.day) days ago"
            }
        } else if components.hour >= 1 {
            if components.hour < 2 {
                return "1 hour ago"
            } else {
                return "\(components.hour) hours ago"
            }
        } else {
            if components.minute >= 2 {
                return "\(components.minute) minutes ago"
            } else if components.minute < 2 && components.minute >= 1 {
                return "1 minute ago"
            } else {
                return "less than a minute ago"
            }
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let child = segue.destinationViewController as! ApplicationView
        child.app = applications[path.row]
    }
    
    override func tableView(tableView: UITableView, willSelectRowAtIndexPath indexPath: NSIndexPath) -> NSIndexPath? {
        
        path = indexPath
        return indexPath
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

}
