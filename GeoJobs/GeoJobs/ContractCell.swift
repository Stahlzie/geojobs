//
//  ContractCell.swift
//  GeoJobs
//
//  Created by Steven Patterson on 4/4/15.
//
//

import UIKit

class ContractCell: UITableViewCell {
    
    @IBOutlet var titleAndPrice : UILabel!
    @IBOutlet var username : UILabel!
    @IBOutlet var date : UILabel!
    @IBOutlet var category : UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
