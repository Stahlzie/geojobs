//
//  SettingsView.swift
//  GeoJobs
//
//  Created by Steven Patterson on 3/3/15.
//
//

import UIKit

class SettingsView: UITableViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    var focusedField : UITextField!
    var parentView : HomeView!
    
    var active : UITextField!
    
    @IBAction func anyFieldSelected(sender: UITextField) {
        active = sender
    }
    
    @IBAction func resign() {
        active.resignFirstResponder()
    }
    
    @IBOutlet weak var profileImage: UIImageView!
    
    @IBAction func focus(sender: AnyObject) {
        focusedField = sender as! UITextField
    }
    @IBAction func defocus(sender: AnyObject) {
        focusedField.resignFirstResponder()
    }
    @IBAction func change(sender: AnyObject) {
        let newZip = zipCodeTextField.text
        let defaults = NSUserDefaults.standardUserDefaults()
        defaults.setValue(newZip, forKey: "zipCode")
        defaults.synchronize()
        
        if count(newZip) != 5 {
            self.navigationItem.setHidesBackButton(true, animated: false)
        } else {
            self.navigationItem.setHidesBackButton(false, animated: false)
        }
    }
    @IBOutlet weak var zipCodeCell: UITableViewCell!
    @IBOutlet weak var zipCodeTextField: UITextField!
    @IBOutlet weak var locationServicesSwitch: UISwitch!
    @IBAction func switchedLocationServices(sender: AnyObject) {
        let newState = locationServicesSwitch.on
        let defaults = NSUserDefaults.standardUserDefaults()
        defaults.setBool(newState, forKey: "useLocationServices")
        defaults.synchronize()
        
        if newState {
            zipCodeCell.hidden = true
//            zipCodeCell.userInteractionEnabled = false
//            zipCodeTextField.enabled = false
//            zipCodeTextField.text = ""
            self.navigationItem.setHidesBackButton(false, animated: false)
        } else {
            zipCodeCell.hidden = false
//            zipCodeCell.userInteractionEnabled = true
//            zipCodeTextField.enabled = true
            zipCodeTextField.text = defaults.valueForKey("zipCode") as! String
            if count(zipCodeTextField.text) != 5 {
                self.navigationItem.setHidesBackButton(true, animated: false)
            }
        }
    }
    @IBOutlet weak var rememberMeSwitch: UISwitch!
    @IBAction func switchedRememberMe(sender: AnyObject) {
        let newState = rememberMeSwitch.on
        let defaults = NSUserDefaults.standardUserDefaults()
        defaults.setBool(newState, forKey: "rememberMe")
        defaults.synchronize()
    }
    
    //var settingsText = ["Use Location Services", "Stay Logged In"]
    
    // Thanks to http://carl-thomas.name/crop-image-to-square/
    func cropToSquare(image originalImage: UIImage) -> UIImage {
        // Create a copy of the image without the imageOrientation property so it is in its native orientation (landscape)
        let contextImage: UIImage = UIImage(CGImage: originalImage.CGImage)!
        
        // Get the size of the contextImage
        let contextSize: CGSize = contextImage.size
        
        let posX: CGFloat
        let posY: CGFloat
        let width: CGFloat
        let height: CGFloat
        
        // Check to see which length is the longest and create the offset based on that length, then set the width and height of our rect
        if contextSize.width > contextSize.height {
            posX = ((contextSize.width - contextSize.height) / 2)
            posY = 0
            width = contextSize.height
            height = contextSize.height
        } else {
            posX = 0
            posY = ((contextSize.height - contextSize.width) / 2)
            width = contextSize.width
            height = contextSize.width
        }
        
        let rect: CGRect = CGRectMake(posX, posY, width, height)
        
        // Create bitmap image from context using the rect
        let imageRef: CGImageRef = CGImageCreateWithImageInRect(contextImage.CGImage, rect)
        
        // Create a new image based on the imageRef and rotate back to the original orientation
        let image: UIImage = UIImage(CGImage: imageRef, scale: originalImage.scale, orientation: originalImage.imageOrientation)!
        
        return image
    }
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let defaults = NSUserDefaults.standardUserDefaults()
        if let useLocationServices = defaults.boolForKey("useLocationServices") as Bool! {
            locationServicesSwitch.setOn(useLocationServices, animated: false)
            if useLocationServices {
                zipCodeCell.hidden = true
//                zipCodeTextField.enabled = false
//                zipCodeCell.userInteractionEnabled = false
            } else {
                zipCodeCell.hidden = false
//                zipCodeCell.userInteractionEnabled = true
//                zipCodeTextField.enabled = true
                zipCodeTextField.text = defaults.valueForKey("zipCode") as! String
            }
        }
        if let rememberMe = defaults.boolForKey("rememberMe") as Bool! {
            rememberMeSwitch.setOn(rememberMe, animated: false)
        }
        
        profileImage.layer.masksToBounds = false
        profileImage.layer.borderColor = UIColor.whiteColor().CGColor
        profileImage.layer.cornerRadius = 49
        //profileImage.layer.cornerRadius = profileImage.frame.size.height/2
        profileImage.clipsToBounds = true
        PSUser.currentUser().getProfileImageInBackground { (image) -> Void in
            self.profileImage.image = image
        }
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        //if "Log Out" was pressed, pop off the nav controller (will go back to last start screen it was on)
        if indexPath.section == 2 && indexPath.row == 0 {
            //dismissViewControllerAnimated(true, completion: nil) //THIS REMOVES THE ENTIRE NAV CONTROLLER
            NSNotificationCenter.defaultCenter().postNotificationName("logOut", object: self)
            dispatch_async(dispatch_get_global_queue(Int(QOS_CLASS_USER_INITIATED.value), 0)) {
                PSUser.logOut()
            }
        }
        //change profile image
        if indexPath.section == 3 && indexPath.row == 1 {
            var imagePicker = UIImagePickerController()
            imagePicker.modalPresentationStyle = UIModalPresentationStyle.CurrentContext
            imagePicker.delegate = self
            self.presentViewController(imagePicker, animated: true, completion: nil)
        }
    }
    
    //image picking code adapted from: http://stackoverflow.com/questions/24625687/swift-uiimagepickercontroller-how-to-use-it
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [NSObject : AnyObject]) {
        var tempImage:UIImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        tempImage = cropToSquare(image: tempImage)
        //if the image was taken with the iPhone's camera, its default orientation will be LandscapeLeft, so we need to rotate it to be normal
        if tempImage.imageOrientation.rawValue == 3 {
            //rotate right
            tempImage = tempImage.imageRotatedByDegrees(90, flip: false)
        }
        profileImage.image  = tempImage
        parentView.profilePicture.image = tempImage
        //SET USER'S IMAGE TO tempImage IN PARSE:
        PSUser.currentUser().setProfileImage(tempImage)
        PSUser.currentUser().saveInBackground()
        
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }

    /*
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 2
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return 1
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("settingsCell", forIndexPath: indexPath) as SettingsCell

        // Configure the cell...
        //cell.label.text = settingsText[indexPath.section]
        cell.switcher.setOn(true, animated: false)
        

        return cell
    }
    
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return ""
    }
*/

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

}
