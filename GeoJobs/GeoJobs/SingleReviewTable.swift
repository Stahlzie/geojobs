//
//  SingleReviewTable.swift
//  GeoJobs
//
//  Created by Kory Kappel on 3/11/15.
//
//

import UIKit

class SingleReviewTable: UITableViewController {

    @IBOutlet weak var username : UILabel!
    @IBOutlet weak var numReviews : UILabel!
    @IBOutlet weak var userRating : FloatRatingView!
    @IBOutlet weak var reviewRating : FloatRatingView!
    @IBOutlet weak var profilePic : UIImageView!
    @IBOutlet weak var reviewDescription : UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
