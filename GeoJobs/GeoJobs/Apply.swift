//
//  Apply.swift
//  GeoJobs
//
//  Created by Kory Kappel on 3/2/15.
//
//

import UIKit

protocol ChildDelegate {
    func applicationSubmitted(childFromDelegate: Apply)
}

class Apply: UIViewController {
    @IBAction func submitButton(sender: UIButton) {
        child.priceField.resignFirstResponder()
        child.pitch.resignFirstResponder()
        if child.priceField.text == "" {
            child.priceField.becomeFirstResponder()
            return
        } else if child.pitch.text == "" {
            child.pitch.becomeFirstResponder()
            return
        }
        delegate.applicationSubmitted(self)
    }
    
    @IBAction func viewTapped() {
        child.pitch.resignFirstResponder()
        child.priceField.resignFirstResponder()
    }
    
    var child : ApplyTable!
    var jobDescription : String!
    var delegate : ChildDelegate!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        child = self.childViewControllers[0] as! ApplyTable
        
        child.jobDescription.text = jobDescription
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
