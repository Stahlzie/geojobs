//
//  User.swift
//  GeoJobs
//
//  Created by Nathan Snyder on 2015-03-21.
//
//
import Foundation

public class PSUser : PFUser, PFSubclassing {
    
    override public class func initialize() {
        var onceToken : dispatch_once_t = 0;
        dispatch_once(&onceToken) {
            //self.registerSubclass()
        }
    }
    
    /*
    // Initializers
    */
    
    public override init() {
        super.init()
    }
    
    /**
        Initializes basic user info. If possible, use another init
    */
    public init(username: String, email: String, password: String) {
        super.init()
        
        self.username = username
        self.email = email
        self.password = password
    }
    
    /**
        Initializes full PSUser.
    */
    public init(username: String, email: String, password: String, realName: String, phoneNumber: String) {
        super.init()
        
        self.username = username
        self.email = email
        self.password = password
        self.realName = realName
        self.phoneNumber = phoneNumber
    }
    
    /*
    // Class Functions
    */
    
    /**
        Replaces class func User.getUser()
    
        :param: username Username of PFUser on which to query
    
        :return: Returns a PSUser!
    */
    public class func queryUser(username: String) -> PSUser! { // Todo: Write Test
        let query = PFUser.query()
        query.whereKey("username", equalTo: username)
        let user = query.getFirstObject()
        if user == nil {
            return nil
        } else {
            return user as! PSUser!
        }
    }
    
    /**
        Replaces class func User.getUser()
    
        :param: username Username of PFUser on which to query
    
        :return: Void Runs in background and returns with a completion block of (PSUser!)
    */
    public class func queryUserInBackground(username: String, completion: (user: PSUser!) -> Void) { // Todo: Write Test
        let query = PFUser.query()
        query.whereKey("username", equalTo: username)
        query.getFirstObjectInBackgroundWithBlock { (user, error) -> Void in
            if(error != nil) {  // There is an error
                completion(user: nil)
            } else {
                completion(user: (user: user as! PSUser))
            }
        }
    }
    
    /*
    // Properties
    */
    
    @NSManaged var profileImg: PFFile?

    /**
        Uses the PFFile profileImageFile and converts it to a usable UIImage if it exist. If it does not exist, it returns the default user profile picture.
    */
    public func getProfileImageInBackground(completion: (image: UIImage!) -> Void) {
        if profileImg != nil {
            var error = NSErrorPointer()
            profileImg!.getDataInBackgroundWithBlock({ (data, error) -> Void in
                if error == nil && data != nil {
                    completion(image: UIImage(data:data)!)
                } else {
                    completion(image: UIImage(named: "ProfilePlaceholderSuit.png")!)
                }
            })
        }
        else {
            completion(image: UIImage(named: "ProfilePlaceholderSuit.png")!)
        }
    }
    public func setProfileImage(newValue: UIImage) {
        let pngData = UIImagePNGRepresentation(newValue)
        profileImg = PFFile(data: pngData)
    }
    
//    var profileUIImage: UIImage?
//    /**
//    Uses the PFFile profileImageFile and converts it to a usable UIImage if it exist. If it does not exist, it returns the default user profile picture.
//    */
//    var profileImage : UIImage {
//        get {
//            if profileUIImage != nil { // we have loaded an image before
//                return profileUIImage!
//            }
//            
//            if profileImg != nil {
//                var error = NSErrorPointer()
//                var imageData = profileImg!.getData(error)  // warnBlockingOperationOnMainThread
//                if error == nil {
//                    profileUIImage = UIImage(data:imageData)!
//                } else {
//                    var defaultPath = NSBundle.mainBundle().pathForResource("ProfilePlaceholderSuit", ofType: "png")
//                    profileUIImage = UIImage(contentsOfFile: defaultPath!)!
//                }
//            }
//            else {
//                // Todo: Return default image
//                var defaultPath = NSBundle.mainBundle().pathForResource("ProfilePlaceholderSuit", ofType: "png")
//                profileUIImage = UIImage(contentsOfFile: defaultPath!)!
//            }
//            
//            return profileUIImage!
//        }
//    }
    
    @NSManaged public var phoneNumber: String
    @NSManaged public var realName: String
    
}
