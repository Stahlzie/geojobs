//
//  ApplicationView.swift
//  GeoJobs
//
//  Created by Kory Kappel on 3/13/15.
//
//

import UIKit

class ApplicationView: UIViewController {
    
    var child : ApplicationTable!
    @IBOutlet var selectButton : UIButton!
    var app : PSApplication!
    
    @IBAction func buttonTapped() {
        var alertController = UIAlertController(title: "Confirm", message: "Are you sure you want to select this applicant?", preferredStyle: .Alert)
        
        // Create the actions
        var selectAction = UIAlertAction(title: "Select", style: UIAlertActionStyle.Default) {
            UIAlertAction in
            self.app.acceptInBackground { (success) -> Void in
                if success {
                    //this sends a notification to home screen that it needs to pop its views and go to My Contracts screen
                    NSNotificationCenter.defaultCenter().postNotificationName("applicantSelected", object: self)
                }
            }
        }
        var cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel) {
            UIAlertAction in
        }
        
        // Add the actions
        alertController.addAction(selectAction)
        alertController.addAction(cancelAction)
        
        // Present the controller
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        child = self.childViewControllers[0] as! ApplicationTable
        
        navigationItem.title = "Application"
        
        let applicant = app.applicant
        child.username.text = applicant.username
        applicant.getProfileImageInBackground { (image) -> Void in
            self.child.profilePic.image = image
        }
        child.profilePic.layer.masksToBounds = false
        child.profilePic.layer.borderColor = UIColor.whiteColor().CGColor
        child.profilePic.layer.cornerRadius = 40
        child.profilePic.clipsToBounds = true
        child.pitch.text = app.pitch
        
        //display price with 2 decimal places
        var formatter = NSNumberFormatter()
        formatter.maximumFractionDigits = 2
        formatter.minimumFractionDigits = 2
        var str = formatter.stringFromNumber(app.offer)
        child.price.text = "$\(str!)"
        
        var actInd : UIActivityIndicatorView = UIActivityIndicatorView(frame: CGRectMake(0,0, 50, 50)) as UIActivityIndicatorView
        actInd.center = self.view.center
        actInd.hidesWhenStopped = true
        actInd.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.Gray
        view.addSubview(actInd)
        
        //only show "select applicant" button if application is "new" or "pending"
        if app.status != "new" && app.status != "pending" {
            selectButton.hidden = true
        }
        
        
        //START SPINNER:
        actInd.startAnimating()
        UIApplication.sharedApplication().beginIgnoringInteractionEvents()
        // WARN PLS
        dispatch_async(dispatch_get_global_queue(Int(QOS_CLASS_USER_INITIATED.value), 0)) {
            
            let rating = (Float)(PSReview.queryAverageRatingOfUser(self.app.applicant))
            dispatch_async(dispatch_get_main_queue()) {
                self.child.floatRatingView.rating = rating
            }
        }
        PSReview.queryReviewCountInBackground(app.applicant, completion: { (count, error) -> Void in
            let temp : Int = Int(count)
            self.child.numReviews.text = "(\(temp))"
            
            actInd.stopAnimating()
            UIApplication.sharedApplication().endIgnoringInteractionEvents()
            actInd.removeFromSuperview()
        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}
