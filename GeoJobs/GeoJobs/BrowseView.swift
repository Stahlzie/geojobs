//
//  BrowseView.swift
//  GeoJobs
//
//  Created by Steven Patterson on 3/11/15.
//
//

import UIKit
import MapKit

class JobPinAnnotation : MKPointAnnotation {
    var relatedJob : PSListing!
    var indexOfJob : Int!
}

class BrowseView: UIViewController, MKMapViewDelegate, UITableViewDataSource, UITableViewDelegate, JobViewDelegate, UISearchBarDelegate {
    
    @IBOutlet var map : MKMapView!
    @IBOutlet var tableView : UITableView!
    
    
    var listings : [PSListing] = []
    var ratings : [Float] = []
    var reviewCount : [Int] = []
    var path = NSIndexPath(forRow: 0, inSection: 0)
    var userLocation : PFGeoPoint!
    var jobPins : [JobPinAnnotation] = []
    var manager : CLLocationManager!
    var applicants = Array<Array<PSApplication>>() //pass this onto child
    var selectedPin : Int = 0
    
    //for searching
    var filteredListings : [PSListing] = []
    var filteredRatings : [Float] = []
    var filteredReviewCount : [Int] = []
    var filteredApplicants = Array<Array<PSApplication>>()
    var searchActive : Bool = false
    var searchBar : UISearchBar!
    
    
    //TO BE USED IN QUERY FOR LISTINGS
    var selectedCategories : [String]!
    var radiusOfSearch : Float!
    var sortByCategory : String!
    var zipCodeForSearch : String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.map.delegate = self
        self.tableView.dataSource = self
        map.showsUserLocation = true
        
        // Load user defaults
        let preferences = NSUserDefaults.standardUserDefaults()
        
        zipCodeForSearch = preferences.valueForKey("zipCode") as! String
        selectedCategories = preferences.valueForKey("selectedSearchCategories") as! [String]
        sortByCategory = preferences.valueForKey("sortByCategory") as! String
        radiusOfSearch = preferences.valueForKey("searchRadius") as! Float
        
        //creates a search bar and sets it to be hidden until user scrolls up
        searchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: self.tableView.frame.width, height: 44))
        self.tableView.tableHeaderView = searchBar
        dispatch_async(dispatch_get_main_queue()) {
            self.tableView.setContentOffset(CGPointMake(0.0, 44), animated: false)
        }
        searchBar.delegate = self
        searchBar.showsCancelButton = true
        
        reloadListingsArray()
    }
    
    func reloadListingsArray() {
        let preferences = NSUserDefaults.standardUserDefaults()
        let useLocation = preferences.boolForKey("useLocationServices")
        self.view.userInteractionEnabled = false
        //hides the search bar upon returning, IF empty
        if searchBar.text == "" {
            self.tableView.scrollRectToVisible(CGRectMake(0, 0, 1, 1), animated: false)
            self.tableView.setContentOffset(CGPointMake(0.0, 44), animated: false)
        }
        /*
        dispatch_async(dispatch_get_main_queue()) {
        if self.selectedCategories.count > 5 && self.selectedCategories.count < Constants.categories.count {
        let tooManyView = UIAlertView(title: "Search error", message: "Too many categories. Please 5 or less to narrow your search.", delegate: nil, cancelButtonTitle: "OK")
        tooManyView.show()
        }
        }
        */
        
        dispatch_async(dispatch_get_global_queue(Int(QOS_CLASS_USER_INITIATED.value), 0)) {
            
            
            if PSUser.currentUser() == nil {
                var geocoder = CLGeocoder()
                geocoder.geocodeAddressString(self.zipCodeForSearch, completionHandler: {(placemarks: [AnyObject]!, error: NSError!) -> Void in
                    if let placemark = placemarks?[0] as? CLPlacemark {
                        
                        self.userLocation = PFGeoPoint(location: placemark.location)
                        
                        
                        // PSListing.queryListingsWithinMilesWithCategoriesInBackground(self.selectedCategories, miles: self.radiusOfSearch, location: self.userLocation) { (listings) -> Void in
                        
                        
                        dispatch_async(dispatch_get_global_queue(Int(QOS_CLASS_USER_INITIATED.value), 0)) {
                            var listings : [PSListing]!
                            if self.selectedCategories.count == 0 {
                                listings = PSListing.queryListingsWithinMiles(self.radiusOfSearch, location: self.userLocation) as [PSListing]
                            } else {
                                listings = PSListing.queryListingsWithinMilesWithCategories(self.selectedCategories, miles: self.radiusOfSearch, location: self.userLocation) as [PSListing]
                            }
                            
                            
                            //NSLog(self.sortByCategory)
                            
                            self.map.removeAnnotations(self.map.annotations)
                            self.listings = listings
                            self.jobPins.removeAll(keepCapacity: false)
                            
                            //NSLog(listings.count.description)
                            self.sortListings(self.sortByCategory)
                            
                            
                            for listing in self.listings {
                                self.applicants.append(listing.queryApplications())
                            }
                            self.fetchOnce()
                            
                            for listing in self.listings {
                                // WARN
                                listing.fetchIfNeeded()
                                // WARN
                                //self.applicants.append(listing.queryApplications())
                                var loc = CLLocationCoordinate2D(latitude: listing.location.latitude, longitude: listing.location.longitude)
                                var pin = JobPinAnnotation()
                                pin.coordinate = loc
                                var formatter = NSNumberFormatter()
                                formatter.maximumFractionDigits = 2
                                formatter.minimumFractionDigits = 2
                                var str = formatter.stringFromNumber(listing.price)
                                pin.title = "\(listing.title) - $\(str!)"
                                // WARN
                                listing.lister.fetchIfNeeded()
                                pin.subtitle = "by \(listing.lister.username)"
                                pin.relatedJob = listing
                                pin.indexOfJob = self.jobPins.count
                                self.jobPins.append(pin)
                                dispatch_async(dispatch_get_main_queue()) {
                                    self.map.addAnnotation(pin)
                                }
                            }
                            //self.fetchOnce()
                            dispatch_async(dispatch_get_main_queue()) {
                                //self.tableView.reloadData()
                                //self.view.userInteractionEnabled = true
                            }
                            
                            //}
                            dispatch_async(dispatch_get_main_queue()) {
                                let loc = CLLocationCoordinate2D(latitude: self.userLocation.latitude, longitude: self.userLocation.longitude)
                                let span = MKCoordinateSpanMake(1,1)
                                let reg = MKCoordinateRegionMake(loc, span)
                                self.map.setRegion(reg, animated: false)
                                //self.view.userInteractionEnabled = true
                            }
                        }
                    }
                })
            } else if useLocation {
                self.manager = CLLocationManager()
                self.manager.desiredAccuracy = kCLLocationAccuracyBest
                self.manager.requestAlwaysAuthorization()
                //manager.startUpdatingLocation()
                
                PFGeoPoint.geoPointForCurrentLocationInBackground { (geoPoint, error) -> Void in
                    
                    if error != nil {
                        //do something to let them know they cant be found.
                        NSLog(error.description)
                        let locationErrorAlert = UIAlertView(title: "Location Error", message: "Could not get your location. Please try again, or search using a zip code instead.", delegate: nil, cancelButtonTitle: "OK")
                        locationErrorAlert.show()
                        self.userLocation = PFGeoPoint(latitude: 41.155232, longitude: -80.08427)
                    } else {
                        self.userLocation = geoPoint
                    }
                    
                    //PSListing.queryListingsWithinMilesWithCategoriesInBackground(self.selectedCategories, miles: self.radiusOfSearch, location: self.userLocation) { (listings) -> Void in
                    
                    dispatch_async(dispatch_get_global_queue(Int(QOS_CLASS_USER_INITIATED.value), 0)) {
                        var listings : [PSListing]!
                        if self.selectedCategories.count == 0 {
                            listings = PSListing.queryListingsWithinMiles(self.radiusOfSearch, location: self.userLocation) as [PSListing]
                        } else {
                            listings = PSListing.queryListingsWithinMilesWithCategories(self.selectedCategories, miles: self.radiusOfSearch, location: self.userLocation) as [PSListing]
                        }
                        self.map.removeAnnotations(self.map.annotations)
                        self.listings = listings
                        self.jobPins.removeAll(keepCapacity: false)
                        self.sortListings(self.sortByCategory)
                        
                        for listing in self.listings {
                            self.applicants.append(listing.queryApplications())
                        }
                        self.fetchOnce()
                        
                        for listing in self.listings {
                            listing.fetchIfNeeded()
                            //self.applicants.append(listing.queryApplications())
                            var loc = CLLocationCoordinate2D(latitude: listing.location.latitude, longitude: listing.location.longitude)
                            var pin = JobPinAnnotation()
                            pin.coordinate = loc
                            var formatter = NSNumberFormatter()
                            formatter.maximumFractionDigits = 2
                            formatter.minimumFractionDigits = 2
                            var str = formatter.stringFromNumber(listing.price)
                            pin.title = "\(listing.title) - $\(str!)"
                            listing.lister.fetchIfNeeded()
                            pin.subtitle = "by \(listing.lister.username)"
                            pin.relatedJob = listing
                            pin.indexOfJob = self.jobPins.count
                            self.jobPins.append(pin)
                            dispatch_async(dispatch_get_main_queue()) {
                                self.map.addAnnotation(pin)
                            }
                        }
                        //self.fetchOnce()
                        dispatch_async(dispatch_get_main_queue()) {
                            //self.tableView.reloadData()
                            //self.view.userInteractionEnabled = true
                        }
                    }
                    
                    let loc = CLLocationCoordinate2D(latitude: self.userLocation.latitude, longitude: self.userLocation.longitude)
                    let span = MKCoordinateSpanMake(1,1)
                    let reg = MKCoordinateRegionMake(loc, span)
                    self.map.setRegion(reg, animated: false)
                    //self.view.userInteractionEnabled = true
                }
            } else {
                var address = preferences.valueForKey("zipCode") as! String
                var geocoder = CLGeocoder()
                dispatch_async(dispatch_get_global_queue(Int(QOS_CLASS_USER_INITIATED.value), 0)) {
                    geocoder.geocodeAddressString(address) { (placemarks: [AnyObject]!, error: NSError!) -> Void in
                        if let e = error {
                            NSLog(e.description)
                        }
                        if let placemark = placemarks?[0] as? CLPlacemark {
                            
                            self.userLocation = PFGeoPoint(location: placemark.location)
                            
                            // PSListing.queryListingsWithinMilesWithCategoriesInBackground(self.selectedCategories, miles: self.radiusOfSearch, location: self.userLocation) { (listings) -> Void in
                            
                            var listings : [PSListing]!
                            dispatch_async(dispatch_get_global_queue(Int(QOS_CLASS_USER_INITIATED.value), 0)) {
                                if self.selectedCategories.count == 0 {
                                    listings = PSListing.queryListingsWithinMiles(self.radiusOfSearch, location: self.userLocation) as [PSListing]
                                } else {
                                    listings = PSListing.queryListingsWithinMilesWithCategories(self.selectedCategories, miles: self.radiusOfSearch, location: self.userLocation) as [PSListing]
                                }
                                
                                self.map.removeAnnotations(self.map.annotations)
                                self.listings = listings
                                self.jobPins.removeAll(keepCapacity: false)
                                self.sortListings(self.sortByCategory)
                                
                                for listing in self.listings {
                                    self.applicants.append(listing.queryApplications())
                                }
                                self.fetchOnce()
                                
                                for listing in self.listings {
                                    listing.fetchIfNeeded()
                                    //self.applicants.append(listing.queryApplications())
                                    var loc = CLLocationCoordinate2D(latitude: listing.location.latitude, longitude: listing.location.longitude)
                                    var pin = JobPinAnnotation()
                                    pin.coordinate = loc
                                    var formatter = NSNumberFormatter()
                                    formatter.maximumFractionDigits = 2
                                    formatter.minimumFractionDigits = 2
                                    var str = formatter.stringFromNumber(listing.price)
                                    pin.title = "\(listing.title) - $\(str!)"
                                    listing.lister.fetchIfNeeded()
                                    pin.subtitle = "by \(listing.lister.username)"
                                    pin.relatedJob = listing
                                    pin.indexOfJob = self.jobPins.count
                                    self.jobPins.append(pin)
                                    dispatch_async(dispatch_get_main_queue()) {
                                        self.map.addAnnotation(pin)
                                    }
                                }
                                //self.fetchOnce()
                                dispatch_async(dispatch_get_main_queue()) {
                                    //self.tableView.reloadData()
                                    //self.view.userInteractionEnabled = true
                                }
                                //}
                                dispatch_async(dispatch_get_main_queue()) {
                                    let loc = CLLocationCoordinate2D(latitude: self.userLocation.latitude, longitude: self.userLocation.longitude)
                                    let span = MKCoordinateSpanMake(1,1)
                                    let reg = MKCoordinateRegionMake(loc, span)
                                    self.map.setRegion(reg, animated: false)
                                    //self.view.userInteractionEnabled = true
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    func sortListings(option: String) {
        if searchActive {
            switch option {
                
            case "price (low to high)":
                self.filteredListings.sort({ $0.price.doubleValue < $1.price.doubleValue })
            case "price (high to low)":
                filteredListings.sort({ $0.price.doubleValue > $1.price.doubleValue })
            case "category":
                filteredListings.sort({ $0.category < $1.category })
            case "distance":
                filteredListings.sort({ $0.location.distanceInMilesTo(self.userLocation) < $1.location.distanceInMilesTo(self.userLocation) })
            default:
                return
            }
        } else {
            switch option {
                
            case "price (low to high)":
                self.listings.sort({ $0.price.doubleValue < $1.price.doubleValue })
            case "price (high to low)":
                listings.sort({ $0.price.doubleValue > $1.price.doubleValue })
            case "category":
                listings.sort({ $0.category < $1.category })
            case "distance":
                listings.sort({ $0.location.distanceInMilesTo(self.userLocation) < $1.location.distanceInMilesTo(self.userLocation) })
            default:
                return
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchActive {
            return filteredListings.count
        } else {
            return listings.count
        }
    }
    
    func fetchOnce() {
        dispatch_async(dispatch_get_global_queue(Int(QOS_CLASS_USER_INITIATED.value), 0)) {
            self.ratings = []
            self.reviewCount = []
            for (index, listing) in enumerate(self.listings) {
                listing.lister.fetchIfNeeded()
                self.ratings.append(Float(PSReview.queryAverageRatingOfUser(listing.lister)))
                self.reviewCount.append(PSReview.queryReviewCount(listing.lister))
            }
            dispatch_async(dispatch_get_main_queue()) {
                if self.searchActive {
                    //manually trigger the filtering function to set filteredListings after just updating listings
                    self.searchBar(self.searchBar, textDidChange: self.searchBar.text) //will call reloadData when it's done
                } else {
                    self.tableView.reloadData()
                }
                self.view.userInteractionEnabled = true
            }
        }
        
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCellWithIdentifier("browseCell", forIndexPath: indexPath) as! BrowseCell
        
        cell.titleAndPrice.alpha = 0
        cell.userAndRating.alpha = 0
        cell.rating.alpha = 0
        cell.numReviews.alpha = 0
        cell.distance.alpha = 0
        cell.numReplies.alpha = 0
        cell.category.alpha = 0
        
        //thisListing.lister.fetchIfNeeded()
        
        var formatter = NSNumberFormatter()
        formatter.maximumFractionDigits = 2
        formatter.minimumFractionDigits = 2
        
        var thisListing : PSListing!
        var numReplies : Int!
        if searchActive {
            thisListing = filteredListings[indexPath.row]
            if indexPath.row < filteredRatings.count {
                cell.rating.rating = filteredRatings[indexPath.row]
            }
            if indexPath.row < filteredReviewCount.count {
                cell.numReviews.text = "(\(filteredReviewCount[indexPath.row].description))"
            }
            numReplies = filteredApplicants[indexPath.row].count
        } else {
            thisListing = listings[indexPath.row]
            if indexPath.row < ratings.count {
                cell.rating.rating = ratings[indexPath.row]
            }
            if indexPath.row < reviewCount.count {
                cell.numReviews.text = "(\(reviewCount[indexPath.row].description))"
            }
            numReplies = applicants[indexPath.row].count
        }
        var str = formatter.stringFromNumber(thisListing.price)
        cell.titleAndPrice.text = "\(thisListing.title) - $\(str!)"
        cell.userAndRating.text = "by \(thisListing.lister.username)"
        
        
        str = formatter.stringFromNumber(thisListing.location.distanceInMilesTo(userLocation))
        cell.distance.text = str! + " mi";
        
        if numReplies == 1 {
            cell.numReplies.text = "\(numReplies) reply"
        } else {
            cell.numReplies.text = "\(numReplies) replies"
        }
        cell.category.text = thisListing.category
        
        UIView.animateWithDuration(0.6, animations: { () -> Void in
            cell.titleAndPrice.alpha = 1
            cell.userAndRating.alpha = 1
            cell.rating.alpha = 1
            cell.numReviews.alpha = 1
            cell.distance.alpha = 1
            cell.numReplies.alpha = 1
            cell.category.alpha = 1
        })
        
        return cell
    }
    
    func mapView(mapView: MKMapView!, viewForAnnotation annotation: MKAnnotation!) -> MKAnnotationView! {
        if annotation.isKindOfClass(MKUserLocation) {
            return nil
        }
        
        if annotation.isKindOfClass(MKPointAnnotation) {
            var pinView : MKPinAnnotationView! = mapView.dequeueReusableAnnotationViewWithIdentifier("jobPin") as! MKPinAnnotationView!
            if pinView != nil {
                pinView.annotation = annotation
            } else {
                pinView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: "jobPin")
                pinView.canShowCallout = true
                pinView.enabled = true
                pinView.animatesDrop = true
                pinView.rightCalloutAccessoryView = UIButton.buttonWithType(UIButtonType.InfoDark) as! UIView
            }
            return pinView
        }
        return nil
    }
    
    func mapView(mapView: MKMapView!, annotationView view: MKAnnotationView!, calloutAccessoryControlTapped control: UIControl!) {
        let annotation = view.annotation
        
        if annotation.isKindOfClass(JobPinAnnotation) {
            let annotation = annotation as! JobPinAnnotation
            selectedPin = annotation.indexOfJob
            performSegueWithIdentifier("pinToJobSegue", sender: nil)
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "filterSegue" {
            searchBar.resignFirstResponder()
            let child = segue.destinationViewController as! FilterView
            child.parentView = self
            //pass persistent data
            child.selectedCategories = selectedCategories
        } else if segue.identifier == "pinToJobSegue" {
            let child = segue.destinationViewController as! JobView
            child.job = listings[selectedPin]
            child.applicants = applicants[selectedPin]
            child.delegate = self
            child.index = path.row
        } else { //will be jobView
            let child = segue.destinationViewController as! JobView
            if searchActive {
                child.job = filteredListings[path.row]
                child.applicants = filteredApplicants[path.row]
            } else {
                child.job = listings[path.row]
                child.applicants = applicants[path.row]
            }
            child.delegate = self
            child.index = path.row
        }
    }
    
    func tableView(tableView: UITableView, willSelectRowAtIndexPath indexPath: NSIndexPath) -> NSIndexPath? {
        
        path = indexPath
        return indexPath
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
    
    //runs when Job Details view returns back -- updates info for that job in the table
    func updateJob(job: PSListing, index: Int) {
        if searchActive {
            filteredListings[index] = job
            dispatch_async(dispatch_get_global_queue(Int(QOS_CLASS_USER_INITIATED.value), 0)) {
                self.filteredApplicants[index] = job.queryApplications()
                dispatch_async(dispatch_get_main_queue()) {
                    self.tableView.reloadData()
                }
            }
        } else {
            listings[index] = job
            dispatch_async(dispatch_get_global_queue(Int(QOS_CLASS_USER_INITIATED.value), 0)) {
                self.applicants[index] = job.queryApplications()
                dispatch_async(dispatch_get_main_queue()) {
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        
        if count(searchText) == 0 {
            searchActive = false;
            self.tableView.reloadData()
            return
        } else {
            searchActive = true;
        }
        
        var index : Int = -1
        self.filteredRatings = []
        self.filteredReviewCount = []
        self.filteredApplicants = []
        dispatch_async(dispatch_get_global_queue(Int(QOS_CLASS_USER_INITIATED.value), 0)) {
            self.filteredListings = self.listings.filter {
                index++
                
                if ($0.title as NSString).rangeOfString(searchText, options: .CaseInsensitiveSearch).location != NSNotFound {
                    self.filteredRatings.append(self.ratings[index])
                    self.filteredReviewCount.append(self.reviewCount[index])
                    self.filteredApplicants.append(self.applicants[index])
                    return true
                }
                if ($0.category as NSString).rangeOfString(searchText, options: .CaseInsensitiveSearch).location != NSNotFound {
                    self.filteredRatings.append(self.ratings[index])
                    self.filteredReviewCount.append(self.reviewCount[index])
                    self.filteredApplicants.append(self.applicants[index])
                    return true
                }
                if ($0.lister.username as NSString).rangeOfString(searchText, options: .CaseInsensitiveSearch).location != NSNotFound {
                    self.filteredRatings.append(self.ratings[index])
                    self.filteredReviewCount.append(self.reviewCount[index])
                    self.filteredApplicants.append(self.applicants[index])
                    return true
                }
                //also search by applicant name
                for applicant in self.applicants[index] {
                    applicant.applicant.fetchIfNeeded()
                    if (applicant.applicant.username as NSString).rangeOfString(searchText, options: .CaseInsensitiveSearch).location != NSNotFound {
                        self.filteredRatings.append(self.ratings[index])
                        self.filteredReviewCount.append(self.reviewCount[index])
                        self.filteredApplicants.append(self.applicants[index])
                        return true
                    }
                }
                return false
                
            }
            dispatch_async(dispatch_get_main_queue()) {
                self.tableView.reloadData()
            }
        }
    }
    
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
}
