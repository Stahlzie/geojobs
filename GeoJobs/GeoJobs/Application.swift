//
//  Application.swift
//  GeoJobs
//
//  Created by Nathan Snyder on 2015-03-21.
//
//

import Foundation

public class PSApplication : PFObject, PFSubclassing {
//    struct ApplicationStatus {
//            let New = "new"
//            let Pending = "pending"
//            let Rejected = "rejected"
//            let Accepted = "accepted"
//            let RejectedSeen = "rejected seen"
//            let AcceptedSeen = "accepted seen"
//    }
    
    /*
    // Initializers
    */
    override public class func initialize() {
        var onceToken : dispatch_once_t = 0;
        dispatch_once(&onceToken) {
            //self.registerSubclass()
        }
    }
    
    override init() {
        super.init()
    }
    
    public init(pitch: String, offer: NSNumber, listing: PSListing) {
        super.init()
        
        let acl = PFACL()
        acl.setPublicReadAccess(true)
        acl.setPublicWriteAccess(true)
        
        self.pitch = pitch
        self.offer = offer
        self.listing = listing
        self.ACL = acl
        
        // Predefined
        applicant = PSUser.currentUser()
        status = "new"
    }
    
    /*
    // Instance Functions
    */
    
    /**
        Accept an application
    */
    
    public func accept() {
        listing.price = self.offer
        var apps = listing.queryApplications()
        listing.status = "new contract"
        for app in apps {
            app.status = "rejected"
        }
        status = "accepted"
        
        listing.save()
        PFObject.saveAll(apps)
    } // tested
    
    /**
        Accept an application
    */
    
    public func acceptInBackground(completion: (success: Bool) -> Void) {
        listing.price = self.offer
        var apps = listing.queryApplications() // WARN
        listing.status = "new contract"
        for app in apps {
            app.status = "rejected"
        }
        status = "accepted"
        
        var objects = apps as [PFObject]
        objects.append(listing)
        
        PFObject.saveAllInBackground(objects, block: { (success, error) -> Void in
            if (error==nil) {
                completion(success: success)
            }
        })
    } // NEEDS TESTED
    
    /*
    // Class Functions
    */
    
    public class func parseClassName() -> String! {
        return "Application"
    }
    
    /**
        Gets the number to display for my applications
    */
    public class func queryApplicationCountInBackground(completion: (count: Int32) -> Void) {
        let query = PSApplication.query()
        query.whereKey("status", equalTo: "rejected")
        query.whereKey("applicant", equalTo: PSUser.currentUser())
        
        query.countObjectsInBackgroundWithBlock {
            (count: Int32, error: NSError!) -> Void in
            if(error != nil) {
                completion(count: 0)
            } else {
                completion(count:count)
            }
        }
    } // NEEDS TESTED
    
    /**
        Gets the number to display for my applications
    */
    public class func queryApplicationCount() -> Int32 {
        let query = PSApplication.query()
        query.whereKey("status", equalTo: "rejected")
        query.whereKey("applicant", equalTo: PSUser.currentUser())
        
        
        let count = Int32(query.countObjects())
        return count
    } //tested
    
    /**
        Sets all rejected applications to seen
    */
    public class func setApplicationsSeen() {
        let query = PSApplication.query()
        query.whereKey("status", equalTo: "rejected")
        query.whereKey("applicant", equalTo: PSUser.currentUser())
        if var apps = query.findObjects() as? [PSApplication] {
        
            for app in apps {
                app.status = "rejected seen"
            }
            
            saveAll(apps)
        }
    } // tested
    
    public class func setApplicationsSeenInBackground() {
        let query = PSApplication.query()
        query.whereKey("status", equalTo: "rejected")
        query.whereKey("applicant", equalTo: PSUser.currentUser())
        var foundApplications = query.findObjects() as! [PSApplication]!
        
        if foundApplications == nil {
            return
        }
        
        for thisApp in foundApplications {
            thisApp.status = "rejected seen"
        }
        saveAllInBackground(foundApplications, block: nil)
        
    }
    
    /**
        Gets the number to display for my applications
    */
    public class func queryContractCountInBackground(completion: (count: Int32) -> Void) {
        let count = PSApplication.queryContractCount()
        completion(count: count)
    }
    
    /**
        Gets the number to display for my applications
    */
    public class func queryContractCount() -> Int32 {
        let query = PSApplication.query()
        query.whereKey("status", equalTo: "accepted")
        query.whereKey("applicant", equalTo: PSUser.currentUser())
        
        let count = Int32(query.countObjects())
        if(count < 0) {
            return Int32(0)
        } // else
        return count
    }
    
    /**
        Sets all contracts (accepted applications) to seen
    */
    public class func setContractsSeen() -> Bool {
        let queryAccepted = PSApplication.query()
        queryAccepted.whereKey("status", equalTo: "accepted")
        queryAccepted.whereKey("applicant", equalTo: PSUser.currentUser())
        var apps = queryAccepted.findObjects() as? [PSApplication]
        
        if apps == nil {
            return true
        }
        
        var newApps : [PSApplication] = []
        
        for app in apps! {
            app.status = "accepted seen"
            newApps.append(app)
        }
        
        return saveAll(apps)
    } // tested
    
    /**
    Sets all contracts (accepted applications) to seen
    */
    public class func setContractsSeenInBackground(completion: (success: Bool) -> Void) {
        let queryAccepted = PSApplication.query()
        queryAccepted.whereKey("status", equalTo: "accepted")
        queryAccepted.whereKey("applicant", equalTo: PSUser.currentUser())
        
        var newApps : [PSApplication] = []
        
        if var appsAcc = queryAccepted.findObjects() as? [PSApplication] {
            for app in appsAcc {
                app.status = "accepted seen"
                newApps.append(app)
            }
        }
        
//        let queryRejected = PSApplication.query()
//        queryRejected.whereKey("status", equalTo: "rejected")
//        queryRejected.whereKey("applicant", equalTo: PSUser.currentUser())
//        if var appsRej = queryRejected.findObjects() as? [PSApplication] {
//        
//            for app in appsRej {
//                app.status = "rejected seen"
//                newApps.append(app)
//            }
//        }
        
        if newApps.isEmpty {
            completion(success: true)
        }
        
        saveAllInBackground(newApps, block: { (success, error) -> Void in
            if(error != nil) {
                completion(success: false)
            } else {
                completion(success: success)
            }
        })
        
    }
    
    /**
        Get an array of PSApplication objects
    */
    
    public class func queryMyApplications() -> [PSApplication] {
        if(PSUser.currentUser() == nil) {
            return []
        }
        let rejectedQuery = PSApplication.query()
        rejectedQuery.whereKey("applicant", equalTo: PSUser.currentUser())
        let pendingQuery = PSApplication.query()
        pendingQuery.whereKey("applicant", equalTo: PSUser.currentUser())
        let newQuery = PSApplication.query()
        newQuery.whereKey("applicant", equalTo: PSUser.currentUser())
        
        // status is: "pending", "rejected" or "rejected seen"
        rejectedQuery.whereKey("status", hasPrefix: "rejected")
        pendingQuery.whereKey("status", equalTo: "pending")
        newQuery.whereKey("status", equalTo: "new")
        
        let query = PFQuery.orQueryWithSubqueries([rejectedQuery, pendingQuery, newQuery])
        
        if let apps = query.findObjects() as? [PSApplication] {
            return apps
        }
        else {
            return [PSApplication]()
        }
    }
    
    /**
        Get an array of PSApplication objects
    */
    
    public class func queryMyApplicationsInBackground(completion: (applications: [PSApplication]) -> Void) {
        if(PSUser.currentUser() == nil) {
            completion(applications: [])
        }
        let rejectedQuery = PSApplication.query()
        rejectedQuery.whereKey("applicant", equalTo: PSUser.currentUser())
        let pendingQuery = PSApplication.query()
        pendingQuery.whereKey("applicant", equalTo: PSUser.currentUser())
        let newQuery = PSApplication.query()
        newQuery.whereKey("applicant", equalTo: PSUser.currentUser())
        
        // status is: "pending", "rejected" or "rejected seen"
        rejectedQuery.whereKey("status", hasPrefix: "rejected")
        pendingQuery.whereKey("status", equalTo: "pending")
        newQuery.whereKey("status", equalTo: "new")
        let query = PFQuery.orQueryWithSubqueries([rejectedQuery, pendingQuery, newQuery])
        
        query.findObjectsInBackgroundWithBlock { (apps, error) -> Void in
            if(error == nil) {
                completion(applications: apps as! [PSApplication])
            } else {
                completion(applications: [])
            }
        }
    }
    
    public class func queryRejectedApplicationsCountInBackground(completion: (count: Int) -> Void) {
        
        let myNewRejectedApplications = PSApplication.query()
        myNewRejectedApplications.whereKey("status", equalTo: "rejected")
        myNewRejectedApplications.whereKey("applicant", equalTo: PSUser.currentUser())
        
        myNewRejectedApplications.countObjectsInBackgroundWithBlock {
            (count: Int32, error: NSError!) -> Void in
            if(error != nil) {
                completion(count: 0)
            } else {
                completion(count: Int(count) )
            }
        }
    }
    
    /*
    // Properties
    */
    
    @NSManaged public var pitch: String
    @NSManaged public var status: String
    @NSManaged public var offer: NSNumber
    @NSManaged public var applicant: PSUser
    @NSManaged public var listing: PSListing
    
    public var rejected: Bool {
        if(self.status != "rejected" && self.status != "rejected seen") {
            return false
        } else {
            return true
        }
    }
}