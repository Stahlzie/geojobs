//
//  ProfileHeaderCell.swift
//  GeoJobs
//
//  Created by Steven Patterson on 3/11/15.
//
//

import UIKit

class ProfileHeaderCell: UITableViewCell {
    
    @IBOutlet var profilePicture : UIImageView!
    @IBOutlet var username : UILabel!
    @IBOutlet var numReviews : UILabel!
    @IBOutlet var rating : FloatRatingView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        profilePicture.layer.masksToBounds = false
        profilePicture.layer.borderColor = UIColor.whiteColor().CGColor
        profilePicture.layer.cornerRadius = profilePicture.frame.size.height/2
        profilePicture.clipsToBounds = true
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
