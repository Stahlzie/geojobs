//
//  Listing.swift
//  GeoJobs
//
//  Created by Nathan Snyder on 2015-03-21.
//
//

import Foundation

public class PSListing : PFObject, PFSubclassing {
    /*
    let ACCEPTING_APPLICATIONS = "accepting"
    let NEW_CONTRACT = "new contract"
    let CONTRACT_IN_PROGRESS = "in progress"
    let COMPLETE = "complete"
    */
    
    override public class func initialize() {
        var onceToken : dispatch_once_t = 0;
        dispatch_once(&onceToken) {
            //self.registerSubclass()
        }
    }
    
    public override init() {
        super.init()
    }
    
    /**
        Initialize a new listing
    */
    public init(title: String,
        description: String,
        location: PFGeoPoint,
        price: NSNumber,
        lister: PSUser
        ) {
            super.init()
            
            let acl = PFACL()
            acl.setPublicReadAccess(true)
            acl.setPublicWriteAccess(true)
            
            self.title = title
            self.location = location
            self.price = price
            self.jobDescription = description
            self.status = "accepting"
            self.lister = lister
            self.ACL = acl
    } // tested
    
    /*
    // Class Functions
    */
    
    /**
        Returns name of Parse Class
    */
    public class func parseClassName() -> String! {
        return "Listing"
    }
    
    /**
        Get a Listing object with a given ID
    */
    public class func queryListingById(id: NSString) -> PSListing! {
        let query = PSListing.query()
        query.getObjectWithId(id as! String)
        var listing = query.getFirstObject()
        if(listing == nil) {
            return nil
        } else {
            return listing as! PSListing
        }
    } // Tested
    
    /**
        Get a Listing object with a given ID asynchronously
    */
    public class func queryListingByIdInBackground(id: String, completion: (listing: PSListing!) -> Void) {
        let query = PSListing.query()
        query.getObjectWithId(id)
        let listing = query.getFirstObject()
        if(listing == nil) {
            return completion(listing: nil)
        } else {
            return completion(listing: listing as! PSListing)
        }
    } // Tested
    
    /**
    Get the number of new listings for the current user
    */
    public class func queryNewContractCount() -> Int {
        
        let innerQuery = PSListing.query()
        innerQuery.whereKey("status", equalTo: "new contract")
        
        let myAcceptedApplications = PSApplication.query()
        myAcceptedApplications.whereKey("applicant", equalTo: PSUser.currentUser())
        myAcceptedApplications.whereKey("status", equalTo: "accepted")
        myAcceptedApplications.whereKey("listing", matchesQuery: innerQuery)
        
        let count =  myAcceptedApplications.countObjects()
        if count > 0 {
            PFQuery.clearAllCachedResults()
        }
        return count
    } // Tested
    
    /**
        Get the number of new listings for the current user
    */
    public class func queryNewContractCountInBackground(completion: (count: Int32) -> Void) {
        
        let innerQuery = PSListing.query()
        innerQuery.whereKey("status", equalTo: "new contract")
        
        let myAcceptedApplications = PSApplication.query()
        myAcceptedApplications.whereKey("applicant", equalTo: PSUser.currentUser())
        myAcceptedApplications.whereKey("status", equalTo: "accepted")
        myAcceptedApplications.whereKey("listing", matchesQuery: innerQuery)
        
        myAcceptedApplications.countObjectsInBackgroundWithBlock { (count, error) -> Void in
            if count > 0 {
                PFQuery.clearAllCachedResults()
            }
            completion(count: count)
        }
        
    } // Tested
    
    /**
    Get the number of new listings for the current user
    */
    public class func queryNewApplicationsCountInBackground(completion: (count: Int32) -> Void) {
        
        let innerQuery = PSListing.query()
        innerQuery.whereKey("lister", equalTo: PSUser.currentUser())
        
        let myNewApplications = PSApplication.query()
        myNewApplications.whereKey("status", equalTo: "new")
        myNewApplications.whereKey("listing", matchesQuery: innerQuery)
        
        myNewApplications.countObjectsInBackgroundWithBlock { (count, error) -> Void in
            completion(count: count)
        }
    } // NEEDS TESTED
    
    /**
    Get the number of new listings for the current user
    */
    public class func queryNewApplicationsCount() -> Int {
        
        let innerQuery = PSListing.query()
        innerQuery.whereKey("lister", equalTo: PSUser.currentUser())
        
        let myNewApplications = PSApplication.query()
        myNewApplications.whereKey("status", equalTo: "new")
        myNewApplications.whereKey("listing", matchesQuery: innerQuery)
        let count = myNewApplications.countObjects()
        return count
    } // NEEDS TESTED
    
    /**
        Count my listings
    */
    public class func queryMyListingsCountInBackground(completion: (count: Int32) -> Void) {
        
        let query = PSListing.query()
        
        let user = PSUser.currentUser()
        if(user == nil) {
            completion(count: 0)
        }
        
        // else
        query.whereKey("lister", equalTo: user)
        query.whereKey("status", equalTo: "accepting")
        let count = query.countObjects()
        completion(count: Int32(count))
    } // NEEDS TESTED
    
    /**
        Count my listings
    */
    public class func queryMyListingsCount() -> Int {
        
        let query = PSListing.query()
        
        let user = PSUser.currentUser()
        if(user == nil) {
            return 0
        }
        
        // else
        query.whereKey("lister", equalTo: user)
        query.whereKey("status", equalTo: "accepting")
        let count = query.countObjects()
        return count
    } // Tested
    
    /**
        Get an array of my listings
    */
    public class func queryMyListingsInBackground(local: Bool, completion: (listings: [PSListing]) -> Void) {
        var listings : [PSListing] = []
        
        let acceptingQuery = PSListing.query()
        let completeQuery = PSListing.query()
        let query = PFQuery.orQueryWithSubqueries([acceptingQuery, completeQuery])
        if local {
            acceptingQuery.fromLocalDatastore()
            completeQuery.fromLocalDatastore()
            query.fromLocalDatastore()
        }
        
        let user = PSUser.currentUser()
        if(user == nil) {
            completion(listings: listings)
        }
        
        // else
        acceptingQuery.whereKey("lister", equalTo: user)
        acceptingQuery.whereKey("status", equalTo: "accepting")
        completeQuery.whereKey("lister", equalTo: user)
        completeQuery.whereKey("status", equalTo: "complete")
        
        query.findObjectsInBackgroundWithBlock { (objects, error) -> Void in
            if objects == nil {
                completion(listings: [])
                return
            }
            for obj in objects {
                listings.append(obj as! PSListing)
            }
            completion(listings: listings)
        }
    } // Tested
    
    /**
    Get an array of my listings
    */
    public class func queryMyCompleteListingsInBackground(completion: (listings: [PSListing]) -> Void) {
        var listings : [PSListing] = []
        
        let query = PSListing.query()
        
        let user = PSUser.currentUser()
        if(user == nil) {
            completion(listings: listings)
        }
        
        // else
        query.whereKey("lister", equalTo: user)
        query.whereKey("status", equalTo: "complete")
        query.findObjectsInBackgroundWithBlock { (objects, error) -> Void in
            for obj in objects {
                listings.append(obj as! PSListing)
            }
            completion(listings: listings)
        }
    } // Tested
    
    /**
        Get an array of my listings
    */
    public class func queryMyListings() -> [PSListing] {
        var listings : [PSListing] = []
        
        let query = PSListing.query()
        query.limit = 10000
        
        let user = PSUser.currentUser()
        if(user == nil) {
            return listings
        }
        
        // else
        query.whereKey("lister", equalTo: user)
        query.whereKey("status", equalTo: "accepting")
        if var newListings = query.findObjects() as? [PSListing] {
            return newListings
        }
        else {
            return [PSListing]()
        }
    } // Tested
    
    /**
    Get an array of my listings
    */
    public class func queryMyCompleteListings() -> [PSListing] {
        var listings : [PSListing] = []
        
        let query = PSListing.query()
        query.limit = 10000
        
        let user = PSUser.currentUser()
        if(user == nil) {
            return listings
        }
        
        // else
        query.whereKey("lister", equalTo: user)
        query.whereKey("status", equalTo: "complete")
        if var newListings = query.findObjects() as? [PSListing] {
            return newListings
        }
        else {
            return [PSListing]()
        }
    } //NEEDS TESTED
    
    /**
        Get my contracts
    */
    
    public class func queryMyContracts() -> [PSListing] {
        let innerQuery1 = PSListing.query()
        innerQuery1.whereKey("status", equalTo: "in progress")
        let innerQuery2 = PSListing.query()
        innerQuery2.whereKey("status", equalTo: "new contract")
        let innerQuery = PFQuery.orQueryWithSubqueries([innerQuery1,innerQuery2])
        
        let myAcceptedApplications = PSApplication.query()
        myAcceptedApplications.whereKey("applicant", equalTo: PSUser.currentUser())
        myAcceptedApplications.whereKey("status", hasPrefix: "accepted")
        myAcceptedApplications.whereKey("listing", matchesQuery: innerQuery)
        myAcceptedApplications.selectKeys(["listing"])
        
        let myNewContracts = PSListing.query()
        myNewContracts.whereKey("lister", equalTo: PSUser.currentUser())
        myNewContracts.whereKey("status", equalTo: "new contract")
        let myInProgressContracts = PSListing.query()
        myInProgressContracts.whereKey("lister", equalTo: PSUser.currentUser())
        myInProgressContracts.whereKey("status", equalTo: "in progress")
        
        let finalQuery = PFQuery.orQueryWithSubqueries([myNewContracts, myInProgressContracts])
        var listings = finalQuery.findObjects() as! [PSListing]
        
        let acceptedApps = myAcceptedApplications.findObjects() as! [PSApplication]
        for app in acceptedApps {
            let listing = app.listing
            listing.fetchIfNeeded()
            listings.append(listing)
        }
        
        return listings
        
    } // Tested
    
    /**
        Get my contracts in background
    */
    
    public class func queryMyContractsInBackground(completion: (listings: [PSListing]) -> Void) {
        let innerQuery1 = PSListing.query()
        innerQuery1.whereKey("status", equalTo: "in progress")
        let innerQuery2 = PSListing.query()
        innerQuery2.whereKey("status", equalTo: "new contract")
        let innerQuery = PFQuery.orQueryWithSubqueries([innerQuery1,innerQuery2])
        
        let myAcceptedApplications = PSApplication.query()
        myAcceptedApplications.whereKey("applicant", equalTo: PSUser.currentUser())
        myAcceptedApplications.whereKey("status", hasPrefix: "accepted")
        myAcceptedApplications.whereKey("listing", matchesQuery: innerQuery)
        myAcceptedApplications.selectKeys(["listing"])
        
        let myNewContracts = PSListing.query()
        myNewContracts.whereKey("lister", equalTo: PSUser.currentUser())
        myNewContracts.whereKey("status", equalTo: "new contract")
        let myInProgressContracts = PSListing.query()
        myInProgressContracts.whereKey("lister", equalTo: PSUser.currentUser())
        myInProgressContracts.whereKey("status", equalTo: "in progress")
        
        let finalQuery = PFQuery.orQueryWithSubqueries([myNewContracts, myInProgressContracts])
        var listings = finalQuery.findObjects() as! [PSListing]!
        
        if listings == nil {
            listings = []
        }
        
        var acceptedApps = myAcceptedApplications.findObjects() as! [PSApplication]!
        
        if acceptedApps == nil {
            acceptedApps = []
        }
        for app in acceptedApps {
            let listing = app.listing
            listing.fetch()
            listings.append(listing)
        }
        
        completion(listings: listings)
        
    } // Tested
    
    /**
        Get a list of listings within a range
    */
    public class func queryListingsWithinMiles(miles: Float, location: PFGeoPoint) -> [PSListing] {
        var listings : [PSListing] = []
        
        let query = PSListing.query()
        query.whereKey("location", nearGeoPoint: location, withinMiles: Double(miles))
        query.whereKey("status", equalTo: "accepting")
//        if let user = PSUser.currentUser() {   // If nil, don't use it in the query
//            query.whereKey("lister", notEqualTo: user)
//        }
        if var newListings = query.findObjects() as? [PSListing] {
            return newListings
        }
        else {
            return [PSListing]()
        }
    } //NEEDS TESTED
    
    /**
        Get a list of listings within a range
    */
    public class func queryListingsWithinMilesInBackground(miles: Float, location: PFGeoPoint, completion: (listings: [PSListing]) -> Void) {
        var listings : [PSListing] = []
        
        let query = PSListing.query()
        query.whereKey("location", nearGeoPoint: location, withinMiles: Double(miles))
        query.whereKey("status", equalTo: "accepting")
//        if let user = PSUser.currentUser() {   // If nil, don't use it in the query
//            query.whereKey("lister", notEqualTo: user)
//        }
        
        query.findObjectsInBackgroundWithBlock { (objects, error) -> Void in
            for obj in objects {
                listings.append(obj as! PSListing)
            }
            completion(listings: listings)
        }
    } //NEEDS TESTED
    
    /**
    Get a list of listings within a range
    */
    class func queryListingsWithinMilesWithCategory(category: String, miles: Float, location: PFGeoPoint) -> [PSListing] {
        var listings : [PSListing] = []
        
        let query = PSListing.query()
        query.whereKey("listing", equalTo: category)
        query.whereKey("location", nearGeoPoint: location, withinMiles: Double(miles))
        query.whereKey("status", equalTo: "accepting")
        //        if let user = PSUser.currentUser() {   // If nil, don't use it in the query
        //            query.whereKey("lister", notEqualTo: user)
        //        }
        listings = query.findObjects() as! [PSListing]
        return listings
    } //NEEDS TESTED
    
    /**
    Get a list of listings within a range
    */
    class func queryListingsWithinMilesWithCategoryInBackground(category: String, miles: Float, location: PFGeoPoint, completion: (listings: [PSListing]) -> Void) {
        var listings : [PSListing] = []
        
        let query = PSListing.query()
        query.whereKey("listing", equalTo: category)
        query.whereKey("location", nearGeoPoint: location, withinMiles: Double(miles))
        query.whereKey("status", equalTo: "accepting")
        //        if let user = PSUser.currentUser() {   // If nil, don't use it in the query
        //            query.whereKey("lister", notEqualTo: user)
        //        }
        
        query.findObjectsInBackgroundWithBlock { (objects, error) -> Void in
            for obj in objects {
                listings.append(obj as! PSListing)
            }
            completion(listings: listings)
        }
    } //NEEDS TESTED
    
    /**
    Get a list of listings within a range
    */
    class func queryListingsWithinMilesWithCategories(categories: [String], miles: Float, location: PFGeoPoint) -> [PSListing] {
        var listings : [PSListing] = []
        
        for category in categories {
            let query = PSListing.query()
            query.whereKey("category", equalTo: category)
            query.whereKey("location", nearGeoPoint: location, withinMiles: Double(miles))
            query.whereKey("status", equalTo: "accepting")
            var error : NSErrorPointer! = NSErrorPointer()
            let objects = query.findObjects(error)
            if error == nil {
                NSLog(error.debugDescription)
            } else {
                for listing in objects {
                    listings.append(listing as! PSListing)
                    var i = 0
                }
            }
            //self.pinAllInBackground(objects)
        }
        return listings
        
    } //NEEDS TESTED
    
    /**
    Get a list of listings within a range
    */
    class func queryListingsWithinMilesWithCategoriesInBackground(categories: [String], miles: Float, location: PFGeoPoint, completion: (listings: [PSListing]) -> Void) {
        var listings : [PSListing] = []
        
        for category in categories {
            let query = PSListing.query()
            query.whereKey("category", equalTo: category)
            query.whereKey("location", nearGeoPoint: location, withinMiles: Double(miles))
            query.whereKey("status", equalTo: "accepting")
            query.findObjectsInBackgroundWithBlock { (objects, error) -> Void in
                if let e = error {
                    NSLog(e.description)
                } else {
                    for listing in objects {
                        listings.append(listing as! PSListing)
                    }
                }
                //self.pinAllInBackground(objects)
            }
        }
        
        completion(listings: listings as [PSListing])
    } //NEEDS TESTED

    
    /**
        Gets the applications associated with the object
    */
    
    public func queryApplications() -> [PSApplication] {
        let query = PSApplication.query()
        query.whereKey("listing", equalTo: self)
        var apps = query.findObjects() as! [PSApplication]!
        if apps == nil {
            apps = []
        }
        return apps
    } //tested
    
    /**
        Gets the applications associated with the object
    */
    
    public func queryApplicationsInBackground() -> [PSApplication] {
        let query = PSApplication.query()
        query.whereKey("listing", equalTo: self)
        
        let apps = query.findObjects() as! [PSApplication]
        return apps
    } //tested
    
    /**
        Get the number of applications
    */
    
    public func queryApplicationCount() -> Int {
        let query = PSApplication.query()
        query.whereKey("listing", equalTo: self)
        
        let count = query.countObjects()
        return count
    } //tested
    
    /**
        Get the number of applications
    */
    
    public func queryApplicationCountInBackground(completion:(count: Int) -> Void) {
        let query = PSApplication.query()
        query.whereKey("listing", equalTo: self)
        
        query.countObjectsInBackgroundWithBlock { (count, success) -> Void in
            completion(count: Int(count))
        }
    } //tested
    
    /**
        Get the accepted application for the current listing
    */
    public func queryAcceptedApplication() -> PSApplication? {
        let query = PSApplication.query()
        query.whereKey("listing", equalTo: self)
        query.whereKey("status", hasPrefix: "accepted")
        
        let app = query.getFirstObject() as? PSApplication
        return app
    } //tested
    
    /**
        Get the accepted application for the current listing
    */
    public func queryAcceptedApplicationInBackground(completion: (application: PSApplication?) -> Void) {
        let query = PSApplication.query()
        query.whereKey("listing", equalTo: self)
        query.whereKey("status", hasPrefix: "accepted")
        
        query.getFirstObjectInBackgroundWithBlock { (app, error) -> Void in
            if error == nil {
                completion(application: app as? PSApplication)
            } else {
                completion(application: nil)
            }
        }
        
    } //tested
    
    /**
        Set applications to pending once they're seen
    */
    
    public func setApplicationsPendingInBackground() {
        let query = PSApplication.query()
        query.whereKey("listing", equalTo: self)
        query.whereKey("status", equalTo: "new")
        query.findObjectsInBackgroundWithBlock { (applications, error) -> Void in
            if error == nil {
                if let apps : [PSApplication]! = applications as! [PSApplication]! {
                    for app : PSApplication in apps {
                        app.status = "pending"
                        app.saveInBackgroundWithBlock(nil)
                    }
                }
            }
        }
    } // NEEDS TESTED
    
    /*
    // Properties
    */
    @NSManaged public var jobDescription: String
    @NSManaged public var location: PFGeoPoint
    @NSManaged public var price: NSNumber
    @NSManaged public var status: String
    @NSManaged public var title: String
    @NSManaged public var category: String
    @NSManaged public var lister: PSUser
}