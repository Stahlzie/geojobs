//
//  MyApplicationsCell.swift
//  
//
//  Created by Kory Kappel on 3/27/15.
//
//

import UIKit

class MyApplicationsCell: UITableViewCell {
    
    @IBOutlet weak var jobTitleAndPrice : UILabel!
    @IBOutlet weak var jobOwner : UILabel!
    @IBOutlet weak var numReplies : UILabel!
    @IBOutlet weak var category : UILabel!
    @IBOutlet weak var dateApplied : UILabel!
    @IBOutlet weak var numReviews : UILabel!
    @IBOutlet weak var rating : FloatRatingView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
