//
//  LogIn.swift
//  GeoJobs
//
//  Created by Kory Kappel on 2/20/15.
//
//

import UIKit
//import Parse

protocol LogInDelegate {
    func cancel(child: UIViewController)
}

class LogIn: UIViewController, UITextFieldDelegate {
    
    var delegate : LogInDelegate!
    var child : LogInInputTable!
    
    @IBAction func cancelButton(sender: UIButton) {
        delegate.cancel(self)
    }
    
    @IBAction func loginPressed(sender: UIButton) {
        
        //make sure there are no initial field validation errors
        if child.emailField.text == "" {
            child.emailField.becomeFirstResponder()
            return
        }
        else if child.passwordField.text == "" {
            child.passwordField.becomeFirstResponder()
            return
        }
        child.active.resignFirstResponder()
        attemptLogin()
    }
    
    @IBAction func forgotPasswordPressed(sender: UIButton) {
        //display screen with instructions for resetting password
        var alert = UIAlertController(title: "Reset Password", message: "Enter the email associated with your account. GeoJobs will send you a message with a link to reset your password.", preferredStyle: .Alert)
        
        alert.addTextFieldWithConfigurationHandler({ (textField) -> Void in
            textField.placeholder = "E-mail..."
        })
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: nil))
        
        alert.addAction(UIAlertAction(title: "Send", style: .Default, handler: { (action) -> Void in
            let textField = alert.textFields![0] as! UITextField
            PSUser.requestPasswordResetForEmailInBackground(textField.text, block: { (success, error) -> Void in
                if error != nil {
                    if error.code == 205 { //205 is the code for when there is no matching email
                        var errorAlert = UIAlertView(title: "Invalid Email", message: "There is no account associated with that email. Please try again.", delegate: nil, cancelButtonTitle: "OK")
                        errorAlert.show()
                    }
                }
            })
        }))
        
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    @IBAction func resign() {
        child.active.resignFirstResponder()
    }
    
    func attemptLogin() {
        
        var actInd : UIActivityIndicatorView = UIActivityIndicatorView(frame: CGRectMake(0,0, 50, 50)) as UIActivityIndicatorView
        actInd.center = self.view.center
        actInd.hidesWhenStopped = true
        actInd.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.Gray
        view.addSubview(actInd)
        
        
        //START SPINNER:
        actInd.startAnimating()
        UIApplication.sharedApplication().beginIgnoringInteractionEvents()
        
        PSUser.logInWithUsernameInBackground(child.emailField.text, password: child.passwordField.text) { (user, error) -> Void in
            if user != nil {
                // Yes, User Exists
                self.performSegueWithIdentifier("logInSegue", sender: self)
                //what do I actually DO with user, though..??
            } else {
                // No, User Doesn't Exist
                var errorMessage : String
                switch error.code {
                case 101:
                    errorMessage = "That username / password combo does not exist"
                case 100:
                    errorMessage = "There was an error connecting to GeoJobs. Please try again later."   // Error connecting to Parse
                default:
                    errorMessage = "An unknown error occurred"
                }
                let errorView = UIAlertView(title: "Error", message: errorMessage, delegate: nil, cancelButtonTitle: "OK")
                errorView.show()
            }
            //STOP SPINNER HERE:
            UIApplication.sharedApplication().endIgnoringInteractionEvents()
            actInd.stopAnimating()
            actInd.removeFromSuperview()
        }
    }
    
    //stuff for the "Go" button on the keyboard (validates and possibly performs segue)
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if child.emailField.text == "" {
            child.emailField.becomeFirstResponder()
            return true
        }
        else if child.passwordField.text == "" {
            child.passwordField.becomeFirstResponder()
            return true
        }
        textField.resignFirstResponder()
        attemptLogin()
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        child = self.childViewControllers[0] as! LogInInputTable
        
        // Do any additional setup after loading the view.
        child.active = child.emailField
        
        //REQUIRED FOR THE textFieldShouldReturn FUNCTION
        child.emailField.delegate = self
        child.passwordField.delegate = self
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
}
