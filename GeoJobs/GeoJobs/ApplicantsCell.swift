//
//  ApplicantsCell.swift
//  GeoJobs
//
//  Created by Kory Kappel on 3/26/15.
//
//

import UIKit

class ApplicantsCell: UITableViewCell {
    
    @IBOutlet weak var username : UILabel!
    @IBOutlet weak var numReviews : UILabel!
    @IBOutlet weak var price : UILabel!
    @IBOutlet weak var timePosted : UILabel!
    @IBOutlet weak var pitch : UITextView!
    @IBOutlet weak var starRating : FloatRatingView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
