//
//  ApplicationTable.swift
//  GeoJobs
//
//  Created by Kory Kappel on 3/13/15.
//
//

import UIKit

class ApplicationTable: UITableViewController {
    
    @IBOutlet weak var username : UILabel!
    @IBOutlet weak var price : UILabel!
    @IBOutlet weak var profilePic : UIImageView!
    @IBOutlet weak var floatRatingView : FloatRatingView!
    @IBOutlet weak var pitch : UITextView!
    @IBOutlet weak var numReviews : UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        //setting up floatRatingView
        self.floatRatingView.editable = false
        self.floatRatingView.halfRatings = false
        //want to use float rating values so we can display averaged-out reviews
        self.floatRatingView.floatRatings = true

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
