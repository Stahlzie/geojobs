//
//  SignUp.swift
//  GeoJobs
//
//  Created by Kory Kappel on 2/19/15.
//
//

import UIKit
import Parse

protocol SignUpDelegate {
    func cancel(child: UIViewController)
}

class SignUp: UIViewController, UITextFieldDelegate {
    
    var delegate : SignUpDelegate!
    var child : SignUpInputTable!
    
    @IBOutlet weak var passwordWarning: UILabel!
    
    //FOR VALIDATION:
    @IBAction func passwordFieldChanged(sender: UITextField) {
        //password = passwordField.text
        checkPasswordsMatch()
    }
    @IBAction func confirmFieldChanged(sender: UITextField) {
        //confirm = confirmField.text
        checkPasswordsMatch()
    }
    
    @IBAction func cancelButton(sender: AnyObject) {
        delegate.cancel(self)
    }
    
    func checkPasswordsMatch() {
        if child.passwordField.text != "" && child.confirmField.text != "" && child.passwordField.text != child.confirmField.text {
            //DISPLAY PASSWORD MISMATCH LABEL
            passwordWarning.hidden = false
            child.passwordField.becomeFirstResponder()
        }
        else {
            passwordWarning.hidden = true
        }
    }
    
    @IBAction func submitPressed(sender: UIButton) {
        attemptSignUp()
    }
    
    @IBAction func resign() {
        child.active.resignFirstResponder()
    }
    
    func attemptSignUp() {
        //**START VALIDATION**
        
        //if there's an empty text field, it puts curser there
        if child.usernameField.text == "" {
            child.usernameField.becomeFirstResponder()
            return
        } else if child.emailField.text == "" {
            child.emailField.becomeFirstResponder()
            return
        }
        else if child.passwordField.text == "" || count(child.passwordField.text) < 5 ||  count(child.passwordField.text) > 12 {
            child.passwordField.becomeFirstResponder()
            return
        }
        else if child.confirmField.text == "" {
            child.confirmField.becomeFirstResponder()
            return
        }
        child.active.resignFirstResponder()
        
        
        checkPasswordsMatch()
        
        //password fields don't match
        if passwordWarning.hidden == false {
            child.confirmField.becomeFirstResponder()
            return
        }
        
        //**END VALIDATION**
        
        //password fields do match
        //"spinner" stuff
        var actInd : UIActivityIndicatorView = UIActivityIndicatorView(frame: CGRectMake(0,0, 50, 50)) as UIActivityIndicatorView
        actInd.center = self.view.center
        actInd.hidesWhenStopped = true
        actInd.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.Gray
        view.addSubview(actInd)
        
        //START SPINNER:
        actInd.startAnimating()
        UIApplication.sharedApplication().beginIgnoringInteractionEvents()
        
        var user = PSUser(username: child.usernameField.text, email: child.emailField.text, password: child.passwordField.text)
        PSUser.logOut()
        user.signUpInBackgroundWithBlock { (success, error) -> Void in
            if error == nil {
                //success
                self.performSegueWithIdentifier("signUpSegue", sender: self)
            } else {
                let alert = UIAlertView()
                alert.title = "Error"
                alert.message = "There was an error creating your account. Please check your internet connection and try again."
                alert.addButtonWithTitle("Ok")
                alert.show()
            }
            //STOP SPINNER HERE:
            actInd.stopAnimating()
            UIApplication.sharedApplication().endIgnoringInteractionEvents()
        }
        
        //OLD WAY OF DOING IT:
        //        var user = PFUser()
        //        user.username = emailField.text
        //        user.password = passwordField.text
        //        user.email = emailField.text
        //        user.signUpInBackgroundWithBlock {
        //            (succeeded: Bool!, error: NSError!) -> Void in
        //            if error == nil {
        //                //success
        //                self.performSegueWithIdentifier("signUpSegue", sender: self)
        //            } else {
        //                let alert = UIAlertView()
        //                alert.title = "Invalid E-mail"
        //                alert.message = "Please enter a valid e-mail address."
        //                alert.addButtonWithTitle("Ok")
        //                alert.show()
        //            }
        //            //STOP SPINNER HERE:
        //            actInd.stopAnimating()
        //        }
    }
    
    //stuff for the "Go" button on the keyboard (validates and possibly performs segue)
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        attemptSignUp()
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        child = self.childViewControllers[0] as! SignUpInputTable
        
        // Do any additional setup after loading the view.
        passwordWarning.hidden = true
        child.active = child.emailField
        
        //REQUIRED FOR THE textFieldShouldReturn FUNCTION
        child.emailField.delegate = self
        child.passwordField.delegate = self
        child.confirmField.delegate = self
        child.usernameField.delegate = self
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
}
