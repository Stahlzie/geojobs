//
//  ParseClasses.swift
//  GeoJobs
//
//  Created by Nathan Snyder on 2015-02-4.
//
//

import Foundation
import Parse

//class User {
//    // Variables
//    var user : PFUser
//    
//    /*
//    // Initializers
//    */
//    
//    // Initialize with user
//    init(u: PFUser) {
//        user = u
//    }
//    
//    // Blank user object
//    init() {
//        user = PFUser()
//    }
//    
//    /*
//    // Class functions
//    */
//    
//    /**
//    Create the JobPosting and attempt to save to the database
//    
//    :param: offer Double value of the offer for the job
//    :param: pitch User's pitch for the job
//    :param: listing JobPosting object containing the listing that you are applying for
//    :param: completion Block that will fire after it attempts to save the add the application
//    */
//    
//    class func applyInBackground(offer: Double, pitch: String, listing: JobPosting,
//        completion: (application: Application!, success: Bool, error: NSError!) -> Void) {
//            if let user = PFUser.currentUser()? {
//                var app = Application()
//                app.pitch = pitch
//                app.listing = listing
//                app.offer = offer
//                app.owner = User.currentUser()
//                
//                app.saveInBackground {
//                    (success: Bool, error: NSError!) -> Void in
//                    if(success) {
//                        completion(application: app, success: success, error: error)
//                    } else {
//                        completion(application: nil, success: success, error: error)
//                    }
//                }
//            }
//            
//            // If there is no currentUser logged in
//            completion(application: nil, success: false, error: NSError())
//    }
//    
//    class func currentUser() -> User! {
//        return User(u: PFUser.currentUser())
//    }
//    
//    class func loginInBackground(username: String, password: String, completion: (user: User!, error: NSError!) -> Void) {
//        PFUser.logInWithUsernameInBackground(username, password: password) {
//            (user: PFUser!, error: NSError!) -> Void in
//            if(user == nil) {
//                completion(user: nil, error: error)
//            } else {
//                completion(user: User(u: user), error: error)
//            }
//        }
//        
//    }
//    
//    class func login(username: String, password: String) -> PFUser {
//        return PFUser.logInWithUsername(username, password: password)
//    }
//    
//    class func loggedIn() -> Bool {
//        return PFUser.currentUser() != nil
//    }
//    
//    class func getUser(username: String, completion: (user: User!, error: NSError!) -> Void) {
//        let query = PFUser.query()
//        query.whereKey("username", equalTo: username)
//        query.getFirstObjectInBackgroundWithBlock { (user, error) -> Void in
//            if(error != nil) {  // There is an error
//                completion(user: nil, error: error)
//            } else {
//                completion(user: User(u: user as PFUser), error: error)
//            }
//        }
//    }
//    
//    /**
//    *Synchronously* sends a password reset email
//    
//    :param: email Email address to send the reset to
//    :return: Boolean telling you whether the email succeded
//    */
//    
//    class func sendPasswordResetEmail(email: String) -> Bool {
//        return PFUser.requestPasswordResetForEmail(email)
//    }
//    
//    /**
//    *Asynchronously* sends a password reset email
//    
//    :param: email Email address to send the reset to
//    :param: completion Block that will fire after it attempts to send the email, contains a success bool and an error message if it fails
//    */
//    
//    class func sendPasswordResetEmailInBackground(email: String, completion: (success: Bool, error: NSError!) -> Void) {
//        PFUser.requestPasswordResetForEmailInBackground(email) {
//            (success: Bool, error: NSError!) -> Void in
//            completion(success: success, error: error)
//        }
//    }
//    
//    
//    /*
//    // Instance functions
//    */
//    
//    func changeUsername(username: String) -> Void {
//        user.username = username
//    }
//    
//    // Sign up user *synchronously*
//    func signUp(username: String, password: String) -> Bool {
//        user.username = username
//        user.email = username
//        user.password = password
//        var error = NSErrorPointer()
//        let success = user.signUp(error)
//        if(!success) {
//            println("Error: " + error.debugDescription)
//        }
//        return success
//        
//    }
//    
//    // Sign up user *asynchronously*
//    func signUpInBackground(username: String, password: String, completion: (success: Bool, error: NSError!) -> Void) {
//        user.username = username
//        user.email = username
//        user.password = password
//        user.signUpInBackgroundWithBlock {
//            (success: Bool, error: NSError!) -> Void in
//            completion(success: success, error: error)
//        }
//    }
//    
//    // Login *synchronously*
//    func login(username: String, password: String) {
//        var u = PFUser.logInWithUsername(username, password: password)
//        if u  != nil {
//            user = u
//        }
//    }
//    
//    // Login *asynchronously*
//    // Implement later
//    
//    func save() {
//        user.save()
//    }
//    
//    func saveInBackground(completion: (success: Bool, error: NSError!) -> Void) {
//        user.saveInBackgroundWithBlock {
//            (success: Bool, error: NSError!) -> Void in
//            completion(success: success, error: error)
//        }
//    }
//    
//    /*
//    // Computed properties
//    */
//    
//    var username : String! {
//        get {
//            return user.username
//        }
//    }
//    
//    var realName : String! {
//        get {
//            return user["realName"] as String!
//        }
//        set {
//            user["realName"] = newValue
//        }
//    }
//    
//    var email : String! {
//        get {
//            return user.email
//        }
//        set {
//            user["email"] = newValue
//        }
//    }
//    
//    var emailVerified : Bool {
//        get {
//            return user["emailVerifed"] as Bool
//        }
//    }
//    
//    var createdAt : NSDate! {
//        get {
//            return user["createdAt"] as NSDate
//        }
//    }
//    
//    var updatedAt : NSDate! {
//        get {
//            return user["updatedAt"] as NSDate
//        }
//    }
//    
//    var profileImg : UIImage! {
//        get {
//            let profilePhotoFile = user["profileImg"] as PFFile
//            var error = NSErrorPointer()
//            var imageData = profilePhotoFile.getData(error)
//            if error == nil {
//                return UIImage(data:imageData)
//            } else {
//                return nil
//            }
//        }
//    }
//    
//    var rating : Double! {
//        get {
//            return Review.averageRating(self)
//        }
//    }
//    
//    var timesReviewed : Int {
//        get {
//            var returnVal = 0
//            Review.reviewCountInBackground(self) { (revCount, error) -> Void in
//                if error != nil {
//                    returnVal = Int(revCount)
//                } else {
//                    returnVal = 0
//                }
//            }
//            return returnVal
//        }
//    }
//    
//    var objectId : String! {
//        get {
//            return user["objectId"] as String!
//        }
//    }
//    
//    // Be careful, but you can do anything with this
//    var rawObject : PFUser {
//        get {
//            return user
//        }
//    }
//    
//}

//class JobPosting {
//    // variables
//    var post : PFObject     // NOT optional object, may be NOT nil
//    private(set) var onServer : Bool    // Private to the class, only true when
//    // initialized with an object, or saved to the server
//    
//    /*
//    // Initializers
//    */
//    
//    // Create a new post from scratch
//    init() {
//        post = PFObject(className: "Listing")
//        onServer = false
//    }
//    
//    // Initialize from postID
//    init(postId: String) {
//        let query = PFQuery(className: "Listing")
//        let result = query.getObjectWithId(postId)
//        //let result = query.getFirstObject()
//        if result != nil {
//            self.post = result
//        } else {
//            post = result //needs to have an error, will crash otherwise
//        }
//        onServer = true
//    }
//    
//    // Initialize from existing post (retrieved from Parse)
//    init(post : PFObject) {
//        self.post = post
//        onServer = true
//    }
//    
//    // Initialize with a post ID
//    init(posterId : String) {
//        self.post = PFObject(className: "Listing")
//        onServer = false
//        
//        var userQuery = PFUser.query()
//        owner = User(u: userQuery.getObjectWithId(posterId as String) as PFUser)
//    }
//    
//    // Initialize with a username
//    init(username : String) {
//        self.post = PFObject(className: "Listing")
//        onServer = false
//        
//        var userQuery = PFUser.query()
//        userQuery.whereKey("username", equalTo: username)
//        owner = User(u: userQuery.getFirstObject() as PFUser)
//    }
//    
//    // Initialize with a user object
//    init(user : PFUser) {
//        post = PFObject(className: "Listing")
//        onServer = false
//        self.owner = User(u: user)
//    }
//    
//    /*
//    // Class functions
//    */
//    
//    class func newJobPostingCountInBackground(completion: (count: Int32, error: NSError!) -> Void) {
//        let query = PFQuery(className: "Listing")
//        query.whereKey("owner", equalTo: PFUser.currentUser())
//        query.whereKey("new", equalTo: true)
//        query.countObjectsInBackgroundWithBlock {
//            (count: Int32, error: NSError!) -> Void in
//            completion(count:count, error: error)
//        }
//    }
//    
//    class func revokeApplication(application: Application, completion: (success: Bool, error: NSError!) -> Void) {
//        application.app.deleteInBackgroundWithBlock {
//            (success: Bool, error: NSError!) -> Void in
//            completion(success: success, error: error)
//        }
//    }
//    
//    class func newFromCurrentUser() -> JobPosting! {
//        if let user = PFUser.currentUser()? {
//            var post = JobPosting()
//            post.owner = User(u: user)
//            return post
//        }
//        
//        // If there is no currentUser
//        return nil
//    }
//    
//    class func myListedJobsInBackground(completion: (jobs: [JobPosting], error: NSError!) -> Void) {
//        let query = PFQuery(className: "Listing")
//        query.whereKey("owner", equalTo: PFUser.currentUser())
//        
//        query.findObjectsInBackgroundWithBlock() {
//            (objects: [AnyObject]!, error: NSError!) -> Void in
//            if(error != nil) {
//                completion(jobs: [] as [JobPosting], error: error)
//            } else {
//                var jobs : [JobPosting]
//                jobs = []
//                for job in objects {
//                    jobs += [JobPosting(post: job as PFObject)]
//                }
//                completion(jobs: jobs, error: error)
//            }
//        }
//        
//    }
//    
//    class func listingById(objectId: String) -> JobPosting! {
//        var query = PFQuery(className: "Listing")
//        query.includeKey("owner")
//        
//        let listing = query.getObjectWithId(objectId)
//        if listing == nil {
//            return nil
//        }
//        else {
//            return JobPosting(post: listing)
//        }
//    }
//    
//    class func listingsByUser(user: User) -> [JobPosting]! {
//        let query = PFQuery(className: "Listing")
//        query.whereKey("owner", equalTo: user.user)
//        let listings = query.findObjects()
//        if listings != nil {
//            var convertedListings : [JobPosting] = []
//            for item in listings {
//                convertedListings.append(JobPosting(post: item as PFObject))
//            }
//            
//            return convertedListings
//        } else {
//            return nil
//        }
//    }
//    
//    class func listingsWithinMiles(location: PFGeoPoint!, miles: Double) -> [JobPosting] {
//        let query = PFQuery(className: "Listing")
//        query.whereKey("location", nearGeoPoint: location, withinMiles: miles)
//        
//        let listings = query.findObjects()
//        var convertedListings : [JobPosting] = []
//        for item in listings {
//            convertedListings.append(JobPosting(post: item as PFObject))
//        }
//        
//        return convertedListings
//    }
//    
//    class func listingsWithinMiles(lat: Double, lon: Double, miles: Double) -> [JobPosting] {
//        let query = PFQuery(className: "Listing")
//        let location = PFGeoPoint(latitude: lat, longitude: lon)
//        
//        query.whereKey("location", nearGeoPoint: location, withinMiles: miles)
//        
//        let listings = query.findObjects()
//        var convertedListings : [JobPosting] = []
//        for item in listings {
//            convertedListings.append(JobPosting(post: item as PFObject))
//        }
//        
//        return convertedListings
//        
//    }
//    
//    // TODO: The above 2 *asynchronously*
//    
//    /*
//    // Instance functions
//    */
//    
//    func save() -> Bool {
//        let succeeded = post.save()
//        return succeeded
//    }
//    
//    func saveInBackground(completion: (success: Bool, error: NSError!) -> Void) {
//        post.saveInBackgroundWithBlock {
//            (success: Bool, error: NSError!) -> Void in
//            completion(success: success, error: error)
//        }
//    }
//    
//    func delete() -> Bool {
//        //let succeeded = post.delete()
//        return true
//    }
//    
//    func deleteInBackground(completion: (success: Bool, error: NSError!) -> Void) {
//        post.deleteInBackgroundWithBlock {
//            (success: Bool, error: NSError!) -> Void in
//            completion(success: success, error: error)
//        }
//    }
//    
//    func setJobLocationByLatitudeAndLongitude(lat: Double, lon: Double) {
//        jobLocation = PFGeoPoint(latitude: lat, longitude: lon)
//    }
//    
//    /*
//    // Disabled because the ACL (Access Control List) won't let a user change another
//    // user's object. Shouldn't be necessary.
//    func changeOwner(newOwner: PFUser) -> Void {
//    var relation = post.relationForKey("owner")
//    relation.removeObject(owner)
//    relation.addObject(newOwner)
//    }
//    */
//    
//    /*
//    // Computed properties
//    */
//    
//    var jobLocation : PFGeoPoint! {
//        get {
//            return post["location"] as PFGeoPoint
//        }
//        set {
//            post["location"] = newValue
//        }
//    }
//    
//    var description : String! {
//        get {
//            return post["description"] as String
//        }
//        set {
//            post["description"] = newValue
//        }
//    }
//    
//    var title : String! {
//        get {
//            return post["title"] as String
//        }
//        set {
//            post["title"] = newValue
//        }
//    }
//    
//    var price : Double! {
//        get {
//            return post["price"] as Double
//        }
//        set {
//            post["price"] = newValue
//        }
//    }
//    
//    var category : String! {
//        get {
//            return post["category"] as String
//        }
//        set {
//            post["category"] = newValue
//        }
//    }
//    
//    var postId : String! {
//        get {
//            return post["objectId"] as String
//        }
//    }
//    
//    var createdAt : NSDate! {
//        get {
//            return post["createdAt"] as NSDate
//        }
//    }
//    
//    var updatedAt : NSDate! {
//        get {
//            return post["updatedAt"] as NSDate
//        }
//    }
//    
//    var owner : User! {
//        get {
//            return User(u: post["owner"] as PFUser)
//            
//        }
//        set {
//            post["owner"] = newValue.rawObject
//        }
//    }
//    
//    var objectId : String! {
//        get {
//            return post["objectId"] as String!
//        }
//    }
//    
//    // Be careful, but you can do anything with this
//    var rawObject : PFObject {
//        get {
//            return post as PFObject
//        }
//    }
//    
//}

//class Application {
//    // variables
//    var app : PFObject     // NOT optional object, may be NOT nil
//    
//    /*
//    // Initializers
//    */
//    
//    // Create a new post from scratch
//    init() {
//        app = PFObject(className: "Application")
//    }
//    
//    // Initialize from existing post (retrieved from Parse)
//    init(application : PFObject) {
//        self.app = application
//    }
//    
//    // Initialize with a user ID
//    init(posterId : String) {
//        self.app = PFObject(className: "Application")
//        
//        var userQuery = PFUser.query()
//        let u = userQuery.getObjectWithId(posterId) as PFUser
//        owner = User(u: u)
//        isNew = true
//        
//    }
//    
//    // Initialize with a username
//    init(username : String) {
//        self.app = PFObject(className: "Application")
//        
//        var userQuery = PFUser.query()
//        userQuery.whereKey("username", equalTo: username)
//        owner = User(u: userQuery.getFirstObject() as PFUser)
//        isNew = true
//    }
//    
//    // Initialize with a user object
//    init(user : PFUser) {
//        app = PFObject(className: "Application")
//        self.owner = User(u: user)
//        isNew = true
//    }
//    
//    /*
//    // Class functions
//    */
//    
//    class func newApplicationCountInBackground(completion: (count: Int32, error: NSError!) -> Void) {
//        let ownedListings = PFQuery(className: "Listing")
//        ownedListings.whereKey("owner", equalTo: PFUser.currentUser())
//        
//        let query = PFQuery(className: "Application")
//        query.whereKey("listing", matchesQuery: ownedListings)
//        query.whereKey("new", equalTo: true)
//        query.countObjectsInBackgroundWithBlock {
//            (count: Int32, error: NSError!) -> Void in
//            completion(count:count, error: error)
//        }
//    }
//    
//    class func getApplicationsForJobPosting(job: JobPosting) -> [Application] {
//        var query = PFQuery(className: "Application")
//        query.whereKey("listing", equalTo: job.rawObject)
//        var apps = query.findObjects()
//        var convertedApps : [Application] = []
//        for app in apps {
//            convertedApps.append(Application(application: app as PFObject))
//        }
//        return convertedApps
//    }
//    
//    class func getApplicationsForJobPostingInBackground(job: JobPosting, completion: (applications: [Application], success: Bool, error: NSError!) -> Void) {
//        var query = PFQuery(className: "Application")
//        query.whereKey("listing", equalTo: job.rawObject)
//        query.findObjectsInBackgroundWithBlock() {
//            (objects: [AnyObject]!, error: NSError!) -> Void in
//            if(error != nil || objects.count == 0) {
//                completion(applications: [] as [Application], success: false, error: error)
//            } else {
//                var apps : [Application]
//                apps = []
//                for app in objects {
//                    apps.append(Application(application: app as PFObject))
//                }
//                completion(applications: apps, success: true, error: error)
//            }
//        }
//    }
//    
//    class func getApplicationsByUser(user: User) -> [Application]! {
//        let query = PFQuery(className: "Application")
//        query.whereKey("owner", equalTo: user.user)
//        let applications = query.findObjects()
//        if applications != nil {
//            var convertedApps : [Application] = []
//            for app in applications {
//                convertedApps.append(Application(application: app as PFObject))
//            }
//            
//            return convertedApps
//        } else {
//            return nil
//        }
//    }
//    
//    class func newFromCurrentUser() -> JobPosting! {
//        if let user = PFUser.currentUser()? {
//            var post = JobPosting()
//            post.owner = User(u: user)
//            return post
//        }
//        
//        // If there is no currentUser
//        return nil
//    }
//    
//    /*
//    // Instance functions
//    */
//    
//    func save() -> Bool {
//        let succeeded = app.save()
//        return succeeded
//    }
//    
//    func saveInBackground(completion: (success: Bool, error: NSError!) -> Void) {
//        app.saveInBackgroundWithBlock {
//            (success: Bool, error: NSError!) -> Void in
//            completion(success: success, error: error)
//        }
//    }
//    
//    func delete() -> Bool {
//        //let succeeded = app.delete()
//        return true
//    }
//    
//    func deleteInBackground(completion: (success: Bool, error: NSError!) -> Void) {
//        app.deleteInBackgroundWithBlock {
//            (success: Bool, error: NSError!) -> Void in
//            completion(success: success, error: error)
//        }
//    }
//    
//    /*
//    // Computed properties
//    */
//    
//    var listing : JobPosting! {
//        get {
//            return JobPosting(post: app["listing"] as PFObject)
//        }
//        set {
//            app["listing"] = newValue.rawObject
//        }
//    }
//    
//    var owner : User! {
//        get {
//            return User(u: app["owner"] as PFUser)
//        }
//        set {
//            app["owner"] = newValue.rawObject
//        }
//    }
//    
//    var pitch : String! {
//        get {
//            return app["pitch"] as String!
//        }
//        set {
//            app["pitch"] = newValue
//        }
//    }
//    
//    var offer : Double! {
//        get {
//            return app["offer"] as Double
//        }
//        set {
//            app["offer"] = NSNumber(double: newValue)
//        }
//    }
//    
//    var objectId : String! {
//        get {
//            return app["objectId"] as String!
//        }
//    }
//    
//    var isNew : Bool! {
//        get {
//            return app["new"] as Bool
//        }
//        set {
//            app["new"] = newValue
//        }
//    }
//    
//    // Be careful, but you can do anything with this
//    var rawObject : PFObject {
//        get {
//            return app
//        }
//    }
//}

//class Contract {
//    // variables
//    var contract : PFObject     // NOT optional object, may be NOT nil
//    
//    /*
//    // Initializers
//    */
//    
//    // Create a new post from scratch
//    init() {
//        contract = PFObject(className: "Contract")
//    }
//    
//    // Initialize from existing post (retrieved from Parse)
//    init(contract : PFObject) {
//        self.contract = contract
//    }
//    
//    // Initialize with a user ID
//    init(listerId : String, responderId : String, jobId : String) {
//        self.contract = PFObject(className: "Contract")
//        
//        var userQuery = PFUser.query()
//        let lister = userQuery.getObjectWithId(listerId) as PFUser
//        let responder = userQuery.getObjectWithId(responderId) as PFUser
//        self.lister = User(u: lister)
//        self.responder = User(u: responder)
//        
//        var jobQuery = PFQuery(className: "Listing")
//        let job = jobQuery.getObjectWithId(jobId) as PFObject
//        listing = JobPosting(post: job)
//        isNew = true
//    }
//    
//    // Initialize with objects
//    init(lister : User, responder : User, job : JobPosting) {
//        self.contract = PFObject(className: "Contract")
//        self.lister = lister
//        self.responder = responder
//        self.listing = job
//        isNew = true
//    }
//    
//    /*
//    // Class functions
//    */
//    
//    class func newContractCountInBackground(completion: (count: Int32, error: NSError!) -> Void) {
//        let query = PFQuery(className: "Contract")
//        query.whereKey("lister", equalTo: PFUser.currentUser())
//        query.whereKey("new", equalTo: true)
//        query.countObjectsInBackgroundWithBlock {
//            (count: Int32, error: NSError!) -> Void in
//            completion(count:count, error: error)
//        }
//    }
//    
//    class func getContractsByLister(user: User) -> [Contract]! {
//        let query = PFQuery(className: "Contract")
//        query.whereKey("lister", equalTo: user.user)
//        let contracts = query.findObjects()
//        if contracts != nil {
//            var convertedContracts : [Contract] = []
//            for item in contracts {
//                convertedContracts.append(Contract(contract: item as PFObject))
//            }
//            
//            return convertedContracts
//        } else {
//            return nil
//        }
//    }
//    
//    /*
//    // Instance functions
//    */
//    
//    func save() -> Bool {
//        let succeeded = contract.save()
//        return succeeded
//    }
//    
//    func saveInBackground(completion: (success: Bool, error: NSError!) -> Void) {
//        contract.saveInBackgroundWithBlock {
//            (success: Bool, error: NSError!) -> Void in
//            completion(success: success, error: error)
//        }
//    }
//    
//    func delete() -> Bool {
//        //let succeeded = contract.delete()
//        return true
//    }
//    
//    func deleteInBackground(completion: (success: Bool, error: NSError!) -> Void) {
//        contract.deleteInBackgroundWithBlock {
//            (success: Bool, error: NSError!) -> Void in
//            completion(success: success, error: error)
//        }
//    }
//    
//    /*
//    // Computed properties
//    */
//    
//    var lister : User! {
//        get {
//            return User(u: contract["lister"] as PFUser)
//        }
//        set {
//            contract["lister"] = newValue.rawObject
//        }
//    }
//    
//    var responder : User! {
//        get {
//            return User(u: contract["responder"] as PFUser)
//        }
//        set {
//            contract["responder"] = newValue.rawObject
//        }
//    }
//    
//    var listing : JobPosting! {
//        get {
//            return JobPosting(post: contract["listing"] as PFObject)
//        }
//        set {
//            contract["listing"] = newValue.rawObject
//        }
//    }
//    
//    var createdAt : NSDate! {
//        get {
//            return contract["createdAt"] as NSDate!
//        }
//    }
//    
//    var updatedAt : NSDate! {
//        get {
//            return contract["updatedAt"] as NSDate!
//        }
//    }
//    
//    var objectId : String! {
//        get {
//            return contract["objectId"] as String!
//        }
//    }
//    
//    var isNew : Bool! {
//        get {
//            return contract["new"] as Bool
//        }
//        set {
//            contract["new"] = newValue
//        }
//    }
//    
//    // Be careful, but you can do anything with this
//    var rawObject : PFObject {
//        get {
//            return contract
//        }
//    }
//}

//class Review {
//    // variables
//    var review : PFObject     // NOT optional object, may be NOT nil
//    
//    /*
//    // Initializers
//    */
//    
//    // Create a new post from scratch
//    init() {
//        review = PFObject(className: "Review")
//        isNew = true
//    }
//    
//    // Initialize from existing post (retrieved from Parse)
//    init(review : PFObject) {
//        self.review = review
//    }
//    
//    // Initialize with a user ID
//    init(reviewingId : String, reviewedId : String, jobId : String) {
//        self.review = PFObject(className: "Review")
//        
//        var userQuery = PFUser.query()
//        let reviewing = userQuery.getObjectWithId(reviewingId) as PFUser
//        let reviewed = userQuery.getObjectWithId(reviewedId) as PFUser
//        self.reviewingUser = User(u: reviewing)
//        self.reviewedUser = User(u: reviewed)
//        
//        var jobQuery = PFQuery(className: "Listing")
//        let job = jobQuery.getObjectWithId(jobId) as PFObject
//        listing = JobPosting(post: job)
//        isNew = false
//    }
//    
//    // Initialize with objects
//    init(reviewingUser : User, reviewedUser : User, job : JobPosting) {
//        self.review = PFObject(className: "Review")
//        self.reviewingUser = reviewingUser
//        self.reviewedUser = reviewedUser
//        self.listing = job
//        isNew = false
//    }
//    
//    /*
//    // Class functions
//    */
//    
//    /**
//    *Asynchronously* creates and posts a review for a user, from the current user
//    
//    :param: rating Double with the rating for the user
//    :param: description The actual textual review
//    :param: listing The JobPosting that the review is for
//    :param: reviewedUsername The username of the person you are reviewing
//    :return: A completion block that returns the review object, or nil, and a bool indicating success and an error message if necessary
//    */
//    
//    class func reviewInBackground(rating: Double, description: String, listing: JobPosting, reviewedUsername: String,
//        completion: (review: Review!, success: Bool, error: NSError!) -> Void) {
//            if let user = PFUser.currentUser()? {
//                var rev = Review()
//                rev.description = description
//                rev.listing = listing
//                rev.rating = rating
//                rev.reviewingUser = User(u: user)
//                let userQuery = PFUser.query()
//                userQuery.whereKey("user", equalTo: reviewedUsername)
//                let revdUser = userQuery.getFirstObject() as PFUser
//                rev.reviewedUser = User(u: revdUser)
//                
//                rev.saveInBackground {
//                    (success: Bool, error: NSError!) -> Void in
//                    if(success) {
//                        completion(review: rev, success: success, error: error)
//                    } else {
//                        completion(review: nil, success: success, error: error)
//                    }
//                }
//            }
//            
//            // If there is no currentUser logged in
//            completion(review: nil, success: false, error: NSError())
//    }
//    
//    class func getUserReviews(username: String, count: Int, completion: (rating: Double, reviews: [Review]!) -> Void) {
//        var rating: Double = 0.0
//        // calculate the user rating via cloud code
//        
//        let revQuery = PFQuery(className: "Review")
//        revQuery.whereKey("reviewedUser", equalTo: username)
//        let revObjects = revQuery.findObjects()
//        var reviews: [Review] = []
//        for obj in revObjects {
//            reviews.append(Review(review: obj as PFObject))
//        }
//        completion(rating: rating, reviews: reviews)
//    }
//    
//    /**
//    Attempt to get a single review from the database
//    
//    :param: id Parse object id of the review (should be able to get from a list of reviews)
//    :return: Review object if it finds one, or nil if it doesn't
//    */
//    
//    class func getReviewById(id: String) -> Review! {
//        var revQuery = PFQuery(className: "Review")
//        let revObject = revQuery.getObjectWithId(id)
//        var returnedReview: Review
//        if(revObject != nil) {
//            returnedReview = Review(review: revObject)
//            return returnedReview
//        } else {
//            return nil
//        }
//    }
//    
//    class func newReviewCountInBackground(completion: (count: Int32, error: NSError!) -> Void) {
//        let query = PFQuery(className: "Review")
//        query.whereKey("reviewedUser", equalTo: PFUser.currentUser())
//        query.whereKey("new", equalTo: true)
//        query.countObjectsInBackgroundWithBlock {
//            (count: Int32, error: NSError!) -> Void in
//            completion(count:count, error: error)
//        }
//    }
//    
//    class func averageRating(user: User) -> Double {
//        let query = PFQuery(className: "Review")
//        query.whereKey("reviewedUser", equalTo: user.user)
//        query.selectKeys(["rating"])
//        var ratings = query.findObjects()
//        if ratings.count != 0 {
//            var sum = Int(0)
//            for rating in ratings {
//                sum += rating.integerValue
//            }
//            var average = Double(sum) / Double(ratings.count)
//            return average
//        } else {
//            return 0
//        }
//    }
//
//    
//    class func reviewCountInBackground(user: User, completion: (count: Int32, error: NSError!) -> Void) {
//        let query = PFQuery(className: "Review")
//        query.whereKey("reviewedUser", equalTo: user.user)
//        query.countObjectsInBackgroundWithBlock {
//            (count: Int32, error: NSError!) -> Void in
//            completion(count:count, error: error)
//        }
//    }
//    
//    /*
//    // Instance functions
//    */
//    
//    func save() -> Bool {
//        let succeeded = review.save()
//        return succeeded
//    }
//    
//    func saveInBackground(completion: (success: Bool, error: NSError!) -> Void) {
//        review.saveInBackgroundWithBlock {
//            (success: Bool, error: NSError!) -> Void in
//            completion(success: success, error: error)
//        }
//    }
//    
//    func delete() -> Bool {
//        //let succeeded = review.delete()
//        return true
//    }
//    
//    func deleteInBackground(completion: (success: Bool, error: NSError!) -> Void) {
//        review.deleteInBackgroundWithBlock {
//            (success: Bool, error: NSError!) -> Void in
//            completion(success: success, error: error)
//        }
//    }
//    
//    /*
//    // Computed properties
//    */
//    
//    var reviewedUser : User! {
//        get {
//            return User(u: review["reviewedUser"] as PFUser)
//        }
//        set {
//            review["lister"] = newValue.rawObject
//        }
//    }
//    
//    var description : String! {
//        get {
//            return review["description"] as String
//        }
//        set {
//            review["description"] = newValue
//        }
//    }
//    
//    var reviewingUser : User! {
//        get {
//            return User(u: review["reviewingUser"] as PFUser)
//        }
//        set {
//            review["reviewingUser"] = newValue.rawObject
//        }
//    }
//    
//    var listing : JobPosting! {
//        get {
//            return JobPosting(post: review["listing"] as PFObject)
//        }
//        set {
//            review["listing"] = newValue.rawObject
//        }
//    }
//    
//    var rating : Double! {
//        get {
//            let ret = review["rating"] as NSNumber
//            return ret.doubleValue
//        }
//        set {
//            review["rating"] = NSNumber(double: newValue)
//        }
//    }
//    
//    var createdAt : NSDate! {
//        get {
//            return review["createdAt"] as NSDate!
//        }
//    }
//    
//    var updatedAt : NSDate! {
//        get {
//            return review["updatedAt"] as NSDate!
//        }
//    }
//    
//    var objectId : String! {
//        get {
//            return review["objectId"] as String!
//        }
//    }
//    
//    var isNew : Bool! {
//        get {
//            return review["new"] as Bool
//        }
//        set {
//            review["new"] = newValue
//        }
//    }
//    
//    // Be careful, but you can do anything with this
//    var rawObject : PFObject {
//        get {
//            return review
//        }
//    }
//}

