//
//  HomeView.swift
//  GeoJobs
//
//  Created by Steven Patterson on 2/27/15.
//
//

import UIKit
//import Parse

class HomeView: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var settings : UIBarButtonItem!
    @IBOutlet weak var createPost : UIButton!
    @IBOutlet weak var browseJobs : UIButton!
    @IBOutlet weak var profilePicture : UIImageView!
    @IBOutlet weak var username : UILabel!
    @IBOutlet weak var rating : FloatRatingView!
    @IBOutlet weak var numReviews : UILabel!
    @IBOutlet var tableView: UITableView!
    
    var choices = ["My Job Listings", "My Contracts", "My Applications", "My Reviews"]
    
    //var jobs:[PSListing] = []
    
    //this is used to tell when we want the completed tab of my job listings view to be shown first when segue'ing there (true after the second review for a contract has been left)
    //var myJobListingsSegueShowCompletedJobs : Bool!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.registerClass(HomeCell.self, forCellReuseIdentifier: "cell")
        createPost.layer.borderColor = UIColor(white: 0.6, alpha: 1.0).CGColor
        createPost.layer.borderWidth = 1
        createPost.layer.cornerRadius = 5
        browseJobs.layer.borderColor = UIColor(white: 0.6, alpha: 1.0).CGColor
        browseJobs.layer.borderWidth = 1
        browseJobs.layer.cornerRadius = 5
        
        // Do any additional setup after loading the view.
        profilePicture.layer.masksToBounds = false
        profilePicture.layer.borderColor = UIColor.whiteColor().CGColor
        profilePicture.layer.cornerRadius = 15
        profilePicture.layer.cornerRadius = profilePicture.frame.size.height/2
        profilePicture.clipsToBounds = true
        
        //set up spinner
        var actInd : UIActivityIndicatorView = UIActivityIndicatorView(frame: CGRectMake(0,0, 50, 50)) as UIActivityIndicatorView
        actInd.center = self.view.center
        actInd.hidesWhenStopped = true
        actInd.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.Gray
        view.addSubview(actInd)

        //START SPINNER:
        actInd.startAnimating()
        UIApplication.sharedApplication().beginIgnoringInteractionEvents()
        //set the dynamic content on the page for current user
        var user = PSUser.currentUser()
        
        user.getProfileImageInBackground { (image) -> Void in
            self.profilePicture.image = image
        }
        
        
        username.text = user.username
        
        PSReview.queryAverageRatingOfUserInBackground(user, completion: { (rating) -> Void in
            self.rating.rating = Float(rating)
        })
        
        PSReview.queryReviewCountInBackground(user, completion: { (count, error) -> Void in
            let temp : Int = Int(count)
            if temp == 1 {
                self.numReviews.text = "\(temp) review"
            } else {
                self.numReviews.text = "\(temp) reviews"
            }
            
            //STOP SPINNER HERE:
            UIApplication.sharedApplication().endIgnoringInteractionEvents()
            actInd.stopAnimating()
            actInd.removeFromSuperview()
        })
        //actually not really sure if spinner was necessary in this situation...
        
        //watch for notifications in order to jump to different places in the app
        let center = NSNotificationCenter.defaultCenter()
        center.addObserver(self, selector: "reviewLeftContractStillIncomplete", name: "firstReviewLeft", object: nil)
        center.addObserver(self, selector: "reviewLeftContractComplete", name: "secondReviewLeft", object: nil)
        center.addObserver(self, selector: "jobListingCreated", name: "jobListingCreated", object: nil)
        center.addObserver(self, selector: "applicantSelected", name: "applicantSelected", object: nil)
        center.addObserver(self, selector: "comeBackHomeChildren", name: "applicationRevoked", object: nil)
        center.addObserver(self, selector: "comeBackHomeChildren", name: "secondReviewLeftHOME", object: nil)
        //myJobListingsSegueShowCompletedJobs = false
        
        // Let it scroll
        
        /*
        let scroller = UIScrollView()
        scroller.addSubview(self.view)
        scroller.contentSize = self.view.frame.size
        NSLog(self.view.frame.size.height.description + " x " + self.view.frame.size.width.description)
        self.view = scroller
        self.view.backgroundColor = UIColor.whiteColor()
        */


    }
    
    func comeBackHomeChildren() {
        self.navigationController?.popToRootViewControllerAnimated(true)
    }
    
    func applicantSelected() {
        self.navigationController?.popToRootViewControllerAnimated(true)
        performSegueWithIdentifier("MyContractsSegue", sender: self)
    }
    //this will run when the first review for a contract has been left
    func reviewLeftContractStillIncomplete() {
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers as! [UIViewController]
        self.navigationController!.popToViewController(viewControllers[viewControllers.count - 3], animated: true)

        //performSegueWithIdentifier("MyContractsSegue", sender: self)
    }
    //this will run when the second review for a contract has been left (and is thus complete)
    func reviewLeftContractComplete() {
        self.navigationController?.popToRootViewControllerAnimated(true)
        performSegueWithIdentifier("MyJobListingsSegue", sender: self)
        //myJobListingsSegueShowCompletedJobs = true
    }
    
    func jobListingCreated() {
        self.navigationController?.popToRootViewControllerAnimated(true)
        performSegueWithIdentifier("MyJobListingsSegue", sender: self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return choices.count;
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = self.tableView.dequeueReusableCellWithIdentifier("HomeCell", forIndexPath: indexPath) as! HomeCell
        //this "textLabel" is apparently provided by default for any cell
        cell.label.text = choices[indexPath.row]
        cell.badge.layer.cornerRadius = 8
        cell.badge.clipsToBounds = true
        dispatch_async(dispatch_get_global_queue(Int(QOS_CLASS_USER_INITIATED.value), 0)) {

            switch indexPath.row {
            case 0:
                PSListing.queryNewApplicationsCountInBackground({ (count) -> Void in
                    let newAppCount = count
                    if newAppCount != 0 {
                        cell.badge.backgroundColor = UIColor.redColor()
                        cell.badge.text = "\(newAppCount) \u{00A0}"
                        cell.badge.textColor = UIColor.whiteColor()
                    } else {
                        cell.badge.backgroundColor = UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 0.0)
                        cell.badge.textColor = UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 0.0)
                    }

                })
            case 1:
                PSListing.queryNewContractCountInBackground({ (count) -> Void in
                    let newContractCount = count
                    if newContractCount != 0 {
                        cell.badge.backgroundColor = UIColor.redColor()
                        cell.badge.text = "\(newContractCount) \u{00A0}"
                        cell.badge.textColor = UIColor.whiteColor()
                    } else {
                        cell.badge.backgroundColor = UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 0.0)
                        cell.badge.textColor = UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 0.0)
                    }
                })
            case 2:
                var rejectedAppsCount = 0
                PSApplication.queryRejectedApplicationsCountInBackground({ (count) -> Void in
                    rejectedAppsCount = count
                    if rejectedAppsCount != 0 {
                        cell.badge.backgroundColor = UIColor.redColor()
                        cell.badge.text = "\(rejectedAppsCount) \u{00A0}"
                        cell.badge.textColor = UIColor.whiteColor()
                    } else {
                        cell.badge.backgroundColor = UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 0.0)
                        cell.badge.textColor = UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 0.0)
                    }
                })
            case 3:
                var newReviewCount = 0
                //WONT LET ME RUN THIS FOR SOME REASON
                PSReview.queryNewReviewCountInBackground({ (count) -> Void in
                    newReviewCount = count
                    
                    if newReviewCount != 0 {
                        cell.badge.backgroundColor = UIColor.redColor()
                        cell.badge.text = "\(newReviewCount) \u{00A0}"
                        cell.badge.textColor = UIColor.whiteColor()
                    } else {
                        cell.badge.backgroundColor = UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 0.0)
                        cell.badge.textColor = UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 0.0)
                    }

                })
            default:
                break
            }
        }
        
        //Programatically set accessory type for cells (wasn't working otherwise)
        cell.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
        
        return cell
    }
    
    func tableView(tableView: UITableView, willSelectRowAtIndexPath indexPath: NSIndexPath) -> NSIndexPath? {
        switch indexPath.row {
        case 0:
            performSegueWithIdentifier("MyJobListingsSegue", sender: self)
        case 1:
            performSegueWithIdentifier("MyContractsSegue", sender: self)
        case 2:
            performSegueWithIdentifier("MyApplicationsSegue", sender: self)
        case 3:
            performSegueWithIdentifier("MyReviewsSegue", sender: self)
        default:
            break
        }
        
        return indexPath
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "SettingsSegue" {
            var child = segue.destinationViewController as! SettingsView
            child.parentView = self
        } else if segue.identifier == "MyContractsSegue" {
            var child = segue.destinationViewController as! MyContractsView
            child.parentView = self
        } else if segue.identifier == "MyJobListingsSegue" {
            var child = segue.destinationViewController as! MyJobListingsView
            child.parentView = self
        } else if segue.identifier == "MyApplicationsSegue" {
            var child = segue.destinationViewController as! MyApplicationsTable
            child.parentView = self
        } else if segue.identifier == "MyReviewsSegue" {
            var child = segue.destinationViewController as! MyReviews
            child.parentView = self
        }
        //this was me trying to get it to show "Old" jobs first when segueing straight from leaving a review, but I don't think it's that important
//        if myJobListingsSegueShowCompletedJobs == true && segue.identifier == "MyJobListingsSegue" {
//            var child = segue.destinationViewController as! MyJobListingsView
//            child.child.segmentedState = "Old"
//            myJobListingsSegueShowCompletedJobs = false
//        }
    }
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
}
