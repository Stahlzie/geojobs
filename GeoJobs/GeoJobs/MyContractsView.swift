//
//  MyContractsView.swift
//  GeoJobs
//
//  Created by Steven Patterson on 4/4/15.
//
//

import UIKit

class MyContractsView: UITableViewController, UISearchBarDelegate {
    
    var path = NSIndexPath(forRow: 0, inSection: 0)
    var parentView : HomeView!
    var data : [CellData] = []
    
    struct CellData {
        var job : PSListing!
        var user : PSUser!
        var title : String
        var price : Float
        var category : String
        var time : NSDate
    }
    
    //for searching
    var filteredData : [CellData] = []
    var searchActive : Bool = false
    var searchBar : UISearchBar!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        updateData()
        dispatch_async(dispatch_get_global_queue(Int(QOS_CLASS_USER_INITIATED.value), 0)) {
            PSApplication.setContractsSeenInBackground { (success) -> Void in
                return
            }
        }
    }
    
    func updateData() {
        self.view.userInteractionEnabled = false
        dispatch_async(dispatch_get_global_queue(Int(QOS_CLASS_USER_INITIATED.value), 0)) {
            PSListing.queryMyContractsInBackground { (listings) -> Void in
                
                for l in listings {
                    var cd = CellData(job: nil, user: nil, title: "", price: 0.0, category: "", time: NSDate())
                    cd.job = l
                    cd.title = l.title
                    cd.price = l.price.floatValue
                    cd.category = l.category
                    cd.time = l.createdAt
                    
                    l.lister.fetchIfNeeded()
                    if (l.lister == PSUser.currentUser()) {
                        //user is lister, so go extra step to get the other user's info
                        let app = l.queryAcceptedApplication()
                        if app != nil {
                            if let user = app?.applicant {
                                cd.user = user.fetchIfNeeded() as! PSUser!
                            }
                        }
                    } else {
                        //user isnt the lister, so use .lister to get the other user's info
                        cd.user = l.lister.fetchIfNeeded() as! PSUser!
                    }
                    self.data.append(cd)
                    
                }
                self.view.userInteractionEnabled = true
                dispatch_async(dispatch_get_main_queue()) {
                    self.tableView.reloadData()
                }
            }
        }
        
        //creates a search bar and sets it to be hidden until user scrolls up
        searchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: self.tableView.frame.width, height: 44))
        self.tableView.tableHeaderView = searchBar
        self.tableView.setContentOffset(CGPointMake(0.0, 44), animated: true)
        searchBar.delegate = self
        searchBar.showsCancelButton = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        if searchActive {
            return filteredData.count
        } else {
            return data.count
        }
    }
    
    func calculateTimeSinceJobPosting(jobDate: NSDate) -> String {
        let currentDate = NSDate()
        var calendar : NSCalendar = NSCalendar.currentCalendar()
        let components = calendar.components(NSCalendarUnit.CalendarUnitYear | NSCalendarUnit.CalendarUnitMonth | NSCalendarUnit.CalendarUnitWeekOfYear | NSCalendarUnit.CalendarUnitDay | NSCalendarUnit.CalendarUnitHour | NSCalendarUnit.CalendarUnitMinute, fromDate: jobDate, toDate: currentDate, options: nil)
        
        if components.year >= 1 {
            if components.year < 2 {
                return "1 year ago"
            } else {
                return "\(components.year) years ago"
            }
        } else if components.month >= 1 {
            if components.month < 2 {
                return "1 month ago"
            } else {
                return "\(components.month) months ago"
            }
        } else if components.weekOfYear >= 1 {
            if components.weekOfYear < 2 {
                return "1 week ago"
            } else {
                return "\(components.weekOfYear) weeks ago"
            }
        } else if components.day >= 1 {
            if components.day < 2 {
                return "1 day ago"
            } else {
                return "\(components.day) days ago"
            }
        } else if components.hour >= 1 {
            if components.hour < 2 {
                return "1 hour ago"
            } else {
                return "\(components.hour) hours ago"
            }
        } else {
            if components.minute >= 2 {
                return "\(components.minute) minutes ago"
            } else if components.minute < 2 && components.minute >= 1 {
                return "1 minute ago"
            } else {
                return "a moment ago"
            }
        }
    }
    
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("contractCell", forIndexPath: indexPath) as! ContractCell
        
        // Configure the cell...
        
        var thisContract : CellData!
        if searchActive {
            thisContract = filteredData[indexPath.row]
        } else {
            thisContract = data[indexPath.row]
        }
        
        
        
        //FOR APPLICANT OF CONTRACT ONLY:
        //set cell stuff to blue if new contract, and set that contract as "seen"
        if thisContract.job.lister != PSUser.currentUser() {
            if thisContract.job.status == "new contract" {
                cell.date.textColor = UIColor(red: 0, green: CGFloat(122.0/255.0), blue: 1.0, alpha: 1.0)
                cell.titleAndPrice.textColor = UIColor(red: 0, green: CGFloat(122.0/255), blue: 1.0, alpha: 1.0)
                cell.category.textColor = UIColor(red: 0, green: CGFloat(122.0/255), blue: 1.0, alpha: 1.0)
                
                thisContract.job.status = "in progress"
                thisContract.job.saveInBackground()
            }
        }
        
        
        
        cell.username.alpha = 0
        cell.titleAndPrice.alpha = 0
        cell.date.alpha = 0
        cell.category.alpha = 0
        
        cell.username.text = "You and \(thisContract.user.username)"
        var formatter = NSNumberFormatter()
        formatter.maximumFractionDigits = 2
        formatter.minimumFractionDigits = 2
        var str = formatter.stringFromNumber(thisContract.price)
        cell.titleAndPrice.text = "\(thisContract.title) - $\(str!)"
        cell.date.text = calculateTimeSinceJobPosting(thisContract.time)
        cell.category.text = thisContract.category
        
        UIView.animateWithDuration(0.6, animations: { () -> Void in
            cell.username.alpha = 1
            cell.titleAndPrice.alpha = 1
            cell.date.alpha = 1
            cell.category.alpha = 1
        })
        
        return cell
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let child = segue.destinationViewController as! ContactView
        
        if searchActive {
            child.otherUser = filteredData[path.row].user
            child.contract = filteredData[path.row].job
        } else {
            child.otherUser = data[path.row].user
            child.contract = data[path.row].job
        }
    }
    
    override func tableView(tableView: UITableView, willSelectRowAtIndexPath indexPath: NSIndexPath) -> NSIndexPath? {
        
        path = indexPath
        return indexPath
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
    
    //back button pressed
    override func viewWillDisappear(animated: Bool) {
        parentView.tableView.reloadData()
    }
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        
        if count(searchText) == 0 {
            searchActive = false;
            self.tableView.reloadData()
            return
        } else {
            searchActive = true;
        }
        
        var index : Int = -1
        
        filteredData = data.filter {
            index++
              
            if ($0.title as NSString).rangeOfString(searchText, options: .CaseInsensitiveSearch).location != NSNotFound {
                return true
            }
            if ($0.user.username as NSString).rangeOfString(searchText, options: .CaseInsensitiveSearch).location != NSNotFound {
                return true
            }
            if ($0.category as NSString).rangeOfString(searchText, options: .CaseInsensitiveSearch).location != NSNotFound {
                return true
            }
            if ($0.user.username as NSString).rangeOfString(searchText, options: .CaseInsensitiveSearch).location != NSNotFound {
                return true
            }
            return false
            
        }
        self.tableView.reloadData()
    }
    
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
}
