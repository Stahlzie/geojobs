//
//  MyReviews.swift
//  GeoJobs
//
//  Created by Kory Kappel on 3/25/15.
//
//

import UIKit

class MyReviews: UITableViewController, UISearchBarDelegate  {
    
    var reviews:[PSReview] = []
    var path = NSIndexPath(forRow: 0, inSection: 0)
    var sortByCategory = "new"
    var parentView : HomeView!
    var data : [CellData] = []
    
    //for searching
    var filteredData : [CellData] = []
    var filteredReviews : [PSReview] = []
    var searchActive : Bool = false
    var searchBar : UISearchBar!
    
    struct CellData {
        var review : PSReview!
        var listing : PSListing!
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        self.view.userInteractionEnabled = false
        dispatch_async(dispatch_get_global_queue(Int(QOS_CLASS_USER_INITIATED.value), 0)) {
            PSReview.queryReviewsOfUser(PSUser.currentUser(), completion: { (reviews) -> Void in
                self.reviews = reviews
                for rev : PSReview in reviews {
                    var cd = CellData(review: nil, listing: nil)
                    cd.review = rev.fetchIfNeeded() as! PSReview!
                    cd.listing = cd.review.listing.fetchIfNeeded() as! PSListing
                    self.data.append(cd)
                }
                self.view.userInteractionEnabled = true
                dispatch_async(dispatch_get_main_queue()) {
                    self.tableView.reloadData()
                }
            })
        }
        //PSReview.setReviewsViewedInBackground()
        
        //creates a search bar and sets it to be hidden until user scrolls up
        searchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: self.tableView.frame.width, height: 44))
        self.tableView.tableHeaderView = searchBar
        self.tableView.setContentOffset(CGPointMake(0.0, 44), animated: true)
        searchBar.delegate = self
        searchBar.showsCancelButton = true
    }
    
    func sortReviews(option: String) {
        switch option {
        case "new":
            reviews.sort({ $0.createdAt.timeIntervalSinceNow < $1.createdAt.timeIntervalSinceNow })
        case "category":
            reviews.sort({ $0.listing.category < $1.listing.category })
        default:
            reviews.sort({ $0.createdAt.timeIntervalSinceNow < $1.createdAt.timeIntervalSinceNow })
        }
    }
    
    func updateData() {
        self.sortReviews(self.sortByCategory)
        self.tableView.reloadData()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        if searchActive {
            return filteredReviews.count
        } else {
            return reviews.count
        }
    }
    
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as! MyReviewsCell
        
        // Configure the cell...
        
        cell.jobTitleandPrice.alpha = 0
        cell.reviewDescription.alpha = 0
        cell.starRating.alpha = 0
        cell.category.alpha = 0
        
        var review : PSReview!
        var listing : PSListing!
        if !searchActive {
            review = data[indexPath.row].review
            listing = data[indexPath.row].listing
        } else {
            review = filteredData[indexPath.row].review
            listing = filteredData[indexPath.row].listing
        }
        
        //set them to blue if new contract, and set that contract as "seen"
        if review.status == "new" {
            cell.jobTitleandPrice.textColor = UIColor(red: 0, green: CGFloat(122.0/255), blue: 1.0, alpha: 1.0)
            cell.category.textColor = UIColor(red: 0, green: CGFloat(122.0/255), blue: 1.0, alpha: 1.0)
            //cell.reviewDescription.textColor = UIColor(red: 0, green: CGFloat(122.0/255), blue: 1.0, alpha: 1.0)
            
            review.status = "viewed"
            review.saveInBackground()
        }
        
        
        var actInd : UIActivityIndicatorView = UIActivityIndicatorView(frame: CGRectMake(0,0, 50, 50)) as UIActivityIndicatorView
        actInd.center = self.view.center
        actInd.hidesWhenStopped = true
        actInd.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.Gray
        UIApplication.sharedApplication().beginIgnoringInteractionEvents()
        actInd.startAnimating()
        view.addSubview(actInd)
        
        
        //STOP SPINNER HERE:
        UIApplication.sharedApplication().endIgnoringInteractionEvents()
        actInd.stopAnimating()
        actInd.removeFromSuperview()
        
        
        var formatter = NSNumberFormatter()
        formatter.maximumFractionDigits = 2
        formatter.minimumFractionDigits = 2
        let str = formatter.stringFromNumber(listing.price)
        cell.jobTitleandPrice.text = "\(listing.title) - $\(str!)"
        cell.reviewDescription.text = review.reviewText
        cell.starRating.rating = Float(review.rating)
        cell.category.text = listing.category
        
        UIView.animateWithDuration(0.6, animations: { () -> Void in
            cell.jobTitleandPrice.alpha = 1
            cell.reviewDescription.alpha = 1
            cell.starRating.alpha = 1
            cell.category.alpha = 1
        })
        
        return cell
    }
    
    override func tableView(tableView: UITableView, willSelectRowAtIndexPath indexPath: NSIndexPath) -> NSIndexPath? {
        
        //performSegueWithIdentifier("singleReviewSegue", sender: self)
        path = indexPath
        return indexPath
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "sortSegue" {
            var child = segue.destinationViewController as! SortReviews
            child.parentView = self
        } else {
            let child = segue.destinationViewController as! SingleReview
            if searchActive {
                child.review = filteredReviews[path.row]
            } else {
                child.review = reviews[path.row]
            }
        }
    }
    
    //back button pressed
    override func viewWillDisappear(animated: Bool) {
        parentView.tableView.reloadData()
    }
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        
        if count(searchText) == 0 {
            searchActive = false;
            self.tableView.reloadData()
            return
        } else {
            searchActive = true;
        }
        
        var index : Int = -1
        self.filteredReviews = []
        dispatch_async(dispatch_get_global_queue(Int(QOS_CLASS_USER_INITIATED.value), 0)) {
            self.filteredData = self.data.filter {
                index++
                
                if ($0.listing.title as NSString).rangeOfString(searchText, options: .CaseInsensitiveSearch).location != NSNotFound {
                    self.filteredReviews.append(self.reviews[index])
                    return true
                }
                $0.review.reviewingUser.fetchIfNeeded()
                if ($0.review.reviewingUser.username as NSString).rangeOfString(searchText, options: .CaseInsensitiveSearch).location != NSNotFound {
                    self.filteredReviews.append(self.reviews[index])
                    return true
                }
                if ($0.listing.category as NSString).rangeOfString(searchText, options: .CaseInsensitiveSearch).location != NSNotFound {
                    self.filteredReviews.append(self.reviews[index])
                    return true
                }
                if ($0.review.reviewText as NSString).rangeOfString(searchText, options: .CaseInsensitiveSearch).location != NSNotFound {
                    self.filteredReviews.append(self.reviews[index])
                    return true
                }
                return false
                
            }
            dispatch_async(dispatch_get_main_queue()) {
                self.tableView.reloadData()
            }
        }
    }
    
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
}
