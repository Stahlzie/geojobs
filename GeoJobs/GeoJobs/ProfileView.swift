//
//  ProfileView.swift
//  GeoJobs
//
//  Created by Steven Patterson on 3/11/15.
//
//

import UIKit

class ProfileView: UITableViewController {
    
    var averageRating : Float = 0.0
    var user : PSUser!
    var reviews:[PSReview] = []
    var reviewingUsers : [String] = []
    
    var path = NSIndexPath(forRow: 0, inSection: 0)

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        //tableView.estimatedRowHeight = 94
        //tableView.rowHeight = UITableViewAutomaticDimension
        
        //var headerCell : ProfileHeaderCell = tableView.cellForRowAtIndexPath(NSIndexPath(index: 0)) as ProfileHeaderCell
        dispatch_async(dispatch_get_global_queue(Int(QOS_CLASS_USER_INITIATED.value), 0)) {
            PSReview.queryReviewsOfUser(self.user, completion: { (reviews) -> Void in
                
                self.reviews = reviews
                for rev in reviews {
                    rev.fetchIfNeeded()
                    let u = rev.reviewingUser.fetchIfNeeded() as! PSUser
                    self.reviewingUsers.append(u.username)
                }
                self.user = self.user.fetchIfNeeded() as! PSUser!
                self.averageRating = (Float)(PSReview.queryAverageRatingOfUser(self.user))
                dispatch_async(dispatch_get_main_queue()) {
                    self.tableView.reloadData()
                }
                
                
            })
        }
    }
    
//    override func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
//        if indexPath == 0 {
//            return 120
//        } else {
//            return 94
//        }
//    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return reviews.count + 1
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        //user.fetchIfNeeded()

        //set up cells
        if indexPath.row == 0 {
            var cell = tableView.dequeueReusableCellWithIdentifier("profileHeaderCell", forIndexPath: indexPath) as! ProfileHeaderCell
            cell.selectionStyle = UITableViewCellSelectionStyle.None
            
//            var actInd : UIActivityIndicatorView = UIActivityIndicatorView(frame: CGRectMake(0,0, 50, 50)) as UIActivityIndicatorView
//            actInd.center = self.view.center
//            actInd.hidesWhenStopped = true
//            actInd.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.Gray
//            view.addSubview(actInd)
            
            
            //START SPINNER:
            //actInd.startAnimating()
            //UIApplication.sharedApplication().beginIgnoringInteractionEvents()
            //UNCOMMENT ONCE WE GET IMAGES WORKING
            user.getProfileImageInBackground({ (image) -> Void in
                cell.profilePicture.image = image
            })
            cell.username.text = user.username
//            PSReview.queryReviewCountInBackground(user, completion: { (count, error) -> Void in
//                let temp : Int = Int(count)
//                if temp == 1 {
//                    cell.numReviews.text = "\(temp) review"
//                } else {
//                    cell.numReviews.text = "\(temp) reviews"
//                }
//                
//                //STOP SPINNER HERE:
//                UIApplication.sharedApplication().endIgnoringInteractionEvents()
//                actInd.stopAnimating()
//                actInd.removeFromSuperview()
//            })
            if reviews.count == 1 {
                cell.numReviews.text = "1 review"
            } else {
                cell.numReviews.text = "\(reviews.count) reviews"
            }
            cell.rating.rating = averageRating
            return cell
        } else {
            var cell = tableView.dequeueReusableCellWithIdentifier("profileReviewCell", forIndexPath: indexPath) as! ProfileReviewCell
            cell.owner.text = reviewingUsers[indexPath.row-1]
            cell.reviewText.text = reviews[indexPath.row-1].reviewText
            cell.rating.rating = (Float)(reviews[indexPath.row-1].rating)
            return cell
        }
    }
    
    override func tableView(tableView: UITableView, willSelectRowAtIndexPath indexPath: NSIndexPath) -> NSIndexPath? {
        path = indexPath
        return indexPath
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let child = segue.destinationViewController as! SingleReview
        child.review = reviews[path.row-1]
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

}
