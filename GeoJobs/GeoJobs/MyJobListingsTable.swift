//
//  MyJobListingsTable.swift
//  GeoJobs
//
//  Created by Kory Kappel on 3/26/15.
//
//

import UIKit

class MyJobListingsTable: UITableViewController, UISearchBarDelegate {
    
    var segmentedState = "New"
    
    //var jobs:[PSListing] = []
    var newJobs:[PSListing] = []
    var oldJobs:[PSListing] = []
    var newApps = Array<Array<PSApplication>>()
    var oldApps = Array<Array<PSApplication>>()
    
    //for searching
    var filteredNewJobs : [PSListing] = []
    var filteredOldJobs : [PSListing] = []
    var filteredOldApps = Array<Array<PSApplication>>()
    var filteredNewApps = Array<Array<PSApplication>>()
    var searchActive : Bool = false
    var searchBar : UISearchBar!
    
    var path = NSIndexPath(forRow: 0, inSection: 0)

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        //creates a search bar and sets it to be hidden until user scrolls up
        searchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: self.tableView.frame.width, height: 44))
        self.tableView.tableHeaderView = searchBar
        dispatch_async(dispatch_get_main_queue()) {
            self.tableView.setContentOffset(CGPointMake(0.0, 44), animated: false)
        }
        searchBar.delegate = self
        searchBar.showsCancelButton = true
        
        updateData()
    }
    
    //will only be called once, from viewDidLoad
    func updateData() {
        /* Fetch from local datastore
        // But there are other dependencies
        PSListing.queryMyListingsInBackground(true) { (listings) -> Void in
            self.jobs = listings
            for job in self.jobs {
                //separates old from new jobs for use in the segmented view
                if job.status == "accepting" {
                    self.newJobs.append(job)
                }
                else if job.status == "complete" {
                    self.oldJobs.append(job)
                }
            }
            
        self.tableView.reloadData()
        }
        */
        
        self.view.userInteractionEnabled = false
        
        dispatch_async(dispatch_get_global_queue(Int(QOS_CLASS_USER_INITIATED.value), 0)) {
            PSListing.queryMyListingsInBackground(false) { (listings) -> Void in
                //self.jobs = listings
                //PSListing.pinAllInBackground(listings)
                dispatch_async(dispatch_get_global_queue(Int(QOS_CLASS_USER_INITIATED.value), 0)) {
                    for job in listings {
                    
                        //self.applications.append(job.queryApplications())
                        //separates old from new jobs for use in the segmented view
                        if job.status == "accepting" {
                            self.newJobs.append(job)
                            self.newApps.append(job.queryApplications())
//                            for app in self.newApps[self.newApps.endIndex] {
//                                app.applicant.fetchIfNeededInBackground()
//                            }
                        }
                        else if job.status == "complete" {
                            self.oldJobs.append(job)
                            self.oldApps.append(job.queryApplications())
//                            for app in self.oldApps[self.oldApps.endIndex] {
//                                app.applicant.fetchIfNeededInBackground()
//                            }
                        }
                    }
                    self.view.userInteractionEnabled = true
                    dispatch_async(dispatch_get_main_queue()) {
                        self.tableView.reloadData()
                    }
                }
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        
        if segmentedState == "New"  {
            if searchActive {
                return filteredNewJobs.count
            } else {
                return newJobs.count
            }
        }
        //else use old jobs array
        if searchActive {
            return filteredOldJobs.count
        } else {
            return oldJobs.count
        }
    }
    
    func calculateTimeSinceJobPosting(jobDate: NSDate) -> String {
        let currentDate = NSDate()
        var calendar : NSCalendar = NSCalendar.currentCalendar()
        let components = calendar.components(NSCalendarUnit.CalendarUnitYear | NSCalendarUnit.CalendarUnitMonth | NSCalendarUnit.CalendarUnitWeekOfYear | NSCalendarUnit.CalendarUnitDay | NSCalendarUnit.CalendarUnitHour | NSCalendarUnit.CalendarUnitMinute, fromDate: jobDate, toDate: currentDate, options: nil)
        
        if components.year >= 1 {
            if components.year < 2 {
                return "1 year ago"
            } else {
                return "\(components.year) years ago"
            }
        } else if components.month >= 1 {
            if components.month < 2 {
                return "1 month ago"
            } else {
                return "\(components.month) months ago"
            }
        } else if components.weekOfYear >= 1 {
            if components.weekOfYear < 2 {
                return "1 week ago"
            } else {
                return "\(components.weekOfYear) weeks ago"
            }
        } else if components.day >= 1 {
            if components.day < 2 {
                return "1 day ago"
            } else {
                return "\(components.day) days ago"
            }
        } else if components.hour >= 1 {
            if components.hour < 2 {
                return "1 hour ago"
            } else {
                return "\(components.hour) hours ago"
            }
        } else {
            if components.minute >= 2 {
                return "\(components.minute) minutes ago"
            } else if components.minute < 2 && components.minute >= 1 {
                return "1 minute ago"
            } else {
                return "a moment ago"
            }
        }
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as! MyJobListingsCell

        // Configure the cell...
        
        //set all stuff to invisible for use in animation
        cell.jobTitleAndPrice.alpha = 0
        cell.timePosted.alpha = 0
        cell.category.alpha = 0
        cell.numReplies.alpha = 0
        
        var thisJob : PSListing!
        if segmentedState == "New" {
            if searchActive {
                thisJob = filteredNewJobs[indexPath.row]
            } else {
                thisJob = newJobs[indexPath.row]
            }
        } else {
            if searchActive {
                thisJob = filteredOldJobs[indexPath.row]
            } else {
                thisJob = oldJobs[indexPath.row]
            }
        }
        
        //black by default
        cell.timePosted.textColor = UIColor.darkGrayColor()
        cell.jobTitleAndPrice.textColor = UIColor.blackColor()
        cell.category.textColor = UIColor.darkGrayColor()
        cell.numReplies.textColor = UIColor.blackColor()
        
        //set them to blue if it has any new applications
        if segmentedState == "New" {
            for app in newApps[indexPath.row] {
                if app.status == "new" {
                    cell.timePosted.textColor = UIColor(red: 0, green: CGFloat(122.0/255.0), blue: 1.0, alpha: 1.0)
                    cell.jobTitleAndPrice.textColor = UIColor(red: 0, green: CGFloat(122.0/255), blue: 1.0, alpha: 1.0)
                    cell.category.textColor = UIColor(red: 0, green: CGFloat(122.0/255), blue: 1.0, alpha: 1.0)
                    cell.numReplies.textColor = UIColor(red: 0, green: CGFloat(122.0/255), blue: 1.0, alpha: 1.0)
                }
            }
        }
        
        var formatter = NSNumberFormatter()
        formatter.maximumFractionDigits = 2
        formatter.minimumFractionDigits = 2
        let str = formatter.stringFromNumber(thisJob.price)
        cell.jobTitleAndPrice.text = "\(thisJob.title) - $\(str!)"
        cell.timePosted.text = calculateTimeSinceJobPosting(thisJob.createdAt)
        cell.category.text = thisJob.category
        var numReplies : Int!
        if segmentedState == "New" {
            if searchActive {
                numReplies = filteredNewApps[indexPath.row].count
            } else {
                numReplies = newApps[indexPath.row].count
            }
        } else {
            if searchActive {
                numReplies = filteredOldApps[indexPath.row].count
            } else {
                numReplies = oldApps[indexPath.row].count
            }
        }
        if numReplies == 1 {
            cell.numReplies.text = (String)(numReplies) + " reply"
        } else {
            cell.numReplies.text = (String)(numReplies) + " replies"
        }
        
        
        UIView.animateWithDuration(0.6, animations: { () -> Void in
            cell.jobTitleAndPrice.alpha = 1
            cell.timePosted.alpha = 1
            cell.category.alpha = 1
            cell.numReplies.alpha = 1
        })
        
        return cell
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let child = segue.destinationViewController as! ApplicantsTableView
        if segmentedState == "New" {
            if searchActive {
                child.applications = filteredNewApps[path.row]
            } else {
                child.applications = newApps[path.row]
            }
        } else {
            if searchActive {
                child.applications = filteredOldApps[path.row]
            } else {
                child.applications = oldApps[path.row]
            }
        }

    }
    
    override func tableView(tableView: UITableView, willSelectRowAtIndexPath indexPath: NSIndexPath) -> NSIndexPath? {
        
        path = indexPath
        return indexPath
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if segmentedState == "New" {
            newJobs[indexPath.row].setApplicationsPendingInBackground()
        }
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        
        if count(searchText) == 0 {
            searchActive = false;
            self.tableView.reloadData()
            return
        } else {
            searchActive = true;
        }
        
        var index : Int = -1
        
        if segmentedState == "Old" {
            //self.filteredOldJobs = []
            self.filteredOldApps = []
            filteredOldJobs = oldJobs.filter {
                index++
                
                if ($0.title as NSString).rangeOfString(searchText, options: .CaseInsensitiveSearch).location != NSNotFound {
                    self.filteredOldApps.append(self.oldApps[index])
                    return true
                }
                if ($0.category as NSString).rangeOfString(searchText, options: .CaseInsensitiveSearch).location != NSNotFound {
                    self.filteredOldApps.append(self.oldApps[index])
                    return true
                }
                //if an applicant with given name has applied
                for applicant in self.oldApps[index] {
//                    dispatch_async(dispatch_get_global_queue(Int(QOS_CLASS_USER_INITIATED.value), 0)) {
//                        applicant.applicant.fetchIfNeeded()
//                    }
                    applicant.applicant.fetchIfNeeded()
                    if (applicant.applicant.username as NSString).rangeOfString(searchText, options: .CaseInsensitiveSearch).location != NSNotFound {
                        self.filteredOldApps.append(self.oldApps[index])
                        return true
                    }
                }
                return false
            }
        } else {
            //self.filteredNewJobs = []
            self.filteredNewApps = []
            filteredNewJobs = newJobs.filter {
                index++
                
                if ($0.title as NSString).rangeOfString(searchText, options: .CaseInsensitiveSearch).location != NSNotFound {
                    self.filteredNewApps.append(self.newApps[index])
                    return true
                }
                if ($0.category as NSString).rangeOfString(searchText, options: .CaseInsensitiveSearch).location != NSNotFound {
                    self.filteredNewApps.append(self.newApps[index])
                    return true
                }
                //also search by applicant name
                for applicant in self.newApps[index] {
//                    dispatch_async(dispatch_get_global_queue(Int(QOS_CLASS_USER_INITIATED.value), 0)) {
//                        applicant.applicant.fetchIfNeeded()
//                    }
                    applicant.applicant.fetchIfNeeded()
                    if (applicant.applicant.username as NSString).rangeOfString(searchText, options: .CaseInsensitiveSearch).location != NSNotFound {
                        self.filteredNewApps.append(self.newApps[index])
                        return true
                    }
                }
                return false
            }
        }
        
        self.tableView.reloadData()
    }

    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }

}
