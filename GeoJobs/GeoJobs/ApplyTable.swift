//
//  ApplyTable.swift
//  GeoJobs
//
//  Created by Kory Kappel on 3/12/15.
//
//

import UIKit

class ApplyTable: UITableViewController {

    @IBOutlet weak var jobDescription : UITextView!
    @IBOutlet weak var priceField : UITextField!
    @IBOutlet weak var pitch : UITextView!
    
    var price : Double = 0.0
    
    @IBAction func validatePrice() {
        var temp = (priceField.text as NSString).doubleValue
        
        //if negative price, set to 0
        if temp < 0 {
            temp = 0
        }
        //round to two decimal places
        let rounded = Double(round(100*temp)/100)
        price = rounded
        
        var formatter = NSNumberFormatter()
        formatter.maximumFractionDigits = 2
        formatter.minimumFractionDigits = 2
        var str = formatter.stringFromNumber(rounded)
        priceField.text = str
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
