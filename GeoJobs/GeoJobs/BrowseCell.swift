//
//  BrowseCell.swift
//  GeoJobs
//
//  Created by Steven Patterson on 3/11/15.
//
//

import UIKit

class BrowseCell: UITableViewCell {
    
    @IBOutlet var titleAndPrice : UILabel!
    @IBOutlet var userAndRating : UILabel!
    @IBOutlet var distance : UILabel!
    @IBOutlet var numReplies : UILabel!
    @IBOutlet var category : UILabel!
    @IBOutlet var rating : FloatRatingView!
    @IBOutlet var numReviews : UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
