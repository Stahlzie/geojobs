//
//  ContactView.swift
//  GeoJobs
//
//  Created by Steven Patterson on 3/12/15.
//
//

import UIKit

class ContactView: UIViewController {
    
    @IBOutlet var info : UILabel!
    
    @IBOutlet weak var infoText: UILabel!
    
    @IBOutlet weak var leaveAReviewButton: UIButton!
    
    var otherUser : PSUser!
    var contract : PSListing!


    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        var subview = self.childViewControllers[0] as! ContactSubView
        subview.name.text = otherUser.username
        subview.email.text = otherUser.email
        
        self.leaveAReviewButton.enabled = false
        
        PSReview.queryILeftReviewForContractInBackground(contract, completion: { (reviewLeft) -> Void in
            if reviewLeft == true {
                self.infoText.text = "You have already left a review for this contract"
            } else {
                self.leaveAReviewButton.enabled = true
            }
        })
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation
    */
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "LeaveAReview" {
            var child = segue.destinationViewController as! LeaveAReview
            child.reviewedUser = otherUser
            child.listing = contract
        }
    }


}
