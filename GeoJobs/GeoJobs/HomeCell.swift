//
//  HomeCell.swift
//  GeoJobs
//
//  Created by Steven Patterson on 2/27/15.
//
//

import UIKit

class HomeCell: UITableViewCell {
    
    //@IBOutlet weak var label : UILabel!
    @IBOutlet weak var badge : UILabel!
    @IBOutlet weak var label : UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
