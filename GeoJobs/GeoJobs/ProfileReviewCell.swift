//
//  profileReviewCell.swift
//  GeoJobs
//
//  Created by Steven Patterson on 3/11/15.
//
//

import UIKit

class ProfileReviewCell: UITableViewCell {
    
    @IBOutlet var owner : UILabel!
    @IBOutlet var reviewText : UILabel!
    @IBOutlet var rating : FloatRatingView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
