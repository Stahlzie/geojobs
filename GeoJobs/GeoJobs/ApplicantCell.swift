//
//  ApplicantCell.swift
//  GeoJobs
//
//  Created by Steven Patterson on 3/13/15.
//
//

import UIKit

class ApplicantCell: UITableViewCell {
    
    @IBOutlet var applicant : UILabel!
    @IBOutlet var rating : FloatRatingView!
    @IBOutlet var numReviews : UILabel!
    @IBOutlet var price : UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        //applicant.preferredMaxLayoutWidth = 145
        //applicant.sizeToFit()
        rating.sizeToFit()
        applicant.text = "\u{00A0}"
        numReviews.text = "\u{00A0}"
        price.text = "\u{00A0}"
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
