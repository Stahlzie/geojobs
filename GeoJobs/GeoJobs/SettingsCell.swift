//
//  SettingsCell.swift
//  GeoJobs
//
//  Created by Steven Patterson on 3/3/15.
//
//

import UIKit

class SettingsCell: UITableViewCell {
    
    
    @IBOutlet weak var switcher : UISwitch!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
