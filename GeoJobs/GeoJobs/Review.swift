//
//  Review.swift
//  GeoJobs
//
//  Created by Nathan Snyder on 2015-03-21.
//
//

import Foundation

public class PSReview : PFObject, PFSubclassing {
//    struct ReviewStatus {
//        let NewStatus = "new"
//        let Viewed = "viewed"
//    }
    
    /*
    // Initializers
    */
    
    override public class func initialize() {
        var onceToken : dispatch_once_t = 0;
        dispatch_once(&onceToken) {
            //self.registerSubclass()
        }
    }
    
    public override init() {
        super.init()
    }
    
    /**
        Initialize a new review
    */
    public init(reviewText: String,
        listing: PSListing,
        rating: NSNumber,
        reviewedUser: PSUser,
        reviewingUser: PSUser
        ) {
            super.init()
            let acl = PFACL()
            acl.setPublicReadAccess(true)
            acl.setPublicWriteAccess(true)
            
            self.reviewText = reviewText
            self.listing = listing
            self.rating = rating
            self.reviewedUser = reviewedUser
            self.reviewingUser = reviewingUser
            self.status = "new"
            self.ACL = acl
    } // tested
    
    /*
    // Class Functions
    */
    
    public class func parseClassName() -> String! {
        return "Review"
    }
    
    public class func queryReviewsOfUser(user: PSUser, completion: (reviews: [PSReview]!) -> Void) {
        var rating: Double = 0.0
        // calculate the user rating via cloud code
        
        let revQuery = PSReview.query()
        revQuery.whereKey("reviewedUser", equalTo: user)
        // WARN PLS
        let revObjects = revQuery.findObjects() // warnBlockingOperationOnMainThread
        
        var reviews: [PSReview] = []
        if revObjects != nil {
            for obj in revObjects {
                reviews.append( obj as! PSReview )
            }
        }
        
        completion(reviews:reviews)
    } // tested
    
    /**
    Attempt to get a single review from the database
    
    :param: id Parse object id of the review (should be able to get from a list of reviews)
    :return: Review object if it finds one, or nil if it doesn't
    */
    public class func  queryReviewById(id: String) -> PSReview! {
        var revQuery = PSReview.query()
        let revObject = revQuery.getObjectWithId(id)
        
        if(revObject != nil) {
            return revObject as! PSReview
        } else {
            return nil
        }
    } // tested
    
    public class func queryReviewCount(user: PSUser) -> Int {
        let query = PSReview.query()
        query.whereKey("reviewedUser", equalTo: user)
        return query.countObjects()
    } //tested
    
    public class func queryReviewCountInBackground(user: PSUser, completion: (count: Int32, error: NSError!) -> Void) {
        let query = PSReview.query()
        query.whereKey("reviewedUser", equalTo: user)
        query.countObjectsInBackgroundWithBlock {
            (count: Int32, error: NSError!) -> Void in
            completion(count:count, error: error)
        }
    } //tested
    
    // Todo: update this with out enum
    public class func queryNewReviewCountInBackground(completion: (count: Int) -> Void) {
        let query = PSReview.query()
        query.whereKey("reviewedUser", equalTo: PFUser.currentUser())
        query.whereKey("status", equalTo: "new")
        query.countObjectsInBackgroundWithBlock {
            (count: Int32, error: NSError!) -> Void in
            completion(count:Int(count))
        }
    } //NEEDS TESTED
    
    public class func queryAverageRatingOfUser(user: PSUser) -> Double {
        let query = PSReview.query()
        query.whereKey("reviewedUser", equalTo: user)
        query.selectKeys(["rating"])
        var ratings = query.findObjects()
        
        if ratings == nil {
            return 0
        }
        
        if ratings.count != 0 {
            var sum = Int(0)
            for rating in ratings {
                sum += rating.rating.integerValue
            }
            var average = Double(sum) / Double(ratings.count)
            return average
        } else {
            return 0
        }
    } //NEEDS TESTED
    
    public class func queryAverageRatingOfUserInBackground(user: PSUser, completion:(rating: Double) -> Void) {
        let query = PSReview.query()
        query.whereKey("reviewedUser", equalTo: user)
        query.selectKeys(["rating"])
        query.findObjectsInBackgroundWithBlock { (ratings, error) -> Void in
            if error != nil {
                completion(rating: Double(0.0))
            }
            if ratings != nil {
                if ratings.count != 0 {
                    var sum = Int(0)
                    for rating in ratings {
                        sum += rating.rating.integerValue
                    }
                    var average = Double(sum) / Double(ratings.count)
                    completion(rating: average)
                } else {
                    completion(rating: Double(0.0))
                }
            }

        }
    } //NEEDS TESTED
    
    public class func queryReviewLeftForContract(listing: PSListing) -> Bool {
        let query = PSReview.query()
        query.whereKey("reviewedUser", equalTo: PSUser.currentUser())
        query.whereKey("listing", equalTo: listing)
        if query.countObjects() > 0 {
            return true
        } else {
            return false
        }
    } // tested
    
    //Note: this is to check if OTHER party left a review for this contract
    public class func queryReviewLeftForContractInBackground(listing: PSListing, completion: (reviewLeft: Bool) -> Void) {
        let query = PSReview.query()
        query.whereKey("reviewedUser", equalTo: PSUser.currentUser())
        query.whereKey("listing", equalTo: listing)
        query.countObjectsInBackgroundWithBlock { (count, error) -> Void in
            if count > 0 {
                completion(reviewLeft: true)
            } else {
                completion(reviewLeft: false)
            }
        }
    } //tested
    
    //Note: this is to check if *I* left a review for this contract
    public class func queryILeftReviewForContractInBackground(listing: PSListing, completion: (reviewLeft: Bool) -> Void) {
        let query = PSReview.query()
        query.whereKey("reviewingUser", equalTo: PSUser.currentUser())
        query.whereKey("listing", equalTo: listing)
        query.countObjectsInBackgroundWithBlock { (count, error) -> Void in
            if count > 0 {
                completion(reviewLeft: true)
            } else {
                completion(reviewLeft: false)
            }
        }
    } //tested
    
    class func setReviewsViewedInBackground() {
        let query = PSReview.query()
        query.whereKey("reviewedUser", equalTo: PFUser.currentUser())
        query.whereKey("status", equalTo: "new")
        query.findObjectsInBackgroundWithBlock { (reviews, error) -> Void in
            if error == nil {
                if let revs : [PSReview]! = reviews as! [PSReview]! {
                    for rev : PSReview in revs {
                        rev.status = "viewed"
                    }
                    self.saveAllInBackground(reviews, block: nil)
                }
            }
        }
    } // NEEDS TESTED

    
    /*
    // Properties
    */
    
    @NSManaged public var reviewText: String
    @NSManaged public var rating: NSNumber
    @NSManaged public var status: String
    @NSManaged public var listing: PSListing
    @NSManaged public var reviewedUser: PSUser
    @NSManaged public var reviewingUser: PSUser
}