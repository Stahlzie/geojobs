//
//  StartScreen.swift
//  GeoJobs
//
//  Created by Kory Kappel on 3/1/15.
//
//

import UIKit

class StartScreen: UIViewController, LogInDelegate, SignUpDelegate {
    
    func cancel(child: UIViewController) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func browseWithoutAccountButtonPressed(sender: UIButton) {
        //present the Browse page embedded in a nav controller
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let browseView = storyboard.instantiateViewControllerWithIdentifier("browse") as! BrowseView
        var backButton = UIBarButtonItem(title: "Back", style: .Plain, target: self, action: "backButtonPressed")
        browseView.navigationItem.leftBarButtonItem = backButton
        let navigationController = UINavigationController(rootViewController: browseView)
        self.presentViewController(navigationController, animated: true, completion: nil)
    }
    
    func logOut() {
        dismissViewControllerAnimated(true, completion: nil) //THIS REMOVES THE ENTIRE NAV CONTROLLER
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func backButtonPressed() {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    override func prepareForSegue(segue:
        UIStoryboardSegue, sender: AnyObject?) {
            
            //These are for the "cancel" button
            if segue.identifier == "StartToLogInSegue" {
                let child = segue.destinationViewController as! LogIn
                child.delegate = self
            }
            if segue.identifier == "StartToSignUpSegue" {
                let child = segue.destinationViewController as! SignUp
                child.delegate = self
            }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let center = NSNotificationCenter.defaultCenter()
        //watch for notification for browse without an account functionality
        center.addObserver(self, selector: "createAccount", name: "createAccount", object: nil)
        center.addObserver(self, selector: "logOut", name: "logOut", object: nil)
    }
    
    func createAccount() {
        //return to this screen then push Sign Up screen
        dismissViewControllerAnimated(true, completion: nil)
        performSegueWithIdentifier("StartToSignUpSegue", sender: self)
    }
    
    override func viewDidAppear(animated: Bool) {
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        if appDelegate.firstTimeLoaded! && PSUser.currentUser() != nil {
            //let storyboard = UIStoryboard(name: "Main", bundle: nil)
            //let viewController = storyboard.instantiateViewControllerWithIdentifier("HomeView") as! UINavigationController
            //self.presentViewController(viewController, animated: true, completion: nil)
            self.performSegueWithIdentifier("StartToHomeSegue", sender: self)
        }
        appDelegate.firstTimeLoaded! = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
