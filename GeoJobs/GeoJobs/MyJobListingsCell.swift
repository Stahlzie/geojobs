//
//  MyJobListingsCell.swift
//  GeoJobs
//
//  Created by Kory Kappel on 3/26/15.
//
//

import UIKit

class MyJobListingsCell: UITableViewCell {
    
    @IBOutlet weak var jobTitleAndPrice : UILabel!
    @IBOutlet weak var category : UILabel!
    @IBOutlet weak var timePosted : UILabel!
    @IBOutlet weak var numReplies : UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
