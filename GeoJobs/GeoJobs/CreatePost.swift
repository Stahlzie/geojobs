//
//  CreatePost.swift
//  GeoJobs
//
//  Created by Steven Patterson on 2/27/15.
//
//

import UIKit

// Todo: Code Review add this to relevant folder

class CreatePost : UITableViewController, UITextViewDelegate, UIPickerViewDelegate, UIPickerViewDataSource {
    
    var categories = [
        Constants.categories
    ]
    
    @IBAction func PostJob(sender: AnyObject) {
        if self.userLocation == nil {
            gettingLocationAlert.hidden = false
            gettingLocationAlert.show()
        } else {
            createPost()
        }
    }
    
    @IBAction func validatePrice() {
        var temp = (priceInput.text as NSString).doubleValue
        
        //if negative price, set to 0
        if temp < 0 {
            temp = 0
        }
        //round to two decimal places
        let rounded = Double(round(100*temp)/100)
        price = rounded
        
        var formatter = NSNumberFormatter()
        formatter.maximumFractionDigits = 2
        formatter.minimumFractionDigits = 2
        var str = formatter.stringFromNumber(rounded)
        priceInput.text = str
    }

    
    @IBOutlet weak var titleInput : UITextField!
    @IBOutlet weak var priceInput : UITextField!
    @IBOutlet weak var descriptionInput : UITextView!
    @IBOutlet weak var categoryInput : UIPickerView!
    @IBOutlet weak var zipcodeInput : UITextField!
    
    var price : Double = 0.0
    
    var userLocation : PFGeoPoint!
    var gettingLocationAlert : UIAlertView!
    
    func textViewDidBeginEditing(textView: UITextView) {
        if textView.text == "Description..." {
            textView.text = ""
            textView.textColor = UIColor.blackColor()
        }
    }
    
    func textViewDidEndEditing(textView: UITextView) {
        if textView.text == "" {
            textView.text = "Description..."
            textView.textColor = UIColor.lightGrayColor()
        }
    }
    
    func createPost() {
        let category = self.categories[0][self.categoryInput.selectedRowInComponent(0)]
        let formatter = NSNumberFormatter()
        //formatter.numberStyle = NSNumberFormatterStyle.DecimalStyle
        //let price : NSNumber! = formatter.numberFromString(self.priceInput.text)
        
        //necessary so that they're "done editing" and thus their .text properties are up-to-date
        self.titleInput.resignFirstResponder()
        self.priceInput.resignFirstResponder()
        self.descriptionInput.resignFirstResponder()
        
        let title = self.titleInput.text
        let description = self.descriptionInput.text
        let priceText : String = self.priceInput.text
        
        if title == "" {
            self.titleInput.becomeFirstResponder()
            return
        } else if priceText == "" {
            self.priceInput.becomeFirstResponder()
            return
        } else if description == "Description..." {
            self.descriptionInput.becomeFirstResponder()
            return
        }
        
        //if self.price != nil {
        let newListing : PSListing = PSListing(title: title, description: description, location: self.userLocation, price: price, lister: PSUser.currentUser())
        newListing.category = self.categories[0][self.categoryInput.selectedRowInComponent(0)]
        dispatch_async(dispatch_get_global_queue(Int(QOS_CLASS_USER_INITIATED.value), 0)) {
            let success = newListing.save()
            if !success {
                newListing.saveEventually()
            }
        }
        NSNotificationCenter.defaultCenter().postNotificationName("jobListingCreated", object: self)
        //        } else {
//            let errorView = UIAlertView(title: "Error", message: "Price is not a number", delegate: nil, cancelButtonTitle: "OK")
//            errorView.title = "Error"
//            errorView.message = "Price is not a number"
//            errorView.show()
//        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        categoryInput.delegate = self
        categoryInput.dataSource = self
        categoryInput.selectRow(7, inComponent: 0, animated: false)
        
        //makes it so there are no extra rows displayed
        self.tableView.tableFooterView = UIView(frame: CGRectZero)
        
        descriptionInput.delegate = self
        descriptionInput.text = "Description..."
        descriptionInput.textColor = UIColor.lightGrayColor()
        
        gettingLocationAlert = UIAlertView(title: "Location", message: "Still getting your location....", delegate: nil, cancelButtonTitle: "OK")
        gettingLocationAlert.hidden = true
        
        // Tell the user what is being used for location, and get location
        let preferences = NSUserDefaults.standardUserDefaults()
        if preferences.boolForKey("useLocationServices") {
            zipcodeInput.text = "Posting job from your current location"
            let manager = CLLocationManager()
            manager.desiredAccuracy = kCLLocationAccuracyBest
            manager.requestAlwaysAuthorization()
            //manager.startUpdatingLocation()
            
            PFGeoPoint.geoPointForCurrentLocationInBackground { (geoPoint, error) -> Void in
                
                if error != nil {
                    //do something to let them know they cant be found.
                    NSLog(error.description)
                    let errorAlert = UIAlertView()
                    errorAlert.title = "Error"
                    errorAlert.message = "There was an error getting your location"
                } else {
                    self.userLocation = geoPoint
                }
                    
                if error != nil {
                    let errorAlert = UIAlertView(title: "Error", message: "An error ocurred, please try again later.", delegate: nil, cancelButtonTitle: "OK")
                } else {
                    self.createPost()
                }
            }

        } else {
            let zip = preferences.valueForKey("zipCode") as! String
            zipcodeInput.text = "Posting job from zip code " + zip
            var address = preferences.valueForKey("zipCode") as! String
            var geocoder = CLGeocoder()
            geocoder.geocodeAddressString(address) { (placemarks: [AnyObject]!, error: NSError!) -> Void in
                if let placemark = placemarks?[0] as? CLPlacemark {
                    
                    self.userLocation = PFGeoPoint(location: placemark.location)
                } else {
                    // Show an error
                    let errorAlert = UIAlertView()
                    errorAlert.title = "Error"
                    errorAlert.message = "There was an error getting your location"
                }
                
                if error != nil {
                    let errorAlert = UIAlertView(title: "Error", message: "An error ocurred, please try again later.", delegate: nil, cancelButtonTitle: "OK")
                } else {
                    self.createPost()
                }
                
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

//    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
//        // #warning Potentially incomplete method implementation.
//        // Return the number of sections.
//        return 1
//    }
//
//    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        // #warning Incomplete method implementation.
//        // Return the number of rows in the section.
//        return 5
//    }
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return categories.count
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return categories[component].count
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String! {
        return categories[component][row]
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        return
    }

    /*
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath) as UITableViewCell

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

}
