//
//  JobView.swift
//  GeoJobs
//
//  Created by Steven Patterson on 3/13/15.
//
//

import UIKit

protocol JobViewDelegate {
    func updateJob(job: PSListing, index : Int)
}

class JobView: UIViewController, UITableViewDelegate, UITableViewDataSource, ChildDelegate {
    
    @IBOutlet var table : UITableView!
    
    var applicants:[PSApplication] = []
    var job : PSListing!
    
    var path = NSIndexPath(forRow: 0, inSection: 0)
    
    var child : JobDetailHeaderView!
    var cameFromMyApplications : Bool = false
    var delegate : JobViewDelegate!
    var index : Int!
    
    var revokeButton : UIBarButtonItem!
    var applyButton : UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.table.dataSource = self
        self.table.delegate = self
        
        child = self.childViewControllers[0] as! JobDetailHeaderView
        
        //set up the two buttons
        if PSUser.currentUser() != nil {
            applyButton = UIBarButtonItem(title: "Apply", style: .Plain, target: self, action: "applyButtonPressed")
        } else {
            applyButton = UIBarButtonItem(title: "Apply", style: .Plain, target: self, action: "applyPressedWhileBrowsingWithoutAccount")
        }
        revokeButton = UIBarButtonItem(title: "Revoke", style: .Plain, target: self, action: "revokeButtonPressed")
        
        dispatch_async(dispatch_get_global_queue(Int(QOS_CLASS_USER_INITIATED.value), 0)) {
            self.job.lister.fetchIfNeeded()
            var rating : Float = (Float)(PSReview.queryAverageRatingOfUser(self.job.lister))
            dispatch_async(dispatch_get_main_queue()) {
            //use job variable to set up stuff in child static table
            self.child.owner.text = "by \(self.job.lister.username)"
            var formatter = NSNumberFormatter()
            formatter.maximumFractionDigits = 2
            formatter.minimumFractionDigits = 2
            var str = formatter.stringFromNumber(self.job.price)
            self.child.titleAndPrice.text = "\(self.job.title) - $\(str!)"
            self.child.category.text = self.job.category
            self.child.jobDescription.text = self.job.jobDescription
            }
            dispatch_async(dispatch_get_main_queue()) {
            self.child.rating.rating = rating
            }
            PSReview.queryReviewCountInBackground(self.job.lister, completion: { (count, error) -> Void in
                let temp : Int = Int(count)
                self.child.numReviews.text = "(\(temp))"
            })
            
            if PSUser.currentUser() == nil {
                self.navigationItem.rightBarButtonItem = self.applyButton
                return
            }
            
            //if current user owns this job, show no button
            if PSUser.currentUser() == self.job.lister {
                self.navigationItem.rightBarButtonItem = nil
                return
            }
            //if user's app has been rejected, show no button
            for applicant in self.applicants {
                if applicant.status == "rejected" {
                    self.navigationItem.rightBarButtonItem = nil
                    return
                }
            }
            self.job.lister.fetchIfNeeded()
            
            //If current user already applied to this job, display Revoke button in upper right, otherwise display Apply button
            var applied = false
            for applicant in self.applicants {
                if applicant.applicant == PSUser.currentUser() {
                    applied = true
                    break
                }
            }
            dispatch_async(dispatch_get_main_queue()) {
                if !applied {
                    self.navigationItem.rightBarButtonItem = self.applyButton
                } else {
                    self.navigationItem.rightBarButtonItem = self.revokeButton
                }
            }
        }
    }
    
    func applyButtonPressed() {
        NSLog("applied!")
        performSegueWithIdentifier("applySegue", sender: self)
    }
    func revokeButtonPressed() {
        //Code adapted from: http://stackoverflow.com/questions/24195310/how-to-add-an-action-to-a-uialertview-button-using-swift-ios
        // Create the alert controller
        var alertController = UIAlertController(title: "Confirm", message: "Are you sure you want to revoke your application to this job listing?", preferredStyle: .Alert)
        
        // Create the actions
        var revokeAction = UIAlertAction(title: "Revoke", style: UIAlertActionStyle.Default) {
            UIAlertAction in
            //if user came from My Applications, delete app then segue back to Home screen
            if self.cameFromMyApplications == true {
                for (index, value) in enumerate(self.applicants) {
                    if self.applicants[index].applicant == PSUser.currentUser() {
                        self.applicants[index].deleteInBackgroundWithBlock({ (success, error) -> Void in
                            if !success {
                                self.applicants[index].deleteEventually()
                            }
                        })
                        break
                    }
                }
                NSNotificationCenter.defaultCenter().postNotificationName("applicationRevoked", object: self)
                return
            }
            //change the button to apply
            self.navigationItem.rightBarButtonItem = self.applyButton
            //remove applicant from the array of applicants and reload the table
            for (index, value) in enumerate(self.applicants) {
                if self.applicants[index].applicant == PSUser.currentUser() {
                    self.applicants[index].deleteInBackground()
                    self.applicants.removeAtIndex(index)
                    self.table.reloadData()
                    break
                }
            }
        }
        var cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel) {
            UIAlertAction in
        }
        
        // Add the actions
        alertController.addAction(revokeAction)
        alertController.addAction(cancelAction)
        
        // Present the controller
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
    func applyPressedWhileBrowsingWithoutAccount() {
        // Create the alert controller
        var alertController = UIAlertController(title: "Create Account?", message: "You must create an account to use that functionality", preferredStyle: .Alert)
        
        // Create the actions
        var createAccount = UIAlertAction(title: "Create Account", style: UIAlertActionStyle.Default) {
            UIAlertAction in
            //post a notification to Start screen to pop all its views then push the Sign Up page
            NSNotificationCenter.defaultCenter().postNotificationName("createAccount", object: self)
        }
        var cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel) {
            UIAlertAction in
        }
        
        // Add the actions
        alertController.addAction(createAccount)
        alertController.addAction(cancelAction)
        
        // Present the controller
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
    func applicationSubmitted(childFromDelegate: Apply) {
        //get child's pitch and price
        let price = childFromDelegate.child.price
        let pitch = childFromDelegate.child.pitch.text
        //create and submit this application to the current job listing
        let app = PSApplication(pitch: pitch, offer: price, listing: job)
        app.saveInBackground()
        //reload the table
        applicants.append(app)
        table.reloadData()
        //change button from Apply to Revoke
        self.navigationItem.rightBarButtonItem = revokeButton
        //childFromDelegate.dismissViewControllerAnimated(true, completion: nil)
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return applicants.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier("applicantCell", forIndexPath: indexPath) as! ApplicantCell
        
        dispatch_async(dispatch_get_global_queue(Int(QOS_CLASS_USER_INITIATED.value), 0)) {
        //configure using applicants array
        PSReview.queryReviewCountInBackground(self.applicants[indexPath.row].applicant, completion: { (count, error) -> Void in
            let temp : Int = Int(count)
            cell.numReviews.text = "(\(temp))"
        })
        
        cell.rating.rating = (Float)(PSReview.queryAverageRatingOfUser(self.applicants[indexPath.row].applicant))
        self.applicants[indexPath.row].applicant.fetchIfNeeded()
        cell.applicant.text = self.applicants[indexPath.row].applicant.username
        var formatter = NSNumberFormatter()
        formatter.maximumFractionDigits = 2
        formatter.minimumFractionDigits = 2
        var str = formatter.stringFromNumber(self.applicants[indexPath.row].offer)
        cell.price.text = "$\(str!)"
        }
        
        return cell
    }
    
    func tableView(tableView: UITableView, willSelectRowAtIndexPath indexPath: NSIndexPath) -> NSIndexPath? {
        
        path = indexPath
        return indexPath
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        //differentiate between which segue was called...
        if segue.identifier == "MyApplicationsApplicant" {
            let childView = segue.destinationViewController as! ProfileView
            childView.user = applicants[path.row].applicant
        } else if segue.identifier == "JobDetailsSegue" {
            let childView = segue.destinationViewController as! ProfileView
            childView.user = job.lister
        } else if segue.identifier == "JobDetailsEmbedSegue" {
            let childView = segue.destinationViewController as! JobDetailHeaderView
            childView.parentController = self
        } else if segue.identifier == "applySegue" {
            let childView = segue.destinationViewController as! Apply
            childView.jobDescription = job.jobDescription
            childView.delegate = self
        }
    }
    
    override func viewWillDisappear(animated: Bool) {
        if !cameFromMyApplications { //came from Browse
            delegate.updateJob(job, index: index)
        }
    }
    
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
}
